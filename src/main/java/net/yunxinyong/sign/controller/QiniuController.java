package net.yunxinyong.sign.controller;

import com.qiniu.util.Auth;
import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.Image;
import net.yunxinyong.sign.mapper.EcnomicInventoryMapper;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.EcnomicService;
import net.yunxinyong.sign.service.QiniuService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.utils.QiniuUtils;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping(value = "qiniu")
public class QiniuController {

    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;
    @Autowired
    private QiniuService qiniuService;
    @Autowired
    private SignAdminMapper signAdminMapper;

    /**
     * 七牛云上传生成凭证
     *
     * @throws Exception
     */

    @ResponseBody
    @RequestMapping(value="/token", method=RequestMethod.GET)
    public ResponseVo QiniuUpToken(@Param("name") String name, @Param("suffix") String suffix,@Param("type") Integer type) throws Exception{
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        return CECResultUtil.success("获取成功",QiniuUtils.QiniuUpToken(suffix,
                name,type));
    }


    @ResponseBody
    @RequestMapping(value="/uploadImg", method=RequestMethod.POST)
    public ResponseVo uploadImg(@RequestParam MultipartFile image, HttpServletRequest request) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        if (image.isEmpty()) {

            return CECResultUtil.error("文件为空，请重新上传");
        }

        try {
            byte[] bytes = image.getBytes();
            String imageName = UUID.randomUUID().toString();
            try {
                //使用base64方式上传到七牛云
                String url = QiniuUtils.put64image(bytes, imageName);
                return CECResultUtil.success("文件上传成功",url);
            } catch (Exception e) {
                throw new CECException(505,e.getMessage());
            }
        } catch (IOException e) {
            throw new CECException(505,e.getMessage());
        }
    }

    /**
     * 根据invId获取访问连接
     * @param invId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/geturl", method=RequestMethod.GET)
    public ResponseVo geturl(@Param("invId") Integer invId) throws Exception{
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        List<String> list = qiniuService.getUrl(invId);
        return CECResultUtil.success("获取成功",list);
    }

    @ResponseBody
    @RequestMapping(value="/insertImages", method=RequestMethod.POST)
    public ResponseVo insert(@RequestBody List<Image> images, @Param("name") String name, @Param("invId") Integer invId){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        qiniuService.insert(images, invId, name);
        return CECResultUtil.success("添加成功");
    }

    /**
     * 根据invId获取审核图片访问连接
     * @param invId
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/getExamineUrl", method=RequestMethod.GET)
    public ResponseVo getExamineUrl(@Param("invId") Integer invId) throws Exception{
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        List<String> list = qiniuService.getExamineUrl(invId);
        return CECResultUtil.success("获取成功",list);
    }

    /**
     * 根据invId获取审核图片访问连接
     * @param
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/mkzip", method=RequestMethod.POST)
    public ResponseVo mkzip(@RequestBody List<Integer> idList,@Param("type") Integer type) throws Exception{
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        String mkzip = qiniuService.mkzip(idList, type, adminId);
        return CECResultUtil.success("打包成功",mkzip);
    }

}

