package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecThree;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.CecThreeService;
import net.yunxinyong.sign.service.RecordService;
import net.yunxinyong.sign.service.RegisterStatusService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.CecThreeVo;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "three")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = CECException.class)
public class CecThreeController {
    @Autowired
    private CecThreeService cecThreeService;
    @Autowired
    private RecordService recordService;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private RegisterStatusService registerStatusService;
    /**
     * 添加和更新企业信息
     *
     * @param
     * @return
     */
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseVo add(@RequestBody CecThreeVo cecThreeVo) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        //首先判断数据库是否存在这条数据，如果不存在就插入，存在就更新。
        CecThree cecThree1 = cecThreeService.get(cecThreeVo.getInvId());
        if (cecThree1 == null) {
            cecThreeVo.setId(null);
        } else {
            if (cecThree1.getState() != null && cecThree1.getState() == -1) {
                return CECResultUtil.error("企业状态为不可修改");
            }
            cecThreeVo.setId(cecThree1.getId());
        }
        cecThreeService.insertOrUpdate(cecThreeVo);
        //更新可下载状态
        registerStatusService.updadeDownloadStatus(cecThreeVo.getInvId());

        boolean insert = recordService.insert(adminId, null, null, 12);
        if(!insert){
            throw new CECException(501,"操作失败");
        }
        return CECResultUtil.success("操作成功");
    }

    /**
     * 查询企业信息
     *
     * @return
     */
    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ResponseVo get(@RequestParam Integer invId) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecThree cecThree = cecThreeService.get(invId);
        return CECResultUtil.success("获取成功", cecThree);
    }

}
