package net.yunxinyong.sign.controller;



import net.yunxinyong.sign.service.ExcelService;


import net.yunxinyong.sign.utils.DateUtils;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



@RestController
@RequestMapping(value = "excel")
public class ExcelController {

    @Autowired
    private ExcelService excelService;

    @RequestMapping(value = "test",method = RequestMethod.GET)
    public void getList(HttpServletResponse response) throws IOException {

        response.setHeader("Content-disposition",
                "attachment; filename=" + DateUtils.getNow(DateUtils.FORMAT_SHORT_CN)
                        + ".xlsx");// 组装附件名称和格式
        ServletOutputStream outputStream = response.getOutputStream();
        try {
            excelService.test(outputStream);
            outputStream.flush();
        } finally {
            outputStream.close();
        }

    }

    /**
     * 这个接口可以下载当当前时间为止，已经注册的企业 的excel 包括 同意社会信用代码 组织机构代码 名字 法人 手机号
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "test2",method = RequestMethod.GET)
    public void getEnterpriseList(HttpServletResponse response) throws IOException {
        response.setHeader("Content-disposition",
                "attachment; filename=" + DateUtils.getNow(DateUtils.FORMAT_LONG_CN)
                        + ".xlsx");// 组装附件名称和格式
        ServletOutputStream outputStream = response.getOutputStream();
        try {
            excelService.listEnterprise(outputStream);
            outputStream.flush();
        }finally {
            outputStream.close();
        }

    }

}
