package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.service.EnumeratorInfoService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import net.yunxinyong.sign.vo.StatusStatisticsVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "enumerator")
public class EnumeratorController {
    @Autowired
    private EnumeratorInfoService enumeratorInfoService;

    @RequestMapping(value = "getAllInfo", method = RequestMethod.GET)
    public ResponseVo getEnumeratorInfo() {
        //获取用户id
        Object principals = SecurityUtils.getSubject().getPrincipals();
        Integer adminId = Integer.parseInt(principals.toString());
        StatusStatisticsVo statisticsVo = enumeratorInfoService.getAllInfo(adminId);
        return CECResultUtil.success("操作成功", statisticsVo);
    }
}
