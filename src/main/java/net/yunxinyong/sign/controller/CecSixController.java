package net.yunxinyong.sign.controller;


import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecOne;
import net.yunxinyong.sign.entity.CecSix;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.CecSixSerivce;
import net.yunxinyong.sign.service.RegisterStatusService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "six")
public class CecSixController {
    @Autowired
    private CecSixSerivce cecSixSerivce;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private RegisterStatusService registerStatusService;

    @RequestMapping(value = "update")
    public ResponseVo addAndupdate(@RequestBody CecSix cecSix) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }

        CecSix cecSix1 = cecSixSerivce.select(cecSix.getInvId());
        if (cecSix1 != null) {
            if (cecSix1.getState() != null && cecSix1.getState() == -1) {
                return CECResultUtil.error("企业状态为不可修改");
            }
            cecSix.setId(cecSix1.getId());
            cecSixSerivce.update(cecSix,adminId);
        } else {
            cecSixSerivce.insert(cecSix,adminId);
        }
        //更新下载状态
        registerStatusService.updadeDownloadStatus(cecSix.getInvId());
        return CECResultUtil.success("操作成功", cecSix);
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ResponseVo get(@RequestParam Integer invId) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecSix select = cecSixSerivce.select(invId);
        return CECResultUtil.success("获取成功", select);
    }
}
