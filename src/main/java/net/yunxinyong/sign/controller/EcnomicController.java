package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.EcnomicInventory;
import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.EcnomicService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.vo.EcnomicReqVo;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "ecnomic")
public class EcnomicController {
    @Autowired
    private EcnomicService ecnomicService;
    @Autowired
    private SignAdminMapper signAdminMapper;

    @RequestMapping(value = "examine",method = RequestMethod.GET)
    public ResponseVo examine(@Param("invId") Integer invId,@Param("uiteStatus") Integer uiteStatus){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        ecnomicService.examine(invId,adminId,uiteStatus);
        return CECResultUtil.success("审核操作成功");
    }

    @RequestMapping(value = "list",method = RequestMethod.POST)
    public ResponseVo getList(@RequestBody EcnomicReqVo ecnomicReqVo,
                              @Param("page") Integer page,
                              @Param("rows") Integer rows){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        PageBean pageBean = ecnomicService.getList(ecnomicReqVo, page, rows, adminId);
        return CECResultUtil.success("获取数据成功",pageBean);
    }

    @RequestMapping(value = "insert",method = RequestMethod.POST)
    public ResponseVo insert(@RequestBody EcnomicInventory ecnomicInventory){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (ecnomicInventory.getUnitDetailedName() == null || ecnomicInventory.getUnitDetailedName().equals("")){
            throw new CECException(501,"请输入企业名称");
        }
        if (signAdmin != null){
            if (signAdmin.getState() == 1){
                throw new CECException(402,"当前用户已被禁用");
            }
            ecnomicInventory.setReferenceAddress02(signAdmin.getArea());
            ecnomicService.insert(ecnomicInventory, adminId);
            return CECResultUtil.success("添加企业成功");
        }else {
            throw new CECException(402,"当前用户不存在");
        }

    }
}
