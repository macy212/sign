package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.service.OverViewService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import net.yunxinyong.sign.vo.StatusStatisticsVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "overView")
public class OverViewController {
    @Autowired
    private OverViewService overViewService;

    @RequestMapping(value = "getOverView", method = RequestMethod.POST)
    public ResponseVo getOverView(String street) {

            //获取用户id
            Object principals = SecurityUtils.getSubject().getPrincipals();
            Integer adminId = Integer.parseInt(principals.toString());
            StatusStatisticsVo statusStatisticsVo = overViewService.getOverView(adminId, street);


        return CECResultUtil.success("操作成功",statusStatisticsVo);
    }
}
