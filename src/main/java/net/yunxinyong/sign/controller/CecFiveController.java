package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecFive;
import net.yunxinyong.sign.entity.CecOne;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.CecFiveSerivce;
import net.yunxinyong.sign.service.RegisterStatusService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "five")
public class CecFiveController {
    @Autowired
    private CecFiveSerivce cecFiveSerivce;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private RegisterStatusService registerStatusService;
    @RequestMapping(value = "update")
    public ResponseVo addAndupdate(@RequestBody CecFive cecFive) {
        Object principals = SecurityUtils.getSubject().getPrincipals();
        Integer adminId = Integer.parseInt(principals.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecFive cecFive1 = cecFiveSerivce.select(cecFive.getInvId());
        if (cecFive1 != null) {
            if (cecFive1.getState() != null && cecFive1.getState() == -1) {
                return CECResultUtil.error("企业状态为不可修改");
            }
            cecFive.setId(cecFive1.getId());
            cecFiveSerivce.update(cecFive,adminId);
        } else {
            cecFiveSerivce.insert(cecFive,adminId);
        }
        //更新可下载
        registerStatusService.updadeDownloadStatus(cecFive.getInvId());
        return CECResultUtil.success("操作成功", cecFive);
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ResponseVo get(@RequestParam Integer invId) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecFive select = cecFiveSerivce.select(invId);
        return CECResultUtil.success("获取成功", select);
    }
}
