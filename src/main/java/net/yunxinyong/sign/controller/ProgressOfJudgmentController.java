
package net.yunxinyong.sign.controller;


import net.yunxinyong.sign.entity.CecBase;
import net.yunxinyong.sign.entity.EcnomicInventory;
import net.yunxinyong.sign.mapper.EcnomicInventoryMapper;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.CecBaseService;
import net.yunxinyong.sign.service.ProgressOfJudgmentService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ProgressVo;
import net.yunxinyong.sign.vo.ResponseVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "poj")
public class ProgressOfJudgmentController {
    @Autowired
    private ProgressOfJudgmentService poj;
    @Autowired
    private CecBaseService cecBaseService;
    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;
    @Autowired
    private SignAdminMapper signAdminMapper;

    @RequestMapping(value = "getBase",method = RequestMethod.GET)
    public ResponseVo getBaseProgress(Integer invId){
        double pojBaseNum = poj.getBaseNum(invId);
        return CECResultUtil.success("获取成功",pojBaseNum);
    }

    @RequestMapping(value = "getOne",method = RequestMethod.GET)
    public ResponseVo getOneProgress(Integer invId){
        double pojOneNum = poj.getOneNum(invId);
        return  CECResultUtil.success("获取成功",pojOneNum);
    }
    @RequestMapping(value = "getTwo",method = RequestMethod.GET)
    public ResponseVo getTwoProgress(Integer invId){
        double pojTwoNum = poj.getTwoNum(invId);
        return CECResultUtil.success("获取成功",pojTwoNum);
    }
    @RequestMapping(value = "getThree",method = RequestMethod.GET)
    public ResponseVo getThreeProgress(Integer invId){
        double pojThreeNum = poj.getThreeNum(invId);
        return  CECResultUtil.success("获取成功",pojThreeNum);
    }
    @RequestMapping(value = "getFour",method = RequestMethod.GET)
    public ResponseVo getFourProgress(Integer invId){
        double pojFourNum = poj.getFourNum(invId);
        return  CECResultUtil.success("获取成功",pojFourNum);
    }
    @RequestMapping(value = "getFive",method = RequestMethod.GET)
    public ResponseVo getFiveProgress(Integer invId){
        double pojFiveNum = poj.getFiveNum(invId);
        return CECResultUtil.success("获取成功",pojFiveNum);
    }
    @RequestMapping(value = "getSix",method = RequestMethod.GET)
    public ResponseVo getSixProgress(Integer invId){
        double pojSixNum = poj.getSixNum(invId);
        return  CECResultUtil.success("获取成功",pojSixNum);
    }
    @RequestMapping(value = "getSeven" ,method = RequestMethod.GET)
    public ResponseVo getSevenProgress(Integer invId){
        double pojSevenNum = poj.getSevenNum(invId);
        return  CECResultUtil.success("获取成功",pojSevenNum);
    }
    @RequestMapping(value = "getTotalProgress",method = RequestMethod.GET)
    public ResponseVo getTotalProgress(Integer invId){
        ProgressVo progressVo = new ProgressVo();
        //总进度
        double pojTotalProgress = poj.getTotalProgress(invId);
        //下载状态
        CecBase cecBase = cecBaseService.select(invId);
        progressVo.setPojTotalProgress(pojTotalProgress);
        if (cecBase != null){
            progressVo.setState(cecBase.getState());
        }
        EcnomicInventory ecnomicInventory = ecnomicInventoryMapper.selectByPrimaryKey((long) invId);
        if (ecnomicInventory != null){
            BeanUtils.copyProperties(ecnomicInventory,progressVo);
            //progressVo.setType(ecnomicInventory.getType());
        }
        return CECResultUtil.success("获取成功",progressVo);
    }


}

