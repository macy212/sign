package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecOne;
import net.yunxinyong.sign.entity.CecSeven;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.CecSevenService;
import net.yunxinyong.sign.service.RegisterStatusService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;

@RestController
@RequestMapping(value = "seven")
public class CecSevenController {

    @Autowired
    private CecSevenService cecSevenService;
    @Autowired
    private RegisterStatusService registerStatusService;
    @Autowired
    private SignAdminMapper signAdminMapper;

    @RequestMapping(value = "update")
    public ResponseVo addAndupdate(@RequestBody CecSeven cecSeven) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecSeven cecSeven1 = cecSevenService.select(cecSeven.getInvId());
        if (cecSeven1 != null) {
            if (cecSeven1.getState() != null && cecSeven1.getState() == -1) {
                return CECResultUtil.error("企业状态为不可修改");
            }
            cecSeven.setId(cecSeven1.getId());
            cecSevenService.update(cecSeven,adminId);
        } else {
            cecSevenService.insert(cecSeven,adminId);
        }
        //更新可下载状态
        registerStatusService.updadeDownloadStatus(cecSeven.getInvId());
        return CECResultUtil.success("操作成功", cecSeven);
    }
    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ResponseVo get(@RequestParam Integer invId) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecSeven select = cecSevenService.select(invId);
        return CECResultUtil.success("获取成功", select);
    }
}
