package net.yunxinyong.sign.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class CecFour {
    private Integer id;

    private Integer invId;

    private String socialCreditCode;

    private String organizationCode;

    private String unitDetailedName;

    private BigDecimal investmentAmount;

    private BigDecimal moreThan500;

    private Integer moreThan5000;

    private String projectCode;

    private String projectName;

    private String province;

    private String city;

    private String county;

    private String towns;

    private String village;

    private String unitZoningCode;

    private String industryCode;

    private String startTime;

    private String productionTime;

    private BigDecimal planTotalInvestment;

    private BigDecimal cumulativeInvestment;

    private BigDecimal completeInvestment;

    private BigDecimal installationEngineering;

    private String unitHead;

    private String statisticalControlOfficer;

    private String fillFormBy;

    private String phone;

    private Integer state;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private Integer status;

    private Integer formType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvId() {
        return invId;
    }

    public void setInvId(Integer invId) {
        this.invId = invId;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName == null ? null : unitDetailedName.trim();
    }

    public BigDecimal getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(BigDecimal investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public BigDecimal getMoreThan500() {
        return moreThan500;
    }

    public void setMoreThan500(BigDecimal moreThan500) {
        this.moreThan500 = moreThan500;
    }

    public Integer getMoreThan5000() {
        return moreThan5000;
    }

    public void setMoreThan5000(Integer moreThan5000) {
        this.moreThan5000 = moreThan5000;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode == null ? null : projectCode.trim();
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName == null ? null : projectName.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county == null ? null : county.trim();
    }

    public String getTowns() {
        return towns;
    }

    public void setTowns(String towns) {
        this.towns = towns == null ? null : towns.trim();
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village == null ? null : village.trim();
    }

    public String getUnitZoningCode() {
        return unitZoningCode;
    }

    public void setUnitZoningCode(String unitZoningCode) {
        this.unitZoningCode = unitZoningCode == null ? null : unitZoningCode.trim();
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode == null ? null : industryCode.trim();
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    public String getProductionTime() {
        return productionTime;
    }

    public void setProductionTime(String productionTime) {
        this.productionTime = productionTime == null ? null : productionTime.trim();
    }

    public BigDecimal getPlanTotalInvestment() {
        return planTotalInvestment;
    }

    public void setPlanTotalInvestment(BigDecimal planTotalInvestment) {
        this.planTotalInvestment = planTotalInvestment;
    }

    public BigDecimal getCumulativeInvestment() {
        return cumulativeInvestment;
    }

    public void setCumulativeInvestment(BigDecimal cumulativeInvestment) {
        this.cumulativeInvestment = cumulativeInvestment;
    }

    public BigDecimal getCompleteInvestment() {
        return completeInvestment;
    }

    public void setCompleteInvestment(BigDecimal completeInvestment) {
        this.completeInvestment = completeInvestment;
    }

    public BigDecimal getInstallationEngineering() {
        return installationEngineering;
    }

    public void setInstallationEngineering(BigDecimal installationEngineering) {
        this.installationEngineering = installationEngineering;
    }

    public String getUnitHead() {
        return unitHead;
    }

    public void setUnitHead(String unitHead) {
        this.unitHead = unitHead == null ? null : unitHead.trim();
    }

    public String getStatisticalControlOfficer() {
        return statisticalControlOfficer;
    }

    public void setStatisticalControlOfficer(String statisticalControlOfficer) {
        this.statisticalControlOfficer = statisticalControlOfficer == null ? null : statisticalControlOfficer.trim();
    }

    public String getFillFormBy() {
        return fillFormBy;
    }

    public void setFillFormBy(String fillFormBy) {
        this.fillFormBy = fillFormBy == null ? null : fillFormBy.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFormType() {
        return formType;
    }

    public void setFormType(Integer formType) {
        this.formType = formType;
    }
}