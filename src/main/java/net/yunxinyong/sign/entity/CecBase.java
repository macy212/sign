package net.yunxinyong.sign.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class CecBase {
    private Integer id;

    private Integer invId;

    private Integer unitType;

    private Integer isCorporateUnit;

    private String cellCode;

    private String buildingCode;

    private String bookSequenceCode;

    private String majorType;

    private String organizationCode;

    private String socialCreditCode;

    private String unitDetailedName;

    private String legalRepresentative;

    private String establishmentTime;

    private String areaCode;

    private String fixedLineTelephone;

    private String cellPhoneNumber;

    private String faxNumber;

    private String postalCode;

    private String eMail;

    private String website;

    private String province;

    private String city;

    private String county;

    private String towns;

    private String village;

    private String subdistrict;

    private String cummunity;

    private String parkName;

    private String parkCode;

    private String unitZoningCode;

    private String unitCountryCode;

    private String registerProvince;

    private String registerCity;

    private String registerCounty;

    private String registerTowns;

    private String registerVillage;

    private String registerSubdistrict;

    private String registerCummunity;

    private String registerParkName;

    private String registerParkCode;

    private String registerUnitZoningCode;

    private String registerUnitCountryCode;

    private Integer businessState;

    private String majorBusiness1;

    private String majorBusiness2;

    private String majorBusiness3;

    private String industryCode;

    private String reportCategory;

    private Integer mechanismType;

    private String registrationType;

    private Integer isB;

    private Integer formOfOperation;

    private String chainBrand;

    private String retailFormat;

    private Float warOperatingArea;

    private Integer accommodationStar;

    private Float aacOperatingArea;

    private String gatInvestmentSituation;

    private Integer affiliation;

    private Integer holdingSituation;

    private Integer accountingStandardCategory;

    private Integer accountingStandard;

    private Integer unitScale;

    private Integer employee;

    private Integer female;

    private BigDecimal businessIncome;

    private BigDecimal mainBusinessIncome;

    private BigDecimal totalAssets;

    private BigDecimal taxAndAdditional;

    private BigDecimal nonUnitExpenditure;

    private BigDecimal nonUnitTotalAssets;

    private String cateringFormat;

    private String constructionQualificationCode;

    private Integer realEstateLevel;

    private Integer isHavaSuperior;

    private String superiorOrganizationCode;

    private String superiorSocialCreditCode;

    private String superiorUnitDetailedName;

    private Integer isHaveIndustrial;

    private Integer unitTypeD;

    private String socialCreditCodeD;

    private String organizationCodeD;

    private String unitDetailedNameD;

    private String addressD;

    private String unitZoningCodeD;

    private Integer employeeD;

    private Integer femaleD;

    private BigDecimal operatingIncomeD;

    private BigDecimal nonOperatingExpenditureD;

    private Float constructionAreaD;

    private Float salesAreaD;

    private Float areaForSaleD;

    private String unitHead;

    private String statisticalControlOfficer;

    private String fillFormBy;

    private String phone;

    private String extensionNumber;

    private Integer state;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date reportingDate;

    private Date updateTime;

    private Date createTime;

    private Integer status;

    private BigDecimal macaoInput;

    private Integer macao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvId() {
        return invId;
    }

    public void setInvId(Integer invId) {
        this.invId = invId;
    }

    public Integer getUnitType() {
        return unitType;
    }

    public void setUnitType(Integer unitType) {
        this.unitType = unitType;
    }

    public Integer getIsCorporateUnit() {
        return isCorporateUnit;
    }

    public void setIsCorporateUnit(Integer isCorporateUnit) {
        this.isCorporateUnit = isCorporateUnit;
    }

    public String getCellCode() {
        return cellCode;
    }

    public void setCellCode(String cellCode) {
        this.cellCode = cellCode == null ? null : cellCode.trim();
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public void setBuildingCode(String buildingCode) {
        this.buildingCode = buildingCode == null ? null : buildingCode.trim();
    }

    public String getBookSequenceCode() {
        return bookSequenceCode;
    }

    public void setBookSequenceCode(String bookSequenceCode) {
        this.bookSequenceCode = bookSequenceCode == null ? null : bookSequenceCode.trim();
    }

    public String getMajorType() {
        return majorType;
    }

    public void setMajorType(String majorType) {
        this.majorType = majorType == null ? null : majorType.trim();
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName == null ? null : unitDetailedName.trim();
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative == null ? null : legalRepresentative.trim();
    }

    public String getEstablishmentTime() {
        return establishmentTime;
    }

    public void setEstablishmentTime(String establishmentTime) {
        this.establishmentTime = establishmentTime == null ? null : establishmentTime.trim();
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    public String getFixedLineTelephone() {
        return fixedLineTelephone;
    }

    public void setFixedLineTelephone(String fixedLineTelephone) {
        this.fixedLineTelephone = fixedLineTelephone == null ? null : fixedLineTelephone.trim();
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber == null ? null : cellPhoneNumber.trim();
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber == null ? null : faxNumber.trim();
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode == null ? null : postalCode.trim();
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail == null ? null : eMail.trim();
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website == null ? null : website.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county == null ? null : county.trim();
    }

    public String getTowns() {
        return towns;
    }

    public void setTowns(String towns) {
        this.towns = towns == null ? null : towns.trim();
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village == null ? null : village.trim();
    }

    public String getSubdistrict() {
        return subdistrict;
    }

    public void setSubdistrict(String subdistrict) {
        this.subdistrict = subdistrict == null ? null : subdistrict.trim();
    }

    public String getCummunity() {
        return cummunity;
    }

    public void setCummunity(String cummunity) {
        this.cummunity = cummunity == null ? null : cummunity.trim();
    }

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName == null ? null : parkName.trim();
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode == null ? null : parkCode.trim();
    }

    public String getUnitZoningCode() {
        return unitZoningCode;
    }

    public void setUnitZoningCode(String unitZoningCode) {
        this.unitZoningCode = unitZoningCode == null ? null : unitZoningCode.trim();
    }

    public String getUnitCountryCode() {
        return unitCountryCode;
    }

    public void setUnitCountryCode(String unitCountryCode) {
        this.unitCountryCode = unitCountryCode == null ? null : unitCountryCode.trim();
    }

    public String getRegisterProvince() {
        return registerProvince;
    }

    public void setRegisterProvince(String registerProvince) {
        this.registerProvince = registerProvince == null ? null : registerProvince.trim();
    }

    public String getRegisterCity() {
        return registerCity;
    }

    public void setRegisterCity(String registerCity) {
        this.registerCity = registerCity == null ? null : registerCity.trim();
    }

    public String getRegisterCounty() {
        return registerCounty;
    }

    public void setRegisterCounty(String registerCounty) {
        this.registerCounty = registerCounty == null ? null : registerCounty.trim();
    }

    public String getRegisterTowns() {
        return registerTowns;
    }

    public void setRegisterTowns(String registerTowns) {
        this.registerTowns = registerTowns == null ? null : registerTowns.trim();
    }

    public String getRegisterVillage() {
        return registerVillage;
    }

    public void setRegisterVillage(String registerVillage) {
        this.registerVillage = registerVillage == null ? null : registerVillage.trim();
    }

    public String getRegisterSubdistrict() {
        return registerSubdistrict;
    }

    public void setRegisterSubdistrict(String registerSubdistrict) {
        this.registerSubdistrict = registerSubdistrict == null ? null : registerSubdistrict.trim();
    }

    public String getRegisterCummunity() {
        return registerCummunity;
    }

    public void setRegisterCummunity(String registerCummunity) {
        this.registerCummunity = registerCummunity == null ? null : registerCummunity.trim();
    }

    public String getRegisterParkName() {
        return registerParkName;
    }

    public void setRegisterParkName(String registerParkName) {
        this.registerParkName = registerParkName == null ? null : registerParkName.trim();
    }

    public String getRegisterParkCode() {
        return registerParkCode;
    }

    public void setRegisterParkCode(String registerParkCode) {
        this.registerParkCode = registerParkCode == null ? null : registerParkCode.trim();
    }

    public String getRegisterUnitZoningCode() {
        return registerUnitZoningCode;
    }

    public void setRegisterUnitZoningCode(String registerUnitZoningCode) {
        this.registerUnitZoningCode = registerUnitZoningCode == null ? null : registerUnitZoningCode.trim();
    }

    public String getRegisterUnitCountryCode() {
        return registerUnitCountryCode;
    }

    public void setRegisterUnitCountryCode(String registerUnitCountryCode) {
        this.registerUnitCountryCode = registerUnitCountryCode == null ? null : registerUnitCountryCode.trim();
    }

    public Integer getBusinessState() {
        return businessState;
    }

    public void setBusinessState(Integer businessState) {
        this.businessState = businessState;
    }

    public String getMajorBusiness1() {
        return majorBusiness1;
    }

    public void setMajorBusiness1(String majorBusiness1) {
        this.majorBusiness1 = majorBusiness1 == null ? null : majorBusiness1.trim();
    }

    public String getMajorBusiness2() {
        return majorBusiness2;
    }

    public void setMajorBusiness2(String majorBusiness2) {
        this.majorBusiness2 = majorBusiness2 == null ? null : majorBusiness2.trim();
    }

    public String getMajorBusiness3() {
        return majorBusiness3;
    }

    public void setMajorBusiness3(String majorBusiness3) {
        this.majorBusiness3 = majorBusiness3 == null ? null : majorBusiness3.trim();
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode == null ? null : industryCode.trim();
    }

    public String getReportCategory() {
        return reportCategory;
    }

    public void setReportCategory(String reportCategory) {
        this.reportCategory = reportCategory == null ? null : reportCategory.trim();
    }

    public Integer getMechanismType() {
        return mechanismType;
    }

    public void setMechanismType(Integer mechanismType) {
        this.mechanismType = mechanismType;
    }

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType == null ? null : registrationType.trim();
    }

    public Integer getIsB() {
        return isB;
    }

    public void setIsB(Integer isB) {
        this.isB = isB;
    }

    public Integer getFormOfOperation() {
        return formOfOperation;
    }

    public void setFormOfOperation(Integer formOfOperation) {
        this.formOfOperation = formOfOperation;
    }

    public String getChainBrand() {
        return chainBrand;
    }

    public void setChainBrand(String chainBrand) {
        this.chainBrand = chainBrand == null ? null : chainBrand.trim();
    }

    public String getRetailFormat() {
        return retailFormat;
    }

    public void setRetailFormat(String retailFormat) {
        this.retailFormat = retailFormat == null ? null : retailFormat.trim();
    }

    public Float getWarOperatingArea() {
        return warOperatingArea;
    }

    public void setWarOperatingArea(Float warOperatingArea) {
        this.warOperatingArea = warOperatingArea;
    }

    public Integer getAccommodationStar() {
        return accommodationStar;
    }

    public void setAccommodationStar(Integer accommodationStar) {
        this.accommodationStar = accommodationStar;
    }

    public Float getAacOperatingArea() {
        return aacOperatingArea;
    }

    public void setAacOperatingArea(Float aacOperatingArea) {
        this.aacOperatingArea = aacOperatingArea;
    }

    public String getGatInvestmentSituation() {
        return gatInvestmentSituation;
    }

    public void setGatInvestmentSituation(String gatInvestmentSituation) {
        this.gatInvestmentSituation = gatInvestmentSituation == null ? null : gatInvestmentSituation.trim();
    }

    public Integer getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(Integer affiliation) {
        this.affiliation = affiliation;
    }

    public Integer getHoldingSituation() {
        return holdingSituation;
    }

    public void setHoldingSituation(Integer holdingSituation) {
        this.holdingSituation = holdingSituation;
    }

    public Integer getAccountingStandardCategory() {
        return accountingStandardCategory;
    }

    public void setAccountingStandardCategory(Integer accountingStandardCategory) {
        this.accountingStandardCategory = accountingStandardCategory;
    }

    public Integer getAccountingStandard() {
        return accountingStandard;
    }

    public void setAccountingStandard(Integer accountingStandard) {
        this.accountingStandard = accountingStandard;
    }

    public Integer getUnitScale() {
        return unitScale;
    }

    public void setUnitScale(Integer unitScale) {
        this.unitScale = unitScale;
    }

    public Integer getEmployee() {
        return employee;
    }

    public void setEmployee(Integer employee) {
        this.employee = employee;
    }

    public Integer getFemale() {
        return female;
    }

    public void setFemale(Integer female) {
        this.female = female;
    }

    public BigDecimal getBusinessIncome() {
        return businessIncome;
    }

    public void setBusinessIncome(BigDecimal businessIncome) {
        this.businessIncome = businessIncome;
    }

    public BigDecimal getMainBusinessIncome() {
        return mainBusinessIncome;
    }

    public void setMainBusinessIncome(BigDecimal mainBusinessIncome) {
        this.mainBusinessIncome = mainBusinessIncome;
    }

    public BigDecimal getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(BigDecimal totalAssets) {
        this.totalAssets = totalAssets;
    }

    public BigDecimal getTaxAndAdditional() {
        return taxAndAdditional;
    }

    public void setTaxAndAdditional(BigDecimal taxAndAdditional) {
        this.taxAndAdditional = taxAndAdditional;
    }

    public BigDecimal getNonUnitExpenditure() {
        return nonUnitExpenditure;
    }

    public void setNonUnitExpenditure(BigDecimal nonUnitExpenditure) {
        this.nonUnitExpenditure = nonUnitExpenditure;
    }

    public BigDecimal getNonUnitTotalAssets() {
        return nonUnitTotalAssets;
    }

    public void setNonUnitTotalAssets(BigDecimal nonUnitTotalAssets) {
        this.nonUnitTotalAssets = nonUnitTotalAssets;
    }

    public String getCateringFormat() {
        return cateringFormat;
    }

    public void setCateringFormat(String cateringFormat) {
        this.cateringFormat = cateringFormat == null ? null : cateringFormat.trim();
    }

    public String getConstructionQualificationCode() {
        return constructionQualificationCode;
    }

    public void setConstructionQualificationCode(String constructionQualificationCode) {
        this.constructionQualificationCode = constructionQualificationCode == null ? null : constructionQualificationCode.trim();
    }

    public Integer getRealEstateLevel() {
        return realEstateLevel;
    }

    public void setRealEstateLevel(Integer realEstateLevel) {
        this.realEstateLevel = realEstateLevel;
    }

    public Integer getIsHavaSuperior() {
        return isHavaSuperior;
    }

    public void setIsHavaSuperior(Integer isHavaSuperior) {
        this.isHavaSuperior = isHavaSuperior;
    }

    public String getSuperiorOrganizationCode() {
        return superiorOrganizationCode;
    }

    public void setSuperiorOrganizationCode(String superiorOrganizationCode) {
        this.superiorOrganizationCode = superiorOrganizationCode == null ? null : superiorOrganizationCode.trim();
    }

    public String getSuperiorSocialCreditCode() {
        return superiorSocialCreditCode;
    }

    public void setSuperiorSocialCreditCode(String superiorSocialCreditCode) {
        this.superiorSocialCreditCode = superiorSocialCreditCode == null ? null : superiorSocialCreditCode.trim();
    }

    public String getSuperiorUnitDetailedName() {
        return superiorUnitDetailedName;
    }

    public void setSuperiorUnitDetailedName(String superiorUnitDetailedName) {
        this.superiorUnitDetailedName = superiorUnitDetailedName == null ? null : superiorUnitDetailedName.trim();
    }

    public Integer getIsHaveIndustrial() {
        return isHaveIndustrial;
    }

    public void setIsHaveIndustrial(Integer isHaveIndustrial) {
        this.isHaveIndustrial = isHaveIndustrial;
    }

    public Integer getUnitTypeD() {
        return unitTypeD;
    }

    public void setUnitTypeD(Integer unitTypeD) {
        this.unitTypeD = unitTypeD;
    }

    public String getSocialCreditCodeD() {
        return socialCreditCodeD;
    }

    public void setSocialCreditCodeD(String socialCreditCodeD) {
        this.socialCreditCodeD = socialCreditCodeD == null ? null : socialCreditCodeD.trim();
    }

    public String getOrganizationCodeD() {
        return organizationCodeD;
    }

    public void setOrganizationCodeD(String organizationCodeD) {
        this.organizationCodeD = organizationCodeD == null ? null : organizationCodeD.trim();
    }

    public String getUnitDetailedNameD() {
        return unitDetailedNameD;
    }

    public void setUnitDetailedNameD(String unitDetailedNameD) {
        this.unitDetailedNameD = unitDetailedNameD == null ? null : unitDetailedNameD.trim();
    }

    public String getAddressD() {
        return addressD;
    }

    public void setAddressD(String addressD) {
        this.addressD = addressD == null ? null : addressD.trim();
    }

    public String getUnitZoningCodeD() {
        return unitZoningCodeD;
    }

    public void setUnitZoningCodeD(String unitZoningCodeD) {
        this.unitZoningCodeD = unitZoningCodeD == null ? null : unitZoningCodeD.trim();
    }

    public Integer getEmployeeD() {
        return employeeD;
    }

    public void setEmployeeD(Integer employeeD) {
        this.employeeD = employeeD;
    }

    public Integer getFemaleD() {
        return femaleD;
    }

    public void setFemaleD(Integer femaleD) {
        this.femaleD = femaleD;
    }

    public BigDecimal getOperatingIncomeD() {
        return operatingIncomeD;
    }

    public void setOperatingIncomeD(BigDecimal operatingIncomeD) {
        this.operatingIncomeD = operatingIncomeD;
    }

    public BigDecimal getNonOperatingExpenditureD() {
        return nonOperatingExpenditureD;
    }

    public void setNonOperatingExpenditureD(BigDecimal nonOperatingExpenditureD) {
        this.nonOperatingExpenditureD = nonOperatingExpenditureD;
    }

    public Float getConstructionAreaD() {
        return constructionAreaD;
    }

    public void setConstructionAreaD(Float constructionAreaD) {
        this.constructionAreaD = constructionAreaD;
    }

    public Float getSalesAreaD() {
        return salesAreaD;
    }

    public void setSalesAreaD(Float salesAreaD) {
        this.salesAreaD = salesAreaD;
    }

    public Float getAreaForSaleD() {
        return areaForSaleD;
    }

    public void setAreaForSaleD(Float areaForSaleD) {
        this.areaForSaleD = areaForSaleD;
    }

    public String getUnitHead() {
        return unitHead;
    }

    public void setUnitHead(String unitHead) {
        this.unitHead = unitHead == null ? null : unitHead.trim();
    }

    public String getStatisticalControlOfficer() {
        return statisticalControlOfficer;
    }

    public void setStatisticalControlOfficer(String statisticalControlOfficer) {
        this.statisticalControlOfficer = statisticalControlOfficer == null ? null : statisticalControlOfficer.trim();
    }

    public String getFillFormBy() {
        return fillFormBy;
    }

    public void setFillFormBy(String fillFormBy) {
        this.fillFormBy = fillFormBy == null ? null : fillFormBy.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getExtensionNumber() {
        return extensionNumber;
    }

    public void setExtensionNumber(String extensionNumber) {
        this.extensionNumber = extensionNumber == null ? null : extensionNumber.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getReportingDate() {
        return reportingDate;
    }

    public void setReportingDate(Date reportingDate) {
        this.reportingDate = reportingDate;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getMacaoInput() {
        return macaoInput;
    }

    public void setMacaoInput(BigDecimal macaoInput) {
        this.macaoInput = macaoInput;
    }

    public Integer getMacao() {
        return macao;
    }

    public void setMacao(Integer macao) {
        this.macao = macao;
    }
}