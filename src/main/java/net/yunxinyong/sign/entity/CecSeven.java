package net.yunxinyong.sign.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class CecSeven {
    private Integer id;

    private Integer invId;

    private String socialCreditCode;

    private String organizationCode;

    private String unitDetailedName;

    private BigDecimal totalEnergy;

    private BigDecimal electric;

    private BigDecimal coal;

    private BigDecimal naturalGas;

    private BigDecimal liquifiedNaturalGas;

    private BigDecimal gasoline;

    private BigDecimal dieselOil;

    private BigDecimal purchasingHeat;

    private BigDecimal totalEnergyMoney;

    private BigDecimal electricMoney;

    private BigDecimal coalMoney;

    private BigDecimal naturalGasMoney;

    private BigDecimal liquifiedNaturalGasMoney;

    private BigDecimal gasolineMoney;

    private BigDecimal dieselOilMoney;

    private BigDecimal purchasingHeatMoney;

    private String unitHead;

    private String statisticalControlOfficer;

    private String fillFormBy;

    private String phone;

    private Integer state;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvId() {
        return invId;
    }

    public void setInvId(Integer invId) {
        this.invId = invId;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName == null ? null : unitDetailedName.trim();
    }

    public BigDecimal getTotalEnergy() {
        return totalEnergy;
    }

    public void setTotalEnergy(BigDecimal totalEnergy) {
        this.totalEnergy = totalEnergy;
    }

    public BigDecimal getElectric() {
        return electric;
    }

    public void setElectric(BigDecimal electric) {
        this.electric = electric;
    }

    public BigDecimal getCoal() {
        return coal;
    }

    public void setCoal(BigDecimal coal) {
        this.coal = coal;
    }

    public BigDecimal getNaturalGas() {
        return naturalGas;
    }

    public void setNaturalGas(BigDecimal naturalGas) {
        this.naturalGas = naturalGas;
    }

    public BigDecimal getLiquifiedNaturalGas() {
        return liquifiedNaturalGas;
    }

    public void setLiquifiedNaturalGas(BigDecimal liquifiedNaturalGas) {
        this.liquifiedNaturalGas = liquifiedNaturalGas;
    }

    public BigDecimal getGasoline() {
        return gasoline;
    }

    public void setGasoline(BigDecimal gasoline) {
        this.gasoline = gasoline;
    }

    public BigDecimal getDieselOil() {
        return dieselOil;
    }

    public void setDieselOil(BigDecimal dieselOil) {
        this.dieselOil = dieselOil;
    }

    public BigDecimal getPurchasingHeat() {
        return purchasingHeat;
    }

    public void setPurchasingHeat(BigDecimal purchasingHeat) {
        this.purchasingHeat = purchasingHeat;
    }

    public BigDecimal getTotalEnergyMoney() {
        return totalEnergyMoney;
    }

    public void setTotalEnergyMoney(BigDecimal totalEnergyMoney) {
        this.totalEnergyMoney = totalEnergyMoney;
    }

    public BigDecimal getElectricMoney() {
        return electricMoney;
    }

    public void setElectricMoney(BigDecimal electricMoney) {
        this.electricMoney = electricMoney;
    }

    public BigDecimal getCoalMoney() {
        return coalMoney;
    }

    public void setCoalMoney(BigDecimal coalMoney) {
        this.coalMoney = coalMoney;
    }

    public BigDecimal getNaturalGasMoney() {
        return naturalGasMoney;
    }

    public void setNaturalGasMoney(BigDecimal naturalGasMoney) {
        this.naturalGasMoney = naturalGasMoney;
    }

    public BigDecimal getLiquifiedNaturalGasMoney() {
        return liquifiedNaturalGasMoney;
    }

    public void setLiquifiedNaturalGasMoney(BigDecimal liquifiedNaturalGasMoney) {
        this.liquifiedNaturalGasMoney = liquifiedNaturalGasMoney;
    }

    public BigDecimal getGasolineMoney() {
        return gasolineMoney;
    }

    public void setGasolineMoney(BigDecimal gasolineMoney) {
        this.gasolineMoney = gasolineMoney;
    }

    public BigDecimal getDieselOilMoney() {
        return dieselOilMoney;
    }

    public void setDieselOilMoney(BigDecimal dieselOilMoney) {
        this.dieselOilMoney = dieselOilMoney;
    }

    public BigDecimal getPurchasingHeatMoney() {
        return purchasingHeatMoney;
    }

    public void setPurchasingHeatMoney(BigDecimal purchasingHeatMoney) {
        this.purchasingHeatMoney = purchasingHeatMoney;
    }

    public String getUnitHead() {
        return unitHead;
    }

    public void setUnitHead(String unitHead) {
        this.unitHead = unitHead == null ? null : unitHead.trim();
    }

    public String getStatisticalControlOfficer() {
        return statisticalControlOfficer;
    }

    public void setStatisticalControlOfficer(String statisticalControlOfficer) {
        this.statisticalControlOfficer = statisticalControlOfficer == null ? null : statisticalControlOfficer.trim();
    }

    public String getFillFormBy() {
        return fillFormBy;
    }

    public void setFillFormBy(String fillFormBy) {
        this.fillFormBy = fillFormBy == null ? null : fillFormBy.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}