package net.yunxinyong.sign.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class CecOne {
    private Integer id;

    private Integer invId;

    private String socialCreditCode;

    private String organizationCode;

    private String unitDetailedName;

    private Integer amount;

    private Integer brancesType;

    private String brancesSocialCreditCode;

    private String brancesOrganizationCode;

    private String brancesUnitDetailedName;

    private String brancesAddress;

    private String brancesZoning;

    private String brancesContactNumber;

    private String branceMainBussinese;

    private String branceIndustryCode;

    private String finalEmployeesNumber;

    private String bussineseIncome;

    private String nonBussineseIncome;

    private Integer status;

    private Integer state;

    private String fillFormBy;

    private String phone;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private Integer formType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvId() {
        return invId;
    }

    public void setInvId(Integer invId) {
        this.invId = invId;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName == null ? null : unitDetailedName.trim();
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getBrancesType() {
        return brancesType;
    }

    public void setBrancesType(Integer brancesType) {
        this.brancesType = brancesType;
    }

    public String getBrancesSocialCreditCode() {
        return brancesSocialCreditCode;
    }

    public void setBrancesSocialCreditCode(String brancesSocialCreditCode) {
        this.brancesSocialCreditCode = brancesSocialCreditCode == null ? null : brancesSocialCreditCode.trim();
    }

    public String getBrancesOrganizationCode() {
        return brancesOrganizationCode;
    }

    public void setBrancesOrganizationCode(String brancesOrganizationCode) {
        this.brancesOrganizationCode = brancesOrganizationCode == null ? null : brancesOrganizationCode.trim();
    }

    public String getBrancesUnitDetailedName() {
        return brancesUnitDetailedName;
    }

    public void setBrancesUnitDetailedName(String brancesUnitDetailedName) {
        this.brancesUnitDetailedName = brancesUnitDetailedName == null ? null : brancesUnitDetailedName.trim();
    }

    public String getBrancesAddress() {
        return brancesAddress;
    }

    public void setBrancesAddress(String brancesAddress) {
        this.brancesAddress = brancesAddress == null ? null : brancesAddress.trim();
    }

    public String getBrancesZoning() {
        return brancesZoning;
    }

    public void setBrancesZoning(String brancesZoning) {
        this.brancesZoning = brancesZoning == null ? null : brancesZoning.trim();
    }

    public String getBrancesContactNumber() {
        return brancesContactNumber;
    }

    public void setBrancesContactNumber(String brancesContactNumber) {
        this.brancesContactNumber = brancesContactNumber == null ? null : brancesContactNumber.trim();
    }

    public String getBranceMainBussinese() {
        return branceMainBussinese;
    }

    public void setBranceMainBussinese(String branceMainBussinese) {
        this.branceMainBussinese = branceMainBussinese == null ? null : branceMainBussinese.trim();
    }

    public String getBranceIndustryCode() {
        return branceIndustryCode;
    }

    public void setBranceIndustryCode(String branceIndustryCode) {
        this.branceIndustryCode = branceIndustryCode == null ? null : branceIndustryCode.trim();
    }

    public String getFinalEmployeesNumber() {
        return finalEmployeesNumber;
    }

    public void setFinalEmployeesNumber(String finalEmployeesNumber) {
        this.finalEmployeesNumber = finalEmployeesNumber == null ? null : finalEmployeesNumber.trim();
    }

    public String getBussineseIncome() {
        return bussineseIncome;
    }

    public void setBussineseIncome(String bussineseIncome) {
        this.bussineseIncome = bussineseIncome == null ? null : bussineseIncome.trim();
    }

    public String getNonBussineseIncome() {
        return nonBussineseIncome;
    }

    public void setNonBussineseIncome(String nonBussineseIncome) {
        this.nonBussineseIncome = nonBussineseIncome == null ? null : nonBussineseIncome.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getFillFormBy() {
        return fillFormBy;
    }

    public void setFillFormBy(String fillFormBy) {
        this.fillFormBy = fillFormBy == null ? null : fillFormBy.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getFormType() {
        return formType;
    }

    public void setFormType(Integer formType) {
        this.formType = formType;
    }
}