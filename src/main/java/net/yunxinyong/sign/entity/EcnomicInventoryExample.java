package net.yunxinyong.sign.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class EcnomicInventoryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EcnomicInventoryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNull() {
            addCriterion("social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNotNull() {
            addCriterion("social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeEqualTo(String value) {
            addCriterion("social_credit_code =", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotEqualTo(String value) {
            addCriterion("social_credit_code <>", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThan(String value) {
            addCriterion("social_credit_code >", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("social_credit_code >=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThan(String value) {
            addCriterion("social_credit_code <", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("social_credit_code <=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLike(String value) {
            addCriterion("social_credit_code like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotLike(String value) {
            addCriterion("social_credit_code not like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIn(List<String> values) {
            addCriterion("social_credit_code in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotIn(List<String> values) {
            addCriterion("social_credit_code not in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("social_credit_code between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("social_credit_code not between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNull() {
            addCriterion("organization_code is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNotNull() {
            addCriterion("organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeEqualTo(String value) {
            addCriterion("organization_code =", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotEqualTo(String value) {
            addCriterion("organization_code <>", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThan(String value) {
            addCriterion("organization_code >", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("organization_code >=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThan(String value) {
            addCriterion("organization_code <", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("organization_code <=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLike(String value) {
            addCriterion("organization_code like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotLike(String value) {
            addCriterion("organization_code not like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIn(List<String> values) {
            addCriterion("organization_code in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotIn(List<String> values) {
            addCriterion("organization_code not in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeBetween(String value1, String value2) {
            addCriterion("organization_code between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("organization_code not between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNull() {
            addCriterion("unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNotNull() {
            addCriterion("unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameEqualTo(String value) {
            addCriterion("unit_detailed_name =", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotEqualTo(String value) {
            addCriterion("unit_detailed_name <>", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThan(String value) {
            addCriterion("unit_detailed_name >", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name >=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThan(String value) {
            addCriterion("unit_detailed_name <", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name <=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLike(String value) {
            addCriterion("unit_detailed_name like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotLike(String value) {
            addCriterion("unit_detailed_name not like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIn(List<String> values) {
            addCriterion("unit_detailed_name in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotIn(List<String> values) {
            addCriterion("unit_detailed_name not in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("unit_detailed_name between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("unit_detailed_name not between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeIsNull() {
            addCriterion("legal_representative is null");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeIsNotNull() {
            addCriterion("legal_representative is not null");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeEqualTo(String value) {
            addCriterion("legal_representative =", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeNotEqualTo(String value) {
            addCriterion("legal_representative <>", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeGreaterThan(String value) {
            addCriterion("legal_representative >", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeGreaterThanOrEqualTo(String value) {
            addCriterion("legal_representative >=", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeLessThan(String value) {
            addCriterion("legal_representative <", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeLessThanOrEqualTo(String value) {
            addCriterion("legal_representative <=", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeLike(String value) {
            addCriterion("legal_representative like", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeNotLike(String value) {
            addCriterion("legal_representative not like", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeIn(List<String> values) {
            addCriterion("legal_representative in", values, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeNotIn(List<String> values) {
            addCriterion("legal_representative not in", values, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeBetween(String value1, String value2) {
            addCriterion("legal_representative between", value1, value2, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeNotBetween(String value1, String value2) {
            addCriterion("legal_representative not between", value1, value2, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceIsNull() {
            addCriterion("unit_location_provience is null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceIsNotNull() {
            addCriterion("unit_location_provience is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceEqualTo(String value) {
            addCriterion("unit_location_provience =", value, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceNotEqualTo(String value) {
            addCriterion("unit_location_provience <>", value, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceGreaterThan(String value) {
            addCriterion("unit_location_provience >", value, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceGreaterThanOrEqualTo(String value) {
            addCriterion("unit_location_provience >=", value, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceLessThan(String value) {
            addCriterion("unit_location_provience <", value, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceLessThanOrEqualTo(String value) {
            addCriterion("unit_location_provience <=", value, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceLike(String value) {
            addCriterion("unit_location_provience like", value, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceNotLike(String value) {
            addCriterion("unit_location_provience not like", value, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceIn(List<String> values) {
            addCriterion("unit_location_provience in", values, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceNotIn(List<String> values) {
            addCriterion("unit_location_provience not in", values, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceBetween(String value1, String value2) {
            addCriterion("unit_location_provience between", value1, value2, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationProvienceNotBetween(String value1, String value2) {
            addCriterion("unit_location_provience not between", value1, value2, "unitLocationProvience");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityIsNull() {
            addCriterion("unit_location_city is null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityIsNotNull() {
            addCriterion("unit_location_city is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityEqualTo(String value) {
            addCriterion("unit_location_city =", value, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityNotEqualTo(String value) {
            addCriterion("unit_location_city <>", value, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityGreaterThan(String value) {
            addCriterion("unit_location_city >", value, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityGreaterThanOrEqualTo(String value) {
            addCriterion("unit_location_city >=", value, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityLessThan(String value) {
            addCriterion("unit_location_city <", value, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityLessThanOrEqualTo(String value) {
            addCriterion("unit_location_city <=", value, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityLike(String value) {
            addCriterion("unit_location_city like", value, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityNotLike(String value) {
            addCriterion("unit_location_city not like", value, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityIn(List<String> values) {
            addCriterion("unit_location_city in", values, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityNotIn(List<String> values) {
            addCriterion("unit_location_city not in", values, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityBetween(String value1, String value2) {
            addCriterion("unit_location_city between", value1, value2, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCityNotBetween(String value1, String value2) {
            addCriterion("unit_location_city not between", value1, value2, "unitLocationCity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictIsNull() {
            addCriterion("unit_location_district is null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictIsNotNull() {
            addCriterion("unit_location_district is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictEqualTo(String value) {
            addCriterion("unit_location_district =", value, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictNotEqualTo(String value) {
            addCriterion("unit_location_district <>", value, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictGreaterThan(String value) {
            addCriterion("unit_location_district >", value, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictGreaterThanOrEqualTo(String value) {
            addCriterion("unit_location_district >=", value, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictLessThan(String value) {
            addCriterion("unit_location_district <", value, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictLessThanOrEqualTo(String value) {
            addCriterion("unit_location_district <=", value, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictLike(String value) {
            addCriterion("unit_location_district like", value, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictNotLike(String value) {
            addCriterion("unit_location_district not like", value, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictIn(List<String> values) {
            addCriterion("unit_location_district in", values, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictNotIn(List<String> values) {
            addCriterion("unit_location_district not in", values, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictBetween(String value1, String value2) {
            addCriterion("unit_location_district between", value1, value2, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationDistrictNotBetween(String value1, String value2) {
            addCriterion("unit_location_district not between", value1, value2, "unitLocationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownIsNull() {
            addCriterion("unit_location_town is null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownIsNotNull() {
            addCriterion("unit_location_town is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownEqualTo(String value) {
            addCriterion("unit_location_town =", value, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownNotEqualTo(String value) {
            addCriterion("unit_location_town <>", value, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownGreaterThan(String value) {
            addCriterion("unit_location_town >", value, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownGreaterThanOrEqualTo(String value) {
            addCriterion("unit_location_town >=", value, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownLessThan(String value) {
            addCriterion("unit_location_town <", value, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownLessThanOrEqualTo(String value) {
            addCriterion("unit_location_town <=", value, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownLike(String value) {
            addCriterion("unit_location_town like", value, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownNotLike(String value) {
            addCriterion("unit_location_town not like", value, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownIn(List<String> values) {
            addCriterion("unit_location_town in", values, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownNotIn(List<String> values) {
            addCriterion("unit_location_town not in", values, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownBetween(String value1, String value2) {
            addCriterion("unit_location_town between", value1, value2, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationTownNotBetween(String value1, String value2) {
            addCriterion("unit_location_town not between", value1, value2, "unitLocationTown");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetIsNull() {
            addCriterion("unit_location_street is null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetIsNotNull() {
            addCriterion("unit_location_street is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetEqualTo(String value) {
            addCriterion("unit_location_street =", value, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetNotEqualTo(String value) {
            addCriterion("unit_location_street <>", value, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetGreaterThan(String value) {
            addCriterion("unit_location_street >", value, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetGreaterThanOrEqualTo(String value) {
            addCriterion("unit_location_street >=", value, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetLessThan(String value) {
            addCriterion("unit_location_street <", value, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetLessThanOrEqualTo(String value) {
            addCriterion("unit_location_street <=", value, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetLike(String value) {
            addCriterion("unit_location_street like", value, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetNotLike(String value) {
            addCriterion("unit_location_street not like", value, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetIn(List<String> values) {
            addCriterion("unit_location_street in", values, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetNotIn(List<String> values) {
            addCriterion("unit_location_street not in", values, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetBetween(String value1, String value2) {
            addCriterion("unit_location_street between", value1, value2, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLocationStreetNotBetween(String value1, String value2) {
            addCriterion("unit_location_street not between", value1, value2, "unitLocationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictIsNull() {
            addCriterion("unit_loaction_subdistrict is null");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictIsNotNull() {
            addCriterion("unit_loaction_subdistrict is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictEqualTo(String value) {
            addCriterion("unit_loaction_subdistrict =", value, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictNotEqualTo(String value) {
            addCriterion("unit_loaction_subdistrict <>", value, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictGreaterThan(String value) {
            addCriterion("unit_loaction_subdistrict >", value, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictGreaterThanOrEqualTo(String value) {
            addCriterion("unit_loaction_subdistrict >=", value, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictLessThan(String value) {
            addCriterion("unit_loaction_subdistrict <", value, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictLessThanOrEqualTo(String value) {
            addCriterion("unit_loaction_subdistrict <=", value, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictLike(String value) {
            addCriterion("unit_loaction_subdistrict like", value, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictNotLike(String value) {
            addCriterion("unit_loaction_subdistrict not like", value, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictIn(List<String> values) {
            addCriterion("unit_loaction_subdistrict in", values, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictNotIn(List<String> values) {
            addCriterion("unit_loaction_subdistrict not in", values, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictBetween(String value1, String value2) {
            addCriterion("unit_loaction_subdistrict between", value1, value2, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLoactionSubdistrictNotBetween(String value1, String value2) {
            addCriterion("unit_loaction_subdistrict not between", value1, value2, "unitLoactionSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityIsNull() {
            addCriterion("unit_location_cummunity is null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityIsNotNull() {
            addCriterion("unit_location_cummunity is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityEqualTo(String value) {
            addCriterion("unit_location_cummunity =", value, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityNotEqualTo(String value) {
            addCriterion("unit_location_cummunity <>", value, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityGreaterThan(String value) {
            addCriterion("unit_location_cummunity >", value, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityGreaterThanOrEqualTo(String value) {
            addCriterion("unit_location_cummunity >=", value, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityLessThan(String value) {
            addCriterion("unit_location_cummunity <", value, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityLessThanOrEqualTo(String value) {
            addCriterion("unit_location_cummunity <=", value, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityLike(String value) {
            addCriterion("unit_location_cummunity like", value, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityNotLike(String value) {
            addCriterion("unit_location_cummunity not like", value, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityIn(List<String> values) {
            addCriterion("unit_location_cummunity in", values, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityNotIn(List<String> values) {
            addCriterion("unit_location_cummunity not in", values, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityBetween(String value1, String value2) {
            addCriterion("unit_location_cummunity between", value1, value2, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationCummunityNotBetween(String value1, String value2) {
            addCriterion("unit_location_cummunity not between", value1, value2, "unitLocationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningIsNull() {
            addCriterion("unit_location_zoning is null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningIsNotNull() {
            addCriterion("unit_location_zoning is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningEqualTo(String value) {
            addCriterion("unit_location_zoning =", value, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningNotEqualTo(String value) {
            addCriterion("unit_location_zoning <>", value, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningGreaterThan(String value) {
            addCriterion("unit_location_zoning >", value, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningGreaterThanOrEqualTo(String value) {
            addCriterion("unit_location_zoning >=", value, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningLessThan(String value) {
            addCriterion("unit_location_zoning <", value, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningLessThanOrEqualTo(String value) {
            addCriterion("unit_location_zoning <=", value, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningLike(String value) {
            addCriterion("unit_location_zoning like", value, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningNotLike(String value) {
            addCriterion("unit_location_zoning not like", value, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningIn(List<String> values) {
            addCriterion("unit_location_zoning in", values, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningNotIn(List<String> values) {
            addCriterion("unit_location_zoning not in", values, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningBetween(String value1, String value2) {
            addCriterion("unit_location_zoning between", value1, value2, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationZoningNotBetween(String value1, String value2) {
            addCriterion("unit_location_zoning not between", value1, value2, "unitLocationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeIsNull() {
            addCriterion("unit_location_villageCode is null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeIsNotNull() {
            addCriterion("unit_location_villageCode is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeEqualTo(String value) {
            addCriterion("unit_location_villageCode =", value, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeNotEqualTo(String value) {
            addCriterion("unit_location_villageCode <>", value, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeGreaterThan(String value) {
            addCriterion("unit_location_villageCode >", value, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeGreaterThanOrEqualTo(String value) {
            addCriterion("unit_location_villageCode >=", value, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeLessThan(String value) {
            addCriterion("unit_location_villageCode <", value, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeLessThanOrEqualTo(String value) {
            addCriterion("unit_location_villageCode <=", value, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeLike(String value) {
            addCriterion("unit_location_villageCode like", value, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeNotLike(String value) {
            addCriterion("unit_location_villageCode not like", value, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeIn(List<String> values) {
            addCriterion("unit_location_villageCode in", values, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeNotIn(List<String> values) {
            addCriterion("unit_location_villageCode not in", values, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeBetween(String value1, String value2) {
            addCriterion("unit_location_villageCode between", value1, value2, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationVillagecodeNotBetween(String value1, String value2) {
            addCriterion("unit_location_villageCode not between", value1, value2, "unitLocationVillagecode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeIsNull() {
            addCriterion("unit_location_constructionCode is null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeIsNotNull() {
            addCriterion("unit_location_constructionCode is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeEqualTo(String value) {
            addCriterion("unit_location_constructionCode =", value, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeNotEqualTo(String value) {
            addCriterion("unit_location_constructionCode <>", value, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeGreaterThan(String value) {
            addCriterion("unit_location_constructionCode >", value, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeGreaterThanOrEqualTo(String value) {
            addCriterion("unit_location_constructionCode >=", value, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeLessThan(String value) {
            addCriterion("unit_location_constructionCode <", value, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeLessThanOrEqualTo(String value) {
            addCriterion("unit_location_constructionCode <=", value, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeLike(String value) {
            addCriterion("unit_location_constructionCode like", value, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeNotLike(String value) {
            addCriterion("unit_location_constructionCode not like", value, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeIn(List<String> values) {
            addCriterion("unit_location_constructionCode in", values, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeNotIn(List<String> values) {
            addCriterion("unit_location_constructionCode not in", values, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeBetween(String value1, String value2) {
            addCriterion("unit_location_constructionCode between", value1, value2, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andUnitLocationConstructioncodeNotBetween(String value1, String value2) {
            addCriterion("unit_location_constructionCode not between", value1, value2, "unitLocationConstructioncode");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameIsNull() {
            addCriterion("park_location_name is null");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameIsNotNull() {
            addCriterion("park_location_name is not null");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameEqualTo(String value) {
            addCriterion("park_location_name =", value, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameNotEqualTo(String value) {
            addCriterion("park_location_name <>", value, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameGreaterThan(String value) {
            addCriterion("park_location_name >", value, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameGreaterThanOrEqualTo(String value) {
            addCriterion("park_location_name >=", value, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameLessThan(String value) {
            addCriterion("park_location_name <", value, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameLessThanOrEqualTo(String value) {
            addCriterion("park_location_name <=", value, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameLike(String value) {
            addCriterion("park_location_name like", value, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameNotLike(String value) {
            addCriterion("park_location_name not like", value, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameIn(List<String> values) {
            addCriterion("park_location_name in", values, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameNotIn(List<String> values) {
            addCriterion("park_location_name not in", values, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameBetween(String value1, String value2) {
            addCriterion("park_location_name between", value1, value2, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationNameNotBetween(String value1, String value2) {
            addCriterion("park_location_name not between", value1, value2, "parkLocationName");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeIsNull() {
            addCriterion("park_location_code is null");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeIsNotNull() {
            addCriterion("park_location_code is not null");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeEqualTo(String value) {
            addCriterion("park_location_code =", value, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeNotEqualTo(String value) {
            addCriterion("park_location_code <>", value, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeGreaterThan(String value) {
            addCriterion("park_location_code >", value, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("park_location_code >=", value, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeLessThan(String value) {
            addCriterion("park_location_code <", value, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeLessThanOrEqualTo(String value) {
            addCriterion("park_location_code <=", value, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeLike(String value) {
            addCriterion("park_location_code like", value, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeNotLike(String value) {
            addCriterion("park_location_code not like", value, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeIn(List<String> values) {
            addCriterion("park_location_code in", values, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeNotIn(List<String> values) {
            addCriterion("park_location_code not in", values, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeBetween(String value1, String value2) {
            addCriterion("park_location_code between", value1, value2, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andParkLocationCodeNotBetween(String value1, String value2) {
            addCriterion("park_location_code not between", value1, value2, "parkLocationCode");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceIsNull() {
            addCriterion("unit_registration_province is null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceIsNotNull() {
            addCriterion("unit_registration_province is not null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceEqualTo(String value) {
            addCriterion("unit_registration_province =", value, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceNotEqualTo(String value) {
            addCriterion("unit_registration_province <>", value, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceGreaterThan(String value) {
            addCriterion("unit_registration_province >", value, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("unit_registration_province >=", value, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceLessThan(String value) {
            addCriterion("unit_registration_province <", value, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceLessThanOrEqualTo(String value) {
            addCriterion("unit_registration_province <=", value, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceLike(String value) {
            addCriterion("unit_registration_province like", value, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceNotLike(String value) {
            addCriterion("unit_registration_province not like", value, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceIn(List<String> values) {
            addCriterion("unit_registration_province in", values, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceNotIn(List<String> values) {
            addCriterion("unit_registration_province not in", values, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceBetween(String value1, String value2) {
            addCriterion("unit_registration_province between", value1, value2, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationProvinceNotBetween(String value1, String value2) {
            addCriterion("unit_registration_province not between", value1, value2, "unitRegistrationProvince");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityIsNull() {
            addCriterion("unit_registration_city is null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityIsNotNull() {
            addCriterion("unit_registration_city is not null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityEqualTo(String value) {
            addCriterion("unit_registration_city =", value, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityNotEqualTo(String value) {
            addCriterion("unit_registration_city <>", value, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityGreaterThan(String value) {
            addCriterion("unit_registration_city >", value, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityGreaterThanOrEqualTo(String value) {
            addCriterion("unit_registration_city >=", value, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityLessThan(String value) {
            addCriterion("unit_registration_city <", value, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityLessThanOrEqualTo(String value) {
            addCriterion("unit_registration_city <=", value, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityLike(String value) {
            addCriterion("unit_registration_city like", value, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityNotLike(String value) {
            addCriterion("unit_registration_city not like", value, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityIn(List<String> values) {
            addCriterion("unit_registration_city in", values, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityNotIn(List<String> values) {
            addCriterion("unit_registration_city not in", values, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityBetween(String value1, String value2) {
            addCriterion("unit_registration_city between", value1, value2, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCityNotBetween(String value1, String value2) {
            addCriterion("unit_registration_city not between", value1, value2, "unitRegistrationCity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictIsNull() {
            addCriterion("unit_registration_district is null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictIsNotNull() {
            addCriterion("unit_registration_district is not null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictEqualTo(String value) {
            addCriterion("unit_registration_district =", value, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictNotEqualTo(String value) {
            addCriterion("unit_registration_district <>", value, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictGreaterThan(String value) {
            addCriterion("unit_registration_district >", value, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictGreaterThanOrEqualTo(String value) {
            addCriterion("unit_registration_district >=", value, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictLessThan(String value) {
            addCriterion("unit_registration_district <", value, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictLessThanOrEqualTo(String value) {
            addCriterion("unit_registration_district <=", value, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictLike(String value) {
            addCriterion("unit_registration_district like", value, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictNotLike(String value) {
            addCriterion("unit_registration_district not like", value, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictIn(List<String> values) {
            addCriterion("unit_registration_district in", values, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictNotIn(List<String> values) {
            addCriterion("unit_registration_district not in", values, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictBetween(String value1, String value2) {
            addCriterion("unit_registration_district between", value1, value2, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationDistrictNotBetween(String value1, String value2) {
            addCriterion("unit_registration_district not between", value1, value2, "unitRegistrationDistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownIsNull() {
            addCriterion("unit_registration_town is null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownIsNotNull() {
            addCriterion("unit_registration_town is not null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownEqualTo(String value) {
            addCriterion("unit_registration_town =", value, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownNotEqualTo(String value) {
            addCriterion("unit_registration_town <>", value, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownGreaterThan(String value) {
            addCriterion("unit_registration_town >", value, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownGreaterThanOrEqualTo(String value) {
            addCriterion("unit_registration_town >=", value, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownLessThan(String value) {
            addCriterion("unit_registration_town <", value, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownLessThanOrEqualTo(String value) {
            addCriterion("unit_registration_town <=", value, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownLike(String value) {
            addCriterion("unit_registration_town like", value, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownNotLike(String value) {
            addCriterion("unit_registration_town not like", value, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownIn(List<String> values) {
            addCriterion("unit_registration_town in", values, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownNotIn(List<String> values) {
            addCriterion("unit_registration_town not in", values, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownBetween(String value1, String value2) {
            addCriterion("unit_registration_town between", value1, value2, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationTownNotBetween(String value1, String value2) {
            addCriterion("unit_registration_town not between", value1, value2, "unitRegistrationTown");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetIsNull() {
            addCriterion("unit_registration_street is null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetIsNotNull() {
            addCriterion("unit_registration_street is not null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetEqualTo(String value) {
            addCriterion("unit_registration_street =", value, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetNotEqualTo(String value) {
            addCriterion("unit_registration_street <>", value, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetGreaterThan(String value) {
            addCriterion("unit_registration_street >", value, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetGreaterThanOrEqualTo(String value) {
            addCriterion("unit_registration_street >=", value, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetLessThan(String value) {
            addCriterion("unit_registration_street <", value, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetLessThanOrEqualTo(String value) {
            addCriterion("unit_registration_street <=", value, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetLike(String value) {
            addCriterion("unit_registration_street like", value, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetNotLike(String value) {
            addCriterion("unit_registration_street not like", value, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetIn(List<String> values) {
            addCriterion("unit_registration_street in", values, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetNotIn(List<String> values) {
            addCriterion("unit_registration_street not in", values, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetBetween(String value1, String value2) {
            addCriterion("unit_registration_street between", value1, value2, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationStreetNotBetween(String value1, String value2) {
            addCriterion("unit_registration_street not between", value1, value2, "unitRegistrationStreet");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictIsNull() {
            addCriterion("unit_registration_subdistrict is null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictIsNotNull() {
            addCriterion("unit_registration_subdistrict is not null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictEqualTo(String value) {
            addCriterion("unit_registration_subdistrict =", value, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictNotEqualTo(String value) {
            addCriterion("unit_registration_subdistrict <>", value, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictGreaterThan(String value) {
            addCriterion("unit_registration_subdistrict >", value, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictGreaterThanOrEqualTo(String value) {
            addCriterion("unit_registration_subdistrict >=", value, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictLessThan(String value) {
            addCriterion("unit_registration_subdistrict <", value, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictLessThanOrEqualTo(String value) {
            addCriterion("unit_registration_subdistrict <=", value, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictLike(String value) {
            addCriterion("unit_registration_subdistrict like", value, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictNotLike(String value) {
            addCriterion("unit_registration_subdistrict not like", value, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictIn(List<String> values) {
            addCriterion("unit_registration_subdistrict in", values, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictNotIn(List<String> values) {
            addCriterion("unit_registration_subdistrict not in", values, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictBetween(String value1, String value2) {
            addCriterion("unit_registration_subdistrict between", value1, value2, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationSubdistrictNotBetween(String value1, String value2) {
            addCriterion("unit_registration_subdistrict not between", value1, value2, "unitRegistrationSubdistrict");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityIsNull() {
            addCriterion("unit_registration_cummunity is null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityIsNotNull() {
            addCriterion("unit_registration_cummunity is not null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityEqualTo(String value) {
            addCriterion("unit_registration_cummunity =", value, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityNotEqualTo(String value) {
            addCriterion("unit_registration_cummunity <>", value, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityGreaterThan(String value) {
            addCriterion("unit_registration_cummunity >", value, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityGreaterThanOrEqualTo(String value) {
            addCriterion("unit_registration_cummunity >=", value, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityLessThan(String value) {
            addCriterion("unit_registration_cummunity <", value, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityLessThanOrEqualTo(String value) {
            addCriterion("unit_registration_cummunity <=", value, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityLike(String value) {
            addCriterion("unit_registration_cummunity like", value, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityNotLike(String value) {
            addCriterion("unit_registration_cummunity not like", value, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityIn(List<String> values) {
            addCriterion("unit_registration_cummunity in", values, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityNotIn(List<String> values) {
            addCriterion("unit_registration_cummunity not in", values, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityBetween(String value1, String value2) {
            addCriterion("unit_registration_cummunity between", value1, value2, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationCummunityNotBetween(String value1, String value2) {
            addCriterion("unit_registration_cummunity not between", value1, value2, "unitRegistrationCummunity");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningIsNull() {
            addCriterion("unit_registration_zoning is null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningIsNotNull() {
            addCriterion("unit_registration_zoning is not null");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningEqualTo(String value) {
            addCriterion("unit_registration_zoning =", value, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningNotEqualTo(String value) {
            addCriterion("unit_registration_zoning <>", value, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningGreaterThan(String value) {
            addCriterion("unit_registration_zoning >", value, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningGreaterThanOrEqualTo(String value) {
            addCriterion("unit_registration_zoning >=", value, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningLessThan(String value) {
            addCriterion("unit_registration_zoning <", value, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningLessThanOrEqualTo(String value) {
            addCriterion("unit_registration_zoning <=", value, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningLike(String value) {
            addCriterion("unit_registration_zoning like", value, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningNotLike(String value) {
            addCriterion("unit_registration_zoning not like", value, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningIn(List<String> values) {
            addCriterion("unit_registration_zoning in", values, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningNotIn(List<String> values) {
            addCriterion("unit_registration_zoning not in", values, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningBetween(String value1, String value2) {
            addCriterion("unit_registration_zoning between", value1, value2, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andUnitRegistrationZoningNotBetween(String value1, String value2) {
            addCriterion("unit_registration_zoning not between", value1, value2, "unitRegistrationZoning");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameIsNull() {
            addCriterion("park_registration_name is null");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameIsNotNull() {
            addCriterion("park_registration_name is not null");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameEqualTo(String value) {
            addCriterion("park_registration_name =", value, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameNotEqualTo(String value) {
            addCriterion("park_registration_name <>", value, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameGreaterThan(String value) {
            addCriterion("park_registration_name >", value, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameGreaterThanOrEqualTo(String value) {
            addCriterion("park_registration_name >=", value, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameLessThan(String value) {
            addCriterion("park_registration_name <", value, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameLessThanOrEqualTo(String value) {
            addCriterion("park_registration_name <=", value, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameLike(String value) {
            addCriterion("park_registration_name like", value, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameNotLike(String value) {
            addCriterion("park_registration_name not like", value, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameIn(List<String> values) {
            addCriterion("park_registration_name in", values, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameNotIn(List<String> values) {
            addCriterion("park_registration_name not in", values, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameBetween(String value1, String value2) {
            addCriterion("park_registration_name between", value1, value2, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationNameNotBetween(String value1, String value2) {
            addCriterion("park_registration_name not between", value1, value2, "parkRegistrationName");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeIsNull() {
            addCriterion("park_registration_code is null");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeIsNotNull() {
            addCriterion("park_registration_code is not null");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeEqualTo(String value) {
            addCriterion("park_registration_code =", value, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeNotEqualTo(String value) {
            addCriterion("park_registration_code <>", value, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeGreaterThan(String value) {
            addCriterion("park_registration_code >", value, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("park_registration_code >=", value, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeLessThan(String value) {
            addCriterion("park_registration_code <", value, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeLessThanOrEqualTo(String value) {
            addCriterion("park_registration_code <=", value, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeLike(String value) {
            addCriterion("park_registration_code like", value, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeNotLike(String value) {
            addCriterion("park_registration_code not like", value, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeIn(List<String> values) {
            addCriterion("park_registration_code in", values, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeNotIn(List<String> values) {
            addCriterion("park_registration_code not in", values, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeBetween(String value1, String value2) {
            addCriterion("park_registration_code between", value1, value2, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andParkRegistrationCodeNotBetween(String value1, String value2) {
            addCriterion("park_registration_code not between", value1, value2, "parkRegistrationCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIsNull() {
            addCriterion("area_code is null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIsNotNull() {
            addCriterion("area_code is not null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeEqualTo(String value) {
            addCriterion("area_code =", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotEqualTo(String value) {
            addCriterion("area_code <>", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThan(String value) {
            addCriterion("area_code >", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThanOrEqualTo(String value) {
            addCriterion("area_code >=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThan(String value) {
            addCriterion("area_code <", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThanOrEqualTo(String value) {
            addCriterion("area_code <=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLike(String value) {
            addCriterion("area_code like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotLike(String value) {
            addCriterion("area_code not like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIn(List<String> values) {
            addCriterion("area_code in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotIn(List<String> values) {
            addCriterion("area_code not in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeBetween(String value1, String value2) {
            addCriterion("area_code between", value1, value2, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotBetween(String value1, String value2) {
            addCriterion("area_code not between", value1, value2, "areaCode");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneIsNull() {
            addCriterion("fixed_line_telephone is null");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneIsNotNull() {
            addCriterion("fixed_line_telephone is not null");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneEqualTo(String value) {
            addCriterion("fixed_line_telephone =", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneNotEqualTo(String value) {
            addCriterion("fixed_line_telephone <>", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneGreaterThan(String value) {
            addCriterion("fixed_line_telephone >", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("fixed_line_telephone >=", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneLessThan(String value) {
            addCriterion("fixed_line_telephone <", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneLessThanOrEqualTo(String value) {
            addCriterion("fixed_line_telephone <=", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneLike(String value) {
            addCriterion("fixed_line_telephone like", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneNotLike(String value) {
            addCriterion("fixed_line_telephone not like", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneIn(List<String> values) {
            addCriterion("fixed_line_telephone in", values, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneNotIn(List<String> values) {
            addCriterion("fixed_line_telephone not in", values, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneBetween(String value1, String value2) {
            addCriterion("fixed_line_telephone between", value1, value2, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneNotBetween(String value1, String value2) {
            addCriterion("fixed_line_telephone not between", value1, value2, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberIsNull() {
            addCriterion("cell_phone_number is null");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberIsNotNull() {
            addCriterion("cell_phone_number is not null");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberEqualTo(String value) {
            addCriterion("cell_phone_number =", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberNotEqualTo(String value) {
            addCriterion("cell_phone_number <>", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberGreaterThan(String value) {
            addCriterion("cell_phone_number >", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberGreaterThanOrEqualTo(String value) {
            addCriterion("cell_phone_number >=", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberLessThan(String value) {
            addCriterion("cell_phone_number <", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberLessThanOrEqualTo(String value) {
            addCriterion("cell_phone_number <=", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberLike(String value) {
            addCriterion("cell_phone_number like", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberNotLike(String value) {
            addCriterion("cell_phone_number not like", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberIn(List<String> values) {
            addCriterion("cell_phone_number in", values, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberNotIn(List<String> values) {
            addCriterion("cell_phone_number not in", values, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberBetween(String value1, String value2) {
            addCriterion("cell_phone_number between", value1, value2, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberNotBetween(String value1, String value2) {
            addCriterion("cell_phone_number not between", value1, value2, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1IsNull() {
            addCriterion("major_business1 is null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1IsNotNull() {
            addCriterion("major_business1 is not null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1EqualTo(String value) {
            addCriterion("major_business1 =", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1NotEqualTo(String value) {
            addCriterion("major_business1 <>", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1GreaterThan(String value) {
            addCriterion("major_business1 >", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1GreaterThanOrEqualTo(String value) {
            addCriterion("major_business1 >=", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1LessThan(String value) {
            addCriterion("major_business1 <", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1LessThanOrEqualTo(String value) {
            addCriterion("major_business1 <=", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1Like(String value) {
            addCriterion("major_business1 like", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1NotLike(String value) {
            addCriterion("major_business1 not like", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1In(List<String> values) {
            addCriterion("major_business1 in", values, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1NotIn(List<String> values) {
            addCriterion("major_business1 not in", values, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1Between(String value1, String value2) {
            addCriterion("major_business1 between", value1, value2, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1NotBetween(String value1, String value2) {
            addCriterion("major_business1 not between", value1, value2, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2IsNull() {
            addCriterion("major_business2 is null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2IsNotNull() {
            addCriterion("major_business2 is not null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2EqualTo(String value) {
            addCriterion("major_business2 =", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2NotEqualTo(String value) {
            addCriterion("major_business2 <>", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2GreaterThan(String value) {
            addCriterion("major_business2 >", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2GreaterThanOrEqualTo(String value) {
            addCriterion("major_business2 >=", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2LessThan(String value) {
            addCriterion("major_business2 <", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2LessThanOrEqualTo(String value) {
            addCriterion("major_business2 <=", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2Like(String value) {
            addCriterion("major_business2 like", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2NotLike(String value) {
            addCriterion("major_business2 not like", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2In(List<String> values) {
            addCriterion("major_business2 in", values, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2NotIn(List<String> values) {
            addCriterion("major_business2 not in", values, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2Between(String value1, String value2) {
            addCriterion("major_business2 between", value1, value2, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2NotBetween(String value1, String value2) {
            addCriterion("major_business2 not between", value1, value2, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3IsNull() {
            addCriterion("major_business3 is null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3IsNotNull() {
            addCriterion("major_business3 is not null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3EqualTo(String value) {
            addCriterion("major_business3 =", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3NotEqualTo(String value) {
            addCriterion("major_business3 <>", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3GreaterThan(String value) {
            addCriterion("major_business3 >", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3GreaterThanOrEqualTo(String value) {
            addCriterion("major_business3 >=", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3LessThan(String value) {
            addCriterion("major_business3 <", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3LessThanOrEqualTo(String value) {
            addCriterion("major_business3 <=", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3Like(String value) {
            addCriterion("major_business3 like", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3NotLike(String value) {
            addCriterion("major_business3 not like", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3In(List<String> values) {
            addCriterion("major_business3 in", values, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3NotIn(List<String> values) {
            addCriterion("major_business3 not in", values, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3Between(String value1, String value2) {
            addCriterion("major_business3 between", value1, value2, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3NotBetween(String value1, String value2) {
            addCriterion("major_business3 not between", value1, value2, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeIsNull() {
            addCriterion("industry_code is null");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeIsNotNull() {
            addCriterion("industry_code is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeEqualTo(String value) {
            addCriterion("industry_code =", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotEqualTo(String value) {
            addCriterion("industry_code <>", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeGreaterThan(String value) {
            addCriterion("industry_code >", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeGreaterThanOrEqualTo(String value) {
            addCriterion("industry_code >=", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeLessThan(String value) {
            addCriterion("industry_code <", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeLessThanOrEqualTo(String value) {
            addCriterion("industry_code <=", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeLike(String value) {
            addCriterion("industry_code like", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotLike(String value) {
            addCriterion("industry_code not like", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeIn(List<String> values) {
            addCriterion("industry_code in", values, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotIn(List<String> values) {
            addCriterion("industry_code not in", values, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeBetween(String value1, String value2) {
            addCriterion("industry_code between", value1, value2, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotBetween(String value1, String value2) {
            addCriterion("industry_code not between", value1, value2, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryNameIsNull() {
            addCriterion("industry_name is null");
            return (Criteria) this;
        }

        public Criteria andIndustryNameIsNotNull() {
            addCriterion("industry_name is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryNameEqualTo(String value) {
            addCriterion("industry_name =", value, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameNotEqualTo(String value) {
            addCriterion("industry_name <>", value, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameGreaterThan(String value) {
            addCriterion("industry_name >", value, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameGreaterThanOrEqualTo(String value) {
            addCriterion("industry_name >=", value, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameLessThan(String value) {
            addCriterion("industry_name <", value, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameLessThanOrEqualTo(String value) {
            addCriterion("industry_name <=", value, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameLike(String value) {
            addCriterion("industry_name like", value, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameNotLike(String value) {
            addCriterion("industry_name not like", value, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameIn(List<String> values) {
            addCriterion("industry_name in", values, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameNotIn(List<String> values) {
            addCriterion("industry_name not in", values, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameBetween(String value1, String value2) {
            addCriterion("industry_name between", value1, value2, "industryName");
            return (Criteria) this;
        }

        public Criteria andIndustryNameNotBetween(String value1, String value2) {
            addCriterion("industry_name not between", value1, value2, "industryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1IsNull() {
            addCriterion("verified_major_business1 is null");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1IsNotNull() {
            addCriterion("verified_major_business1 is not null");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1EqualTo(String value) {
            addCriterion("verified_major_business1 =", value, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1NotEqualTo(String value) {
            addCriterion("verified_major_business1 <>", value, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1GreaterThan(String value) {
            addCriterion("verified_major_business1 >", value, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1GreaterThanOrEqualTo(String value) {
            addCriterion("verified_major_business1 >=", value, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1LessThan(String value) {
            addCriterion("verified_major_business1 <", value, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1LessThanOrEqualTo(String value) {
            addCriterion("verified_major_business1 <=", value, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1Like(String value) {
            addCriterion("verified_major_business1 like", value, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1NotLike(String value) {
            addCriterion("verified_major_business1 not like", value, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1In(List<String> values) {
            addCriterion("verified_major_business1 in", values, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1NotIn(List<String> values) {
            addCriterion("verified_major_business1 not in", values, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1Between(String value1, String value2) {
            addCriterion("verified_major_business1 between", value1, value2, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness1NotBetween(String value1, String value2) {
            addCriterion("verified_major_business1 not between", value1, value2, "verifiedMajorBusiness1");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2IsNull() {
            addCriterion("verified_major_business2 is null");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2IsNotNull() {
            addCriterion("verified_major_business2 is not null");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2EqualTo(String value) {
            addCriterion("verified_major_business2 =", value, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2NotEqualTo(String value) {
            addCriterion("verified_major_business2 <>", value, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2GreaterThan(String value) {
            addCriterion("verified_major_business2 >", value, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2GreaterThanOrEqualTo(String value) {
            addCriterion("verified_major_business2 >=", value, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2LessThan(String value) {
            addCriterion("verified_major_business2 <", value, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2LessThanOrEqualTo(String value) {
            addCriterion("verified_major_business2 <=", value, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2Like(String value) {
            addCriterion("verified_major_business2 like", value, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2NotLike(String value) {
            addCriterion("verified_major_business2 not like", value, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2In(List<String> values) {
            addCriterion("verified_major_business2 in", values, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2NotIn(List<String> values) {
            addCriterion("verified_major_business2 not in", values, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2Between(String value1, String value2) {
            addCriterion("verified_major_business2 between", value1, value2, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness2NotBetween(String value1, String value2) {
            addCriterion("verified_major_business2 not between", value1, value2, "verifiedMajorBusiness2");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3IsNull() {
            addCriterion("verified_major_business3 is null");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3IsNotNull() {
            addCriterion("verified_major_business3 is not null");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3EqualTo(String value) {
            addCriterion("verified_major_business3 =", value, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3NotEqualTo(String value) {
            addCriterion("verified_major_business3 <>", value, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3GreaterThan(String value) {
            addCriterion("verified_major_business3 >", value, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3GreaterThanOrEqualTo(String value) {
            addCriterion("verified_major_business3 >=", value, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3LessThan(String value) {
            addCriterion("verified_major_business3 <", value, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3LessThanOrEqualTo(String value) {
            addCriterion("verified_major_business3 <=", value, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3Like(String value) {
            addCriterion("verified_major_business3 like", value, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3NotLike(String value) {
            addCriterion("verified_major_business3 not like", value, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3In(List<String> values) {
            addCriterion("verified_major_business3 in", values, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3NotIn(List<String> values) {
            addCriterion("verified_major_business3 not in", values, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3Between(String value1, String value2) {
            addCriterion("verified_major_business3 between", value1, value2, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedMajorBusiness3NotBetween(String value1, String value2) {
            addCriterion("verified_major_business3 not between", value1, value2, "verifiedMajorBusiness3");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeIsNull() {
            addCriterion("verified_industry_code is null");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeIsNotNull() {
            addCriterion("verified_industry_code is not null");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeEqualTo(String value) {
            addCriterion("verified_industry_code =", value, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeNotEqualTo(String value) {
            addCriterion("verified_industry_code <>", value, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeGreaterThan(String value) {
            addCriterion("verified_industry_code >", value, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeGreaterThanOrEqualTo(String value) {
            addCriterion("verified_industry_code >=", value, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeLessThan(String value) {
            addCriterion("verified_industry_code <", value, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeLessThanOrEqualTo(String value) {
            addCriterion("verified_industry_code <=", value, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeLike(String value) {
            addCriterion("verified_industry_code like", value, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeNotLike(String value) {
            addCriterion("verified_industry_code not like", value, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeIn(List<String> values) {
            addCriterion("verified_industry_code in", values, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeNotIn(List<String> values) {
            addCriterion("verified_industry_code not in", values, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeBetween(String value1, String value2) {
            addCriterion("verified_industry_code between", value1, value2, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryCodeNotBetween(String value1, String value2) {
            addCriterion("verified_industry_code not between", value1, value2, "verifiedIndustryCode");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameIsNull() {
            addCriterion("verified_industry_name is null");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameIsNotNull() {
            addCriterion("verified_industry_name is not null");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameEqualTo(String value) {
            addCriterion("verified_industry_name =", value, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameNotEqualTo(String value) {
            addCriterion("verified_industry_name <>", value, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameGreaterThan(String value) {
            addCriterion("verified_industry_name >", value, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameGreaterThanOrEqualTo(String value) {
            addCriterion("verified_industry_name >=", value, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameLessThan(String value) {
            addCriterion("verified_industry_name <", value, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameLessThanOrEqualTo(String value) {
            addCriterion("verified_industry_name <=", value, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameLike(String value) {
            addCriterion("verified_industry_name like", value, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameNotLike(String value) {
            addCriterion("verified_industry_name not like", value, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameIn(List<String> values) {
            addCriterion("verified_industry_name in", values, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameNotIn(List<String> values) {
            addCriterion("verified_industry_name not in", values, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameBetween(String value1, String value2) {
            addCriterion("verified_industry_name between", value1, value2, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andVerifiedIndustryNameNotBetween(String value1, String value2) {
            addCriterion("verified_industry_name not between", value1, value2, "verifiedIndustryName");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeIsNull() {
            addCriterion("mechanism_type is null");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeIsNotNull() {
            addCriterion("mechanism_type is not null");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeEqualTo(String value) {
            addCriterion("mechanism_type =", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeNotEqualTo(String value) {
            addCriterion("mechanism_type <>", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeGreaterThan(String value) {
            addCriterion("mechanism_type >", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeGreaterThanOrEqualTo(String value) {
            addCriterion("mechanism_type >=", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeLessThan(String value) {
            addCriterion("mechanism_type <", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeLessThanOrEqualTo(String value) {
            addCriterion("mechanism_type <=", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeLike(String value) {
            addCriterion("mechanism_type like", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeNotLike(String value) {
            addCriterion("mechanism_type not like", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeIn(List<String> values) {
            addCriterion("mechanism_type in", values, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeNotIn(List<String> values) {
            addCriterion("mechanism_type not in", values, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeBetween(String value1, String value2) {
            addCriterion("mechanism_type between", value1, value2, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeNotBetween(String value1, String value2) {
            addCriterion("mechanism_type not between", value1, value2, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeIsNull() {
            addCriterion("establishment_time is null");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeIsNotNull() {
            addCriterion("establishment_time is not null");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeEqualTo(Date value) {
            addCriterionForJDBCDate("establishment_time =", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("establishment_time <>", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("establishment_time >", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("establishment_time >=", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeLessThan(Date value) {
            addCriterionForJDBCDate("establishment_time <", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("establishment_time <=", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeIn(List<Date> values) {
            addCriterionForJDBCDate("establishment_time in", values, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("establishment_time not in", values, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("establishment_time between", value1, value2, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("establishment_time not between", value1, value2, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andBusinessStateIsNull() {
            addCriterion("business_state is null");
            return (Criteria) this;
        }

        public Criteria andBusinessStateIsNotNull() {
            addCriterion("business_state is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessStateEqualTo(String value) {
            addCriterion("business_state =", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotEqualTo(String value) {
            addCriterion("business_state <>", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateGreaterThan(String value) {
            addCriterion("business_state >", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateGreaterThanOrEqualTo(String value) {
            addCriterion("business_state >=", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateLessThan(String value) {
            addCriterion("business_state <", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateLessThanOrEqualTo(String value) {
            addCriterion("business_state <=", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateLike(String value) {
            addCriterion("business_state like", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotLike(String value) {
            addCriterion("business_state not like", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateIn(List<String> values) {
            addCriterion("business_state in", values, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotIn(List<String> values) {
            addCriterion("business_state not in", values, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateBetween(String value1, String value2) {
            addCriterion("business_state between", value1, value2, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotBetween(String value1, String value2) {
            addCriterion("business_state not between", value1, value2, "businessState");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIsNull() {
            addCriterion("unit_type is null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIsNotNull() {
            addCriterion("unit_type is not null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeEqualTo(String value) {
            addCriterion("unit_type =", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotEqualTo(String value) {
            addCriterion("unit_type <>", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeGreaterThan(String value) {
            addCriterion("unit_type >", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeGreaterThanOrEqualTo(String value) {
            addCriterion("unit_type >=", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLessThan(String value) {
            addCriterion("unit_type <", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLessThanOrEqualTo(String value) {
            addCriterion("unit_type <=", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLike(String value) {
            addCriterion("unit_type like", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotLike(String value) {
            addCriterion("unit_type not like", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIn(List<String> values) {
            addCriterion("unit_type in", values, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotIn(List<String> values) {
            addCriterion("unit_type not in", values, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeBetween(String value1, String value2) {
            addCriterion("unit_type between", value1, value2, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotBetween(String value1, String value2) {
            addCriterion("unit_type not between", value1, value2, "unitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeIsNull() {
            addCriterion("verified_unit_type is null");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeIsNotNull() {
            addCriterion("verified_unit_type is not null");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeEqualTo(Integer value) {
            addCriterion("verified_unit_type =", value, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeNotEqualTo(Integer value) {
            addCriterion("verified_unit_type <>", value, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeGreaterThan(Integer value) {
            addCriterion("verified_unit_type >", value, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("verified_unit_type >=", value, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeLessThan(Integer value) {
            addCriterion("verified_unit_type <", value, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeLessThanOrEqualTo(Integer value) {
            addCriterion("verified_unit_type <=", value, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeIn(List<Integer> values) {
            addCriterion("verified_unit_type in", values, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeNotIn(List<Integer> values) {
            addCriterion("verified_unit_type not in", values, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeBetween(Integer value1, Integer value2) {
            addCriterion("verified_unit_type between", value1, value2, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andVerifiedUnitTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("verified_unit_type not between", value1, value2, "verifiedUnitType");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryIsNull() {
            addCriterion("accounting_standard_category is null");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryIsNotNull() {
            addCriterion("accounting_standard_category is not null");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryEqualTo(String value) {
            addCriterion("accounting_standard_category =", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryNotEqualTo(String value) {
            addCriterion("accounting_standard_category <>", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryGreaterThan(String value) {
            addCriterion("accounting_standard_category >", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryGreaterThanOrEqualTo(String value) {
            addCriterion("accounting_standard_category >=", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryLessThan(String value) {
            addCriterion("accounting_standard_category <", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryLessThanOrEqualTo(String value) {
            addCriterion("accounting_standard_category <=", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryLike(String value) {
            addCriterion("accounting_standard_category like", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryNotLike(String value) {
            addCriterion("accounting_standard_category not like", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryIn(List<String> values) {
            addCriterion("accounting_standard_category in", values, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryNotIn(List<String> values) {
            addCriterion("accounting_standard_category not in", values, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryBetween(String value1, String value2) {
            addCriterion("accounting_standard_category between", value1, value2, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryNotBetween(String value1, String value2) {
            addCriterion("accounting_standard_category not between", value1, value2, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andProductEnergyIsNull() {
            addCriterion("product_energy is null");
            return (Criteria) this;
        }

        public Criteria andProductEnergyIsNotNull() {
            addCriterion("product_energy is not null");
            return (Criteria) this;
        }

        public Criteria andProductEnergyEqualTo(Integer value) {
            addCriterion("product_energy =", value, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andProductEnergyNotEqualTo(Integer value) {
            addCriterion("product_energy <>", value, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andProductEnergyGreaterThan(Integer value) {
            addCriterion("product_energy >", value, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andProductEnergyGreaterThanOrEqualTo(Integer value) {
            addCriterion("product_energy >=", value, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andProductEnergyLessThan(Integer value) {
            addCriterion("product_energy <", value, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andProductEnergyLessThanOrEqualTo(Integer value) {
            addCriterion("product_energy <=", value, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andProductEnergyIn(List<Integer> values) {
            addCriterion("product_energy in", values, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andProductEnergyNotIn(List<Integer> values) {
            addCriterion("product_energy not in", values, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andProductEnergyBetween(Integer value1, Integer value2) {
            addCriterion("product_energy between", value1, value2, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andProductEnergyNotBetween(Integer value1, Integer value2) {
            addCriterion("product_energy not between", value1, value2, "productEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyIsNull() {
            addCriterion("sale_energy is null");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyIsNotNull() {
            addCriterion("sale_energy is not null");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyEqualTo(Integer value) {
            addCriterion("sale_energy =", value, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyNotEqualTo(Integer value) {
            addCriterion("sale_energy <>", value, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyGreaterThan(Integer value) {
            addCriterion("sale_energy >", value, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyGreaterThanOrEqualTo(Integer value) {
            addCriterion("sale_energy >=", value, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyLessThan(Integer value) {
            addCriterion("sale_energy <", value, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyLessThanOrEqualTo(Integer value) {
            addCriterion("sale_energy <=", value, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyIn(List<Integer> values) {
            addCriterion("sale_energy in", values, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyNotIn(List<Integer> values) {
            addCriterion("sale_energy not in", values, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyBetween(Integer value1, Integer value2) {
            addCriterion("sale_energy between", value1, value2, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andSaleEnergyNotBetween(Integer value1, Integer value2) {
            addCriterion("sale_energy not between", value1, value2, "saleEnergy");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesIsNull() {
            addCriterion("is_hava_brances is null");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesIsNotNull() {
            addCriterion("is_hava_brances is not null");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesEqualTo(Integer value) {
            addCriterion("is_hava_brances =", value, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesNotEqualTo(Integer value) {
            addCriterion("is_hava_brances <>", value, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesGreaterThan(Integer value) {
            addCriterion("is_hava_brances >", value, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_hava_brances >=", value, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesLessThan(Integer value) {
            addCriterion("is_hava_brances <", value, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesLessThanOrEqualTo(Integer value) {
            addCriterion("is_hava_brances <=", value, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesIn(List<Integer> values) {
            addCriterion("is_hava_brances in", values, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesNotIn(List<Integer> values) {
            addCriterion("is_hava_brances not in", values, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesBetween(Integer value1, Integer value2) {
            addCriterion("is_hava_brances between", value1, value2, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andIsHavaBrancesNotBetween(Integer value1, Integer value2) {
            addCriterion("is_hava_brances not between", value1, value2, "isHavaBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesIsNull() {
            addCriterion("count_brances is null");
            return (Criteria) this;
        }

        public Criteria andCountBrancesIsNotNull() {
            addCriterion("count_brances is not null");
            return (Criteria) this;
        }

        public Criteria andCountBrancesEqualTo(Integer value) {
            addCriterion("count_brances =", value, "countBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesNotEqualTo(Integer value) {
            addCriterion("count_brances <>", value, "countBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesGreaterThan(Integer value) {
            addCriterion("count_brances >", value, "countBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesGreaterThanOrEqualTo(Integer value) {
            addCriterion("count_brances >=", value, "countBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesLessThan(Integer value) {
            addCriterion("count_brances <", value, "countBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesLessThanOrEqualTo(Integer value) {
            addCriterion("count_brances <=", value, "countBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesIn(List<Integer> values) {
            addCriterion("count_brances in", values, "countBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesNotIn(List<Integer> values) {
            addCriterion("count_brances not in", values, "countBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesBetween(Integer value1, Integer value2) {
            addCriterion("count_brances between", value1, value2, "countBrances");
            return (Criteria) this;
        }

        public Criteria andCountBrancesNotBetween(Integer value1, Integer value2) {
            addCriterion("count_brances not between", value1, value2, "countBrances");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeIsNull() {
            addCriterion("legal_unit_type is null");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeIsNotNull() {
            addCriterion("legal_unit_type is not null");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeEqualTo(Integer value) {
            addCriterion("legal_unit_type =", value, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeNotEqualTo(Integer value) {
            addCriterion("legal_unit_type <>", value, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeGreaterThan(Integer value) {
            addCriterion("legal_unit_type >", value, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("legal_unit_type >=", value, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeLessThan(Integer value) {
            addCriterion("legal_unit_type <", value, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeLessThanOrEqualTo(Integer value) {
            addCriterion("legal_unit_type <=", value, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeIn(List<Integer> values) {
            addCriterion("legal_unit_type in", values, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeNotIn(List<Integer> values) {
            addCriterion("legal_unit_type not in", values, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeBetween(Integer value1, Integer value2) {
            addCriterion("legal_unit_type between", value1, value2, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalUnitTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("legal_unit_type not between", value1, value2, "legalUnitType");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeIsNull() {
            addCriterion("legal_social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeIsNotNull() {
            addCriterion("legal_social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeEqualTo(String value) {
            addCriterion("legal_social_credit_code =", value, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeNotEqualTo(String value) {
            addCriterion("legal_social_credit_code <>", value, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeGreaterThan(String value) {
            addCriterion("legal_social_credit_code >", value, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("legal_social_credit_code >=", value, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeLessThan(String value) {
            addCriterion("legal_social_credit_code <", value, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("legal_social_credit_code <=", value, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeLike(String value) {
            addCriterion("legal_social_credit_code like", value, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeNotLike(String value) {
            addCriterion("legal_social_credit_code not like", value, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeIn(List<String> values) {
            addCriterion("legal_social_credit_code in", values, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeNotIn(List<String> values) {
            addCriterion("legal_social_credit_code not in", values, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("legal_social_credit_code between", value1, value2, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("legal_social_credit_code not between", value1, value2, "legalSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeIsNull() {
            addCriterion("legal_organization_code is null");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeIsNotNull() {
            addCriterion("legal_organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeEqualTo(String value) {
            addCriterion("legal_organization_code =", value, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeNotEqualTo(String value) {
            addCriterion("legal_organization_code <>", value, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeGreaterThan(String value) {
            addCriterion("legal_organization_code >", value, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("legal_organization_code >=", value, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeLessThan(String value) {
            addCriterion("legal_organization_code <", value, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("legal_organization_code <=", value, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeLike(String value) {
            addCriterion("legal_organization_code like", value, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeNotLike(String value) {
            addCriterion("legal_organization_code not like", value, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeIn(List<String> values) {
            addCriterion("legal_organization_code in", values, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeNotIn(List<String> values) {
            addCriterion("legal_organization_code not in", values, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeBetween(String value1, String value2) {
            addCriterion("legal_organization_code between", value1, value2, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("legal_organization_code not between", value1, value2, "legalOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameIsNull() {
            addCriterion("legal_unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameIsNotNull() {
            addCriterion("legal_unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameEqualTo(String value) {
            addCriterion("legal_unit_detailed_name =", value, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameNotEqualTo(String value) {
            addCriterion("legal_unit_detailed_name <>", value, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameGreaterThan(String value) {
            addCriterion("legal_unit_detailed_name >", value, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("legal_unit_detailed_name >=", value, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameLessThan(String value) {
            addCriterion("legal_unit_detailed_name <", value, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("legal_unit_detailed_name <=", value, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameLike(String value) {
            addCriterion("legal_unit_detailed_name like", value, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameNotLike(String value) {
            addCriterion("legal_unit_detailed_name not like", value, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameIn(List<String> values) {
            addCriterion("legal_unit_detailed_name in", values, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameNotIn(List<String> values) {
            addCriterion("legal_unit_detailed_name not in", values, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("legal_unit_detailed_name between", value1, value2, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("legal_unit_detailed_name not between", value1, value2, "legalUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalAddressIsNull() {
            addCriterion("legal_address is null");
            return (Criteria) this;
        }

        public Criteria andLegalAddressIsNotNull() {
            addCriterion("legal_address is not null");
            return (Criteria) this;
        }

        public Criteria andLegalAddressEqualTo(String value) {
            addCriterion("legal_address =", value, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressNotEqualTo(String value) {
            addCriterion("legal_address <>", value, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressGreaterThan(String value) {
            addCriterion("legal_address >", value, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressGreaterThanOrEqualTo(String value) {
            addCriterion("legal_address >=", value, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressLessThan(String value) {
            addCriterion("legal_address <", value, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressLessThanOrEqualTo(String value) {
            addCriterion("legal_address <=", value, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressLike(String value) {
            addCriterion("legal_address like", value, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressNotLike(String value) {
            addCriterion("legal_address not like", value, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressIn(List<String> values) {
            addCriterion("legal_address in", values, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressNotIn(List<String> values) {
            addCriterion("legal_address not in", values, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressBetween(String value1, String value2) {
            addCriterion("legal_address between", value1, value2, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalAddressNotBetween(String value1, String value2) {
            addCriterion("legal_address not between", value1, value2, "legalAddress");
            return (Criteria) this;
        }

        public Criteria andLegalZoningIsNull() {
            addCriterion("legal_zoning is null");
            return (Criteria) this;
        }

        public Criteria andLegalZoningIsNotNull() {
            addCriterion("legal_zoning is not null");
            return (Criteria) this;
        }

        public Criteria andLegalZoningEqualTo(String value) {
            addCriterion("legal_zoning =", value, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningNotEqualTo(String value) {
            addCriterion("legal_zoning <>", value, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningGreaterThan(String value) {
            addCriterion("legal_zoning >", value, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningGreaterThanOrEqualTo(String value) {
            addCriterion("legal_zoning >=", value, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningLessThan(String value) {
            addCriterion("legal_zoning <", value, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningLessThanOrEqualTo(String value) {
            addCriterion("legal_zoning <=", value, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningLike(String value) {
            addCriterion("legal_zoning like", value, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningNotLike(String value) {
            addCriterion("legal_zoning not like", value, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningIn(List<String> values) {
            addCriterion("legal_zoning in", values, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningNotIn(List<String> values) {
            addCriterion("legal_zoning not in", values, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningBetween(String value1, String value2) {
            addCriterion("legal_zoning between", value1, value2, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andLegalZoningNotBetween(String value1, String value2) {
            addCriterion("legal_zoning not between", value1, value2, "legalZoning");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeIsNull() {
            addCriterion("unique_code is null");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeIsNotNull() {
            addCriterion("unique_code is not null");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeEqualTo(String value) {
            addCriterion("unique_code =", value, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeNotEqualTo(String value) {
            addCriterion("unique_code <>", value, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeGreaterThan(String value) {
            addCriterion("unique_code >", value, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeGreaterThanOrEqualTo(String value) {
            addCriterion("unique_code >=", value, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeLessThan(String value) {
            addCriterion("unique_code <", value, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeLessThanOrEqualTo(String value) {
            addCriterion("unique_code <=", value, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeLike(String value) {
            addCriterion("unique_code like", value, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeNotLike(String value) {
            addCriterion("unique_code not like", value, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeIn(List<String> values) {
            addCriterion("unique_code in", values, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeNotIn(List<String> values) {
            addCriterion("unique_code not in", values, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeBetween(String value1, String value2) {
            addCriterion("unique_code between", value1, value2, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andUniqueCodeNotBetween(String value1, String value2) {
            addCriterion("unique_code not between", value1, value2, "uniqueCode");
            return (Criteria) this;
        }

        public Criteria andDataSourceIsNull() {
            addCriterion("data_source is null");
            return (Criteria) this;
        }

        public Criteria andDataSourceIsNotNull() {
            addCriterion("data_source is not null");
            return (Criteria) this;
        }

        public Criteria andDataSourceEqualTo(String value) {
            addCriterion("data_source =", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceNotEqualTo(String value) {
            addCriterion("data_source <>", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceGreaterThan(String value) {
            addCriterion("data_source >", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceGreaterThanOrEqualTo(String value) {
            addCriterion("data_source >=", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceLessThan(String value) {
            addCriterion("data_source <", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceLessThanOrEqualTo(String value) {
            addCriterion("data_source <=", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceLike(String value) {
            addCriterion("data_source like", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceNotLike(String value) {
            addCriterion("data_source not like", value, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceIn(List<String> values) {
            addCriterion("data_source in", values, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceNotIn(List<String> values) {
            addCriterion("data_source not in", values, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceBetween(String value1, String value2) {
            addCriterion("data_source between", value1, value2, "dataSource");
            return (Criteria) this;
        }

        public Criteria andDataSourceNotBetween(String value1, String value2) {
            addCriterion("data_source not between", value1, value2, "dataSource");
            return (Criteria) this;
        }

        public Criteria andPreparerIsNull() {
            addCriterion("preparer is null");
            return (Criteria) this;
        }

        public Criteria andPreparerIsNotNull() {
            addCriterion("preparer is not null");
            return (Criteria) this;
        }

        public Criteria andPreparerEqualTo(String value) {
            addCriterion("preparer =", value, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerNotEqualTo(String value) {
            addCriterion("preparer <>", value, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerGreaterThan(String value) {
            addCriterion("preparer >", value, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerGreaterThanOrEqualTo(String value) {
            addCriterion("preparer >=", value, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerLessThan(String value) {
            addCriterion("preparer <", value, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerLessThanOrEqualTo(String value) {
            addCriterion("preparer <=", value, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerLike(String value) {
            addCriterion("preparer like", value, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerNotLike(String value) {
            addCriterion("preparer not like", value, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerIn(List<String> values) {
            addCriterion("preparer in", values, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerNotIn(List<String> values) {
            addCriterion("preparer not in", values, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerBetween(String value1, String value2) {
            addCriterion("preparer between", value1, value2, "preparer");
            return (Criteria) this;
        }

        public Criteria andPreparerNotBetween(String value1, String value2) {
            addCriterion("preparer not between", value1, value2, "preparer");
            return (Criteria) this;
        }

        public Criteria andContactNumberIsNull() {
            addCriterion("contact_number is null");
            return (Criteria) this;
        }

        public Criteria andContactNumberIsNotNull() {
            addCriterion("contact_number is not null");
            return (Criteria) this;
        }

        public Criteria andContactNumberEqualTo(String value) {
            addCriterion("contact_number =", value, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberNotEqualTo(String value) {
            addCriterion("contact_number <>", value, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberGreaterThan(String value) {
            addCriterion("contact_number >", value, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberGreaterThanOrEqualTo(String value) {
            addCriterion("contact_number >=", value, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberLessThan(String value) {
            addCriterion("contact_number <", value, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberLessThanOrEqualTo(String value) {
            addCriterion("contact_number <=", value, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberLike(String value) {
            addCriterion("contact_number like", value, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberNotLike(String value) {
            addCriterion("contact_number not like", value, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberIn(List<String> values) {
            addCriterion("contact_number in", values, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberNotIn(List<String> values) {
            addCriterion("contact_number not in", values, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberBetween(String value1, String value2) {
            addCriterion("contact_number between", value1, value2, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andContactNumberNotBetween(String value1, String value2) {
            addCriterion("contact_number not between", value1, value2, "contactNumber");
            return (Criteria) this;
        }

        public Criteria andFormTimeIsNull() {
            addCriterion("form_time is null");
            return (Criteria) this;
        }

        public Criteria andFormTimeIsNotNull() {
            addCriterion("form_time is not null");
            return (Criteria) this;
        }

        public Criteria andFormTimeEqualTo(Date value) {
            addCriterionForJDBCDate("form_time =", value, "formTime");
            return (Criteria) this;
        }

        public Criteria andFormTimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("form_time <>", value, "formTime");
            return (Criteria) this;
        }

        public Criteria andFormTimeGreaterThan(Date value) {
            addCriterionForJDBCDate("form_time >", value, "formTime");
            return (Criteria) this;
        }

        public Criteria andFormTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("form_time >=", value, "formTime");
            return (Criteria) this;
        }

        public Criteria andFormTimeLessThan(Date value) {
            addCriterionForJDBCDate("form_time <", value, "formTime");
            return (Criteria) this;
        }

        public Criteria andFormTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("form_time <=", value, "formTime");
            return (Criteria) this;
        }

        public Criteria andFormTimeIn(List<Date> values) {
            addCriterionForJDBCDate("form_time in", values, "formTime");
            return (Criteria) this;
        }

        public Criteria andFormTimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("form_time not in", values, "formTime");
            return (Criteria) this;
        }

        public Criteria andFormTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("form_time between", value1, value2, "formTime");
            return (Criteria) this;
        }

        public Criteria andFormTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("form_time not between", value1, value2, "formTime");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderIsNull() {
            addCriterion("statistic_leader is null");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderIsNotNull() {
            addCriterion("statistic_leader is not null");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderEqualTo(String value) {
            addCriterion("statistic_leader =", value, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderNotEqualTo(String value) {
            addCriterion("statistic_leader <>", value, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderGreaterThan(String value) {
            addCriterion("statistic_leader >", value, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderGreaterThanOrEqualTo(String value) {
            addCriterion("statistic_leader >=", value, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderLessThan(String value) {
            addCriterion("statistic_leader <", value, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderLessThanOrEqualTo(String value) {
            addCriterion("statistic_leader <=", value, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderLike(String value) {
            addCriterion("statistic_leader like", value, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderNotLike(String value) {
            addCriterion("statistic_leader not like", value, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderIn(List<String> values) {
            addCriterion("statistic_leader in", values, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderNotIn(List<String> values) {
            addCriterion("statistic_leader not in", values, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderBetween(String value1, String value2) {
            addCriterion("statistic_leader between", value1, value2, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatisticLeaderNotBetween(String value1, String value2) {
            addCriterion("statistic_leader not between", value1, value2, "statisticLeader");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneIsNull() {
            addCriterion("preparer_fixed_line_telephone is null");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneIsNotNull() {
            addCriterion("preparer_fixed_line_telephone is not null");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneEqualTo(String value) {
            addCriterion("preparer_fixed_line_telephone =", value, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneNotEqualTo(String value) {
            addCriterion("preparer_fixed_line_telephone <>", value, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneGreaterThan(String value) {
            addCriterion("preparer_fixed_line_telephone >", value, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("preparer_fixed_line_telephone >=", value, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneLessThan(String value) {
            addCriterion("preparer_fixed_line_telephone <", value, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneLessThanOrEqualTo(String value) {
            addCriterion("preparer_fixed_line_telephone <=", value, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneLike(String value) {
            addCriterion("preparer_fixed_line_telephone like", value, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneNotLike(String value) {
            addCriterion("preparer_fixed_line_telephone not like", value, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneIn(List<String> values) {
            addCriterion("preparer_fixed_line_telephone in", values, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneNotIn(List<String> values) {
            addCriterion("preparer_fixed_line_telephone not in", values, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneBetween(String value1, String value2) {
            addCriterion("preparer_fixed_line_telephone between", value1, value2, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andPreparerFixedLineTelephoneNotBetween(String value1, String value2) {
            addCriterion("preparer_fixed_line_telephone not between", value1, value2, "preparerFixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderIsNull() {
            addCriterion("unit_leader is null");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderIsNotNull() {
            addCriterion("unit_leader is not null");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderEqualTo(String value) {
            addCriterion("unit_leader =", value, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderNotEqualTo(String value) {
            addCriterion("unit_leader <>", value, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderGreaterThan(String value) {
            addCriterion("unit_leader >", value, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderGreaterThanOrEqualTo(String value) {
            addCriterion("unit_leader >=", value, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderLessThan(String value) {
            addCriterion("unit_leader <", value, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderLessThanOrEqualTo(String value) {
            addCriterion("unit_leader <=", value, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderLike(String value) {
            addCriterion("unit_leader like", value, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderNotLike(String value) {
            addCriterion("unit_leader not like", value, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderIn(List<String> values) {
            addCriterion("unit_leader in", values, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderNotIn(List<String> values) {
            addCriterion("unit_leader not in", values, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderBetween(String value1, String value2) {
            addCriterion("unit_leader between", value1, value2, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andUnitLeaderNotBetween(String value1, String value2) {
            addCriterion("unit_leader not between", value1, value2, "unitLeader");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressIsNull() {
            addCriterion("reference_address is null");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressIsNotNull() {
            addCriterion("reference_address is not null");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressEqualTo(String value) {
            addCriterion("reference_address =", value, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressNotEqualTo(String value) {
            addCriterion("reference_address <>", value, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressGreaterThan(String value) {
            addCriterion("reference_address >", value, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressGreaterThanOrEqualTo(String value) {
            addCriterion("reference_address >=", value, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressLessThan(String value) {
            addCriterion("reference_address <", value, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressLessThanOrEqualTo(String value) {
            addCriterion("reference_address <=", value, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressLike(String value) {
            addCriterion("reference_address like", value, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressNotLike(String value) {
            addCriterion("reference_address not like", value, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressIn(List<String> values) {
            addCriterion("reference_address in", values, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressNotIn(List<String> values) {
            addCriterion("reference_address not in", values, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressBetween(String value1, String value2) {
            addCriterion("reference_address between", value1, value2, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddressNotBetween(String value1, String value2) {
            addCriterion("reference_address not between", value1, value2, "referenceAddress");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02IsNull() {
            addCriterion("reference_address_02 is null");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02IsNotNull() {
            addCriterion("reference_address_02 is not null");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02EqualTo(String value) {
            addCriterion("reference_address_02 =", value, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02NotEqualTo(String value) {
            addCriterion("reference_address_02 <>", value, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02GreaterThan(String value) {
            addCriterion("reference_address_02 >", value, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02GreaterThanOrEqualTo(String value) {
            addCriterion("reference_address_02 >=", value, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02LessThan(String value) {
            addCriterion("reference_address_02 <", value, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02LessThanOrEqualTo(String value) {
            addCriterion("reference_address_02 <=", value, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02Like(String value) {
            addCriterion("reference_address_02 like", value, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02NotLike(String value) {
            addCriterion("reference_address_02 not like", value, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02In(List<String> values) {
            addCriterion("reference_address_02 in", values, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02NotIn(List<String> values) {
            addCriterion("reference_address_02 not in", values, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02Between(String value1, String value2) {
            addCriterion("reference_address_02 between", value1, value2, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andReferenceAddress02NotBetween(String value1, String value2) {
            addCriterion("reference_address_02 not between", value1, value2, "referenceAddress02");
            return (Criteria) this;
        }

        public Criteria andBuildingNameIsNull() {
            addCriterion("building_name is null");
            return (Criteria) this;
        }

        public Criteria andBuildingNameIsNotNull() {
            addCriterion("building_name is not null");
            return (Criteria) this;
        }

        public Criteria andBuildingNameEqualTo(String value) {
            addCriterion("building_name =", value, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameNotEqualTo(String value) {
            addCriterion("building_name <>", value, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameGreaterThan(String value) {
            addCriterion("building_name >", value, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameGreaterThanOrEqualTo(String value) {
            addCriterion("building_name >=", value, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameLessThan(String value) {
            addCriterion("building_name <", value, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameLessThanOrEqualTo(String value) {
            addCriterion("building_name <=", value, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameLike(String value) {
            addCriterion("building_name like", value, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameNotLike(String value) {
            addCriterion("building_name not like", value, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameIn(List<String> values) {
            addCriterion("building_name in", values, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameNotIn(List<String> values) {
            addCriterion("building_name not in", values, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameBetween(String value1, String value2) {
            addCriterion("building_name between", value1, value2, "buildingName");
            return (Criteria) this;
        }

        public Criteria andBuildingNameNotBetween(String value1, String value2) {
            addCriterion("building_name not between", value1, value2, "buildingName");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeIsNull() {
            addCriterion("taxes_range is null");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeIsNotNull() {
            addCriterion("taxes_range is not null");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeEqualTo(String value) {
            addCriterion("taxes_range =", value, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeNotEqualTo(String value) {
            addCriterion("taxes_range <>", value, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeGreaterThan(String value) {
            addCriterion("taxes_range >", value, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeGreaterThanOrEqualTo(String value) {
            addCriterion("taxes_range >=", value, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeLessThan(String value) {
            addCriterion("taxes_range <", value, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeLessThanOrEqualTo(String value) {
            addCriterion("taxes_range <=", value, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeLike(String value) {
            addCriterion("taxes_range like", value, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeNotLike(String value) {
            addCriterion("taxes_range not like", value, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeIn(List<String> values) {
            addCriterion("taxes_range in", values, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeNotIn(List<String> values) {
            addCriterion("taxes_range not in", values, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeBetween(String value1, String value2) {
            addCriterion("taxes_range between", value1, value2, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andTaxesRangeNotBetween(String value1, String value2) {
            addCriterion("taxes_range not between", value1, value2, "taxesRange");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalIsNull() {
            addCriterion("registered_capital is null");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalIsNotNull() {
            addCriterion("registered_capital is not null");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalEqualTo(String value) {
            addCriterion("registered_capital =", value, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalNotEqualTo(String value) {
            addCriterion("registered_capital <>", value, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalGreaterThan(String value) {
            addCriterion("registered_capital >", value, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalGreaterThanOrEqualTo(String value) {
            addCriterion("registered_capital >=", value, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalLessThan(String value) {
            addCriterion("registered_capital <", value, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalLessThanOrEqualTo(String value) {
            addCriterion("registered_capital <=", value, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalLike(String value) {
            addCriterion("registered_capital like", value, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalNotLike(String value) {
            addCriterion("registered_capital not like", value, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalIn(List<String> values) {
            addCriterion("registered_capital in", values, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalNotIn(List<String> values) {
            addCriterion("registered_capital not in", values, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalBetween(String value1, String value2) {
            addCriterion("registered_capital between", value1, value2, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andRegisteredCapitalNotBetween(String value1, String value2) {
            addCriterion("registered_capital not between", value1, value2, "registeredCapital");
            return (Criteria) this;
        }

        public Criteria andCommunityIsNull() {
            addCriterion("community is null");
            return (Criteria) this;
        }

        public Criteria andCommunityIsNotNull() {
            addCriterion("community is not null");
            return (Criteria) this;
        }

        public Criteria andCommunityEqualTo(String value) {
            addCriterion("community =", value, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityNotEqualTo(String value) {
            addCriterion("community <>", value, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityGreaterThan(String value) {
            addCriterion("community >", value, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityGreaterThanOrEqualTo(String value) {
            addCriterion("community >=", value, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityLessThan(String value) {
            addCriterion("community <", value, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityLessThanOrEqualTo(String value) {
            addCriterion("community <=", value, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityLike(String value) {
            addCriterion("community like", value, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityNotLike(String value) {
            addCriterion("community not like", value, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityIn(List<String> values) {
            addCriterion("community in", values, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityNotIn(List<String> values) {
            addCriterion("community not in", values, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityBetween(String value1, String value2) {
            addCriterion("community between", value1, value2, "community");
            return (Criteria) this;
        }

        public Criteria andCommunityNotBetween(String value1, String value2) {
            addCriterion("community not between", value1, value2, "community");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeIsNull() {
            addCriterion("register_type is null");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeIsNotNull() {
            addCriterion("register_type is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeEqualTo(String value) {
            addCriterion("register_type =", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotEqualTo(String value) {
            addCriterion("register_type <>", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeGreaterThan(String value) {
            addCriterion("register_type >", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeGreaterThanOrEqualTo(String value) {
            addCriterion("register_type >=", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeLessThan(String value) {
            addCriterion("register_type <", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeLessThanOrEqualTo(String value) {
            addCriterion("register_type <=", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeLike(String value) {
            addCriterion("register_type like", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotLike(String value) {
            addCriterion("register_type not like", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeIn(List<String> values) {
            addCriterion("register_type in", values, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotIn(List<String> values) {
            addCriterion("register_type not in", values, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeBetween(String value1, String value2) {
            addCriterion("register_type between", value1, value2, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotBetween(String value1, String value2) {
            addCriterion("register_type not between", value1, value2, "registerType");
            return (Criteria) this;
        }

        public Criteria andAppendix1IsNull() {
            addCriterion("appendix1 is null");
            return (Criteria) this;
        }

        public Criteria andAppendix1IsNotNull() {
            addCriterion("appendix1 is not null");
            return (Criteria) this;
        }

        public Criteria andAppendix1EqualTo(String value) {
            addCriterion("appendix1 =", value, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1NotEqualTo(String value) {
            addCriterion("appendix1 <>", value, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1GreaterThan(String value) {
            addCriterion("appendix1 >", value, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1GreaterThanOrEqualTo(String value) {
            addCriterion("appendix1 >=", value, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1LessThan(String value) {
            addCriterion("appendix1 <", value, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1LessThanOrEqualTo(String value) {
            addCriterion("appendix1 <=", value, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1Like(String value) {
            addCriterion("appendix1 like", value, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1NotLike(String value) {
            addCriterion("appendix1 not like", value, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1In(List<String> values) {
            addCriterion("appendix1 in", values, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1NotIn(List<String> values) {
            addCriterion("appendix1 not in", values, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1Between(String value1, String value2) {
            addCriterion("appendix1 between", value1, value2, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix1NotBetween(String value1, String value2) {
            addCriterion("appendix1 not between", value1, value2, "appendix1");
            return (Criteria) this;
        }

        public Criteria andAppendix2IsNull() {
            addCriterion("appendix2 is null");
            return (Criteria) this;
        }

        public Criteria andAppendix2IsNotNull() {
            addCriterion("appendix2 is not null");
            return (Criteria) this;
        }

        public Criteria andAppendix2EqualTo(String value) {
            addCriterion("appendix2 =", value, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2NotEqualTo(String value) {
            addCriterion("appendix2 <>", value, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2GreaterThan(String value) {
            addCriterion("appendix2 >", value, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2GreaterThanOrEqualTo(String value) {
            addCriterion("appendix2 >=", value, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2LessThan(String value) {
            addCriterion("appendix2 <", value, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2LessThanOrEqualTo(String value) {
            addCriterion("appendix2 <=", value, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2Like(String value) {
            addCriterion("appendix2 like", value, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2NotLike(String value) {
            addCriterion("appendix2 not like", value, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2In(List<String> values) {
            addCriterion("appendix2 in", values, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2NotIn(List<String> values) {
            addCriterion("appendix2 not in", values, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2Between(String value1, String value2) {
            addCriterion("appendix2 between", value1, value2, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix2NotBetween(String value1, String value2) {
            addCriterion("appendix2 not between", value1, value2, "appendix2");
            return (Criteria) this;
        }

        public Criteria andAppendix3IsNull() {
            addCriterion("appendix3 is null");
            return (Criteria) this;
        }

        public Criteria andAppendix3IsNotNull() {
            addCriterion("appendix3 is not null");
            return (Criteria) this;
        }

        public Criteria andAppendix3EqualTo(String value) {
            addCriterion("appendix3 =", value, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3NotEqualTo(String value) {
            addCriterion("appendix3 <>", value, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3GreaterThan(String value) {
            addCriterion("appendix3 >", value, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3GreaterThanOrEqualTo(String value) {
            addCriterion("appendix3 >=", value, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3LessThan(String value) {
            addCriterion("appendix3 <", value, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3LessThanOrEqualTo(String value) {
            addCriterion("appendix3 <=", value, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3Like(String value) {
            addCriterion("appendix3 like", value, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3NotLike(String value) {
            addCriterion("appendix3 not like", value, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3In(List<String> values) {
            addCriterion("appendix3 in", values, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3NotIn(List<String> values) {
            addCriterion("appendix3 not in", values, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3Between(String value1, String value2) {
            addCriterion("appendix3 between", value1, value2, "appendix3");
            return (Criteria) this;
        }

        public Criteria andAppendix3NotBetween(String value1, String value2) {
            addCriterion("appendix3 not between", value1, value2, "appendix3");
            return (Criteria) this;
        }

        public Criteria andUiteStatusIsNull() {
            addCriterion("uite_status is null");
            return (Criteria) this;
        }

        public Criteria andUiteStatusIsNotNull() {
            addCriterion("uite_status is not null");
            return (Criteria) this;
        }

        public Criteria andUiteStatusEqualTo(Integer value) {
            addCriterion("uite_status =", value, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUiteStatusNotEqualTo(Integer value) {
            addCriterion("uite_status <>", value, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUiteStatusGreaterThan(Integer value) {
            addCriterion("uite_status >", value, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUiteStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("uite_status >=", value, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUiteStatusLessThan(Integer value) {
            addCriterion("uite_status <", value, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUiteStatusLessThanOrEqualTo(Integer value) {
            addCriterion("uite_status <=", value, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUiteStatusIn(List<Integer> values) {
            addCriterion("uite_status in", values, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUiteStatusNotIn(List<Integer> values) {
            addCriterion("uite_status not in", values, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUiteStatusBetween(Integer value1, Integer value2) {
            addCriterion("uite_status between", value1, value2, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUiteStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("uite_status not between", value1, value2, "uiteStatus");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andAdminIdIsNull() {
            addCriterion("admin_id is null");
            return (Criteria) this;
        }

        public Criteria andAdminIdIsNotNull() {
            addCriterion("admin_id is not null");
            return (Criteria) this;
        }

        public Criteria andAdminIdEqualTo(Integer value) {
            addCriterion("admin_id =", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdNotEqualTo(Integer value) {
            addCriterion("admin_id <>", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdGreaterThan(Integer value) {
            addCriterion("admin_id >", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("admin_id >=", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdLessThan(Integer value) {
            addCriterion("admin_id <", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdLessThanOrEqualTo(Integer value) {
            addCriterion("admin_id <=", value, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdIn(List<Integer> values) {
            addCriterion("admin_id in", values, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdNotIn(List<Integer> values) {
            addCriterion("admin_id not in", values, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdBetween(Integer value1, Integer value2) {
            addCriterion("admin_id between", value1, value2, "adminId");
            return (Criteria) this;
        }

        public Criteria andAdminIdNotBetween(Integer value1, Integer value2) {
            addCriterion("admin_id not between", value1, value2, "adminId");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createTime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNull() {
            addCriterion("updateTime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNotNull() {
            addCriterion("updateTime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeEqualTo(Date value) {
            addCriterion("updateTime =", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotEqualTo(Date value) {
            addCriterion("updateTime <>", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThan(Date value) {
            addCriterion("updateTime >", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updateTime >=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThan(Date value) {
            addCriterion("updateTime <", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThanOrEqualTo(Date value) {
            addCriterion("updateTime <=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIn(List<Date> values) {
            addCriterion("updateTime in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotIn(List<Date> values) {
            addCriterion("updateTime not in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeBetween(Date value1, Date value2) {
            addCriterion("updateTime between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotBetween(Date value1, Date value2) {
            addCriterion("updateTime not between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andSequenceIsNull() {
            addCriterion("sequence is null");
            return (Criteria) this;
        }

        public Criteria andSequenceIsNotNull() {
            addCriterion("sequence is not null");
            return (Criteria) this;
        }

        public Criteria andSequenceEqualTo(Integer value) {
            addCriterion("sequence =", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotEqualTo(Integer value) {
            addCriterion("sequence <>", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceGreaterThan(Integer value) {
            addCriterion("sequence >", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceGreaterThanOrEqualTo(Integer value) {
            addCriterion("sequence >=", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceLessThan(Integer value) {
            addCriterion("sequence <", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceLessThanOrEqualTo(Integer value) {
            addCriterion("sequence <=", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceIn(List<Integer> values) {
            addCriterion("sequence in", values, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotIn(List<Integer> values) {
            addCriterion("sequence not in", values, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceBetween(Integer value1, Integer value2) {
            addCriterion("sequence between", value1, value2, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotBetween(Integer value1, Integer value2) {
            addCriterion("sequence not between", value1, value2, "sequence");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}