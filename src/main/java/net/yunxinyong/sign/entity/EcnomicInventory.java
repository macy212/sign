package net.yunxinyong.sign.entity;

import java.util.Date;

public class EcnomicInventory {
    private Long id;

    private String socialCreditCode;

    private String organizationCode;

    private String unitDetailedName;

    private String legalRepresentative;

    private String unitLocationProvience;

    private String unitLocationCity;

    private String unitLocationDistrict;

    private String unitLocationTown;

    private String unitLocationStreet;

    private String unitLoactionSubdistrict;

    private String unitLocationCummunity;

    private String unitLocationZoning;

    private String unitLocationVillagecode;

    private String unitLocationConstructioncode;

    private String parkLocationName;

    private String parkLocationCode;

    private String unitRegistrationProvince;

    private String unitRegistrationCity;

    private String unitRegistrationDistrict;

    private String unitRegistrationTown;

    private String unitRegistrationStreet;

    private String unitRegistrationSubdistrict;

    private String unitRegistrationCummunity;

    private String unitRegistrationZoning;

    private String parkRegistrationName;

    private String parkRegistrationCode;

    private String areaCode;

    private String fixedLineTelephone;

    private String cellPhoneNumber;

    private String majorBusiness1;

    private String majorBusiness2;

    private String majorBusiness3;

    private String industryCode;

    private String industryName;

    private String verifiedMajorBusiness1;

    private String verifiedMajorBusiness2;

    private String verifiedMajorBusiness3;

    private String verifiedIndustryCode;

    private String verifiedIndustryName;

    private String mechanismType;

    private Date establishmentTime;

    private String businessState;

    private String unitType;

    private Integer verifiedUnitType;

    private String accountingStandardCategory;

    private Integer productEnergy;

    private Integer saleEnergy;

    private Integer isHavaBrances;

    private Integer countBrances;

    private Integer legalUnitType;

    private String legalSocialCreditCode;

    private String legalOrganizationCode;

    private String legalUnitDetailedName;

    private String legalAddress;

    private String legalZoning;

    private String uniqueCode;

    private String dataSource;

    private String preparer;

    private String contactNumber;

    private Date formTime;

    private String statisticLeader;

    private Integer status;

    private String preparerFixedLineTelephone;

    private String unitLeader;

    private String referenceAddress;

    private String referenceAddress02;

    private String buildingName;

    private String taxesRange;

    private String registeredCapital;

    private String community;

    private String registerType;

    private String appendix1;

    private String appendix2;

    private String appendix3;

    private Integer uiteStatus;

    private Integer userId;

    private Integer adminId;

    private String type;

    private Date createtime;

    private Date updatetime;

    private Integer sequence;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName == null ? null : unitDetailedName.trim();
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative == null ? null : legalRepresentative.trim();
    }

    public String getUnitLocationProvience() {
        return unitLocationProvience;
    }

    public void setUnitLocationProvience(String unitLocationProvience) {
        this.unitLocationProvience = unitLocationProvience == null ? null : unitLocationProvience.trim();
    }

    public String getUnitLocationCity() {
        return unitLocationCity;
    }

    public void setUnitLocationCity(String unitLocationCity) {
        this.unitLocationCity = unitLocationCity == null ? null : unitLocationCity.trim();
    }

    public String getUnitLocationDistrict() {
        return unitLocationDistrict;
    }

    public void setUnitLocationDistrict(String unitLocationDistrict) {
        this.unitLocationDistrict = unitLocationDistrict == null ? null : unitLocationDistrict.trim();
    }

    public String getUnitLocationTown() {
        return unitLocationTown;
    }

    public void setUnitLocationTown(String unitLocationTown) {
        this.unitLocationTown = unitLocationTown == null ? null : unitLocationTown.trim();
    }

    public String getUnitLocationStreet() {
        return unitLocationStreet;
    }

    public void setUnitLocationStreet(String unitLocationStreet) {
        this.unitLocationStreet = unitLocationStreet == null ? null : unitLocationStreet.trim();
    }

    public String getUnitLoactionSubdistrict() {
        return unitLoactionSubdistrict;
    }

    public void setUnitLoactionSubdistrict(String unitLoactionSubdistrict) {
        this.unitLoactionSubdistrict = unitLoactionSubdistrict == null ? null : unitLoactionSubdistrict.trim();
    }

    public String getUnitLocationCummunity() {
        return unitLocationCummunity;
    }

    public void setUnitLocationCummunity(String unitLocationCummunity) {
        this.unitLocationCummunity = unitLocationCummunity == null ? null : unitLocationCummunity.trim();
    }

    public String getUnitLocationZoning() {
        return unitLocationZoning;
    }

    public void setUnitLocationZoning(String unitLocationZoning) {
        this.unitLocationZoning = unitLocationZoning == null ? null : unitLocationZoning.trim();
    }

    public String getUnitLocationVillagecode() {
        return unitLocationVillagecode;
    }

    public void setUnitLocationVillagecode(String unitLocationVillagecode) {
        this.unitLocationVillagecode = unitLocationVillagecode == null ? null : unitLocationVillagecode.trim();
    }

    public String getUnitLocationConstructioncode() {
        return unitLocationConstructioncode;
    }

    public void setUnitLocationConstructioncode(String unitLocationConstructioncode) {
        this.unitLocationConstructioncode = unitLocationConstructioncode == null ? null : unitLocationConstructioncode.trim();
    }

    public String getParkLocationName() {
        return parkLocationName;
    }

    public void setParkLocationName(String parkLocationName) {
        this.parkLocationName = parkLocationName == null ? null : parkLocationName.trim();
    }

    public String getParkLocationCode() {
        return parkLocationCode;
    }

    public void setParkLocationCode(String parkLocationCode) {
        this.parkLocationCode = parkLocationCode == null ? null : parkLocationCode.trim();
    }

    public String getUnitRegistrationProvince() {
        return unitRegistrationProvince;
    }

    public void setUnitRegistrationProvince(String unitRegistrationProvince) {
        this.unitRegistrationProvince = unitRegistrationProvince == null ? null : unitRegistrationProvince.trim();
    }

    public String getUnitRegistrationCity() {
        return unitRegistrationCity;
    }

    public void setUnitRegistrationCity(String unitRegistrationCity) {
        this.unitRegistrationCity = unitRegistrationCity == null ? null : unitRegistrationCity.trim();
    }

    public String getUnitRegistrationDistrict() {
        return unitRegistrationDistrict;
    }

    public void setUnitRegistrationDistrict(String unitRegistrationDistrict) {
        this.unitRegistrationDistrict = unitRegistrationDistrict == null ? null : unitRegistrationDistrict.trim();
    }

    public String getUnitRegistrationTown() {
        return unitRegistrationTown;
    }

    public void setUnitRegistrationTown(String unitRegistrationTown) {
        this.unitRegistrationTown = unitRegistrationTown == null ? null : unitRegistrationTown.trim();
    }

    public String getUnitRegistrationStreet() {
        return unitRegistrationStreet;
    }

    public void setUnitRegistrationStreet(String unitRegistrationStreet) {
        this.unitRegistrationStreet = unitRegistrationStreet == null ? null : unitRegistrationStreet.trim();
    }

    public String getUnitRegistrationSubdistrict() {
        return unitRegistrationSubdistrict;
    }

    public void setUnitRegistrationSubdistrict(String unitRegistrationSubdistrict) {
        this.unitRegistrationSubdistrict = unitRegistrationSubdistrict == null ? null : unitRegistrationSubdistrict.trim();
    }

    public String getUnitRegistrationCummunity() {
        return unitRegistrationCummunity;
    }

    public void setUnitRegistrationCummunity(String unitRegistrationCummunity) {
        this.unitRegistrationCummunity = unitRegistrationCummunity == null ? null : unitRegistrationCummunity.trim();
    }

    public String getUnitRegistrationZoning() {
        return unitRegistrationZoning;
    }

    public void setUnitRegistrationZoning(String unitRegistrationZoning) {
        this.unitRegistrationZoning = unitRegistrationZoning == null ? null : unitRegistrationZoning.trim();
    }

    public String getParkRegistrationName() {
        return parkRegistrationName;
    }

    public void setParkRegistrationName(String parkRegistrationName) {
        this.parkRegistrationName = parkRegistrationName == null ? null : parkRegistrationName.trim();
    }

    public String getParkRegistrationCode() {
        return parkRegistrationCode;
    }

    public void setParkRegistrationCode(String parkRegistrationCode) {
        this.parkRegistrationCode = parkRegistrationCode == null ? null : parkRegistrationCode.trim();
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    public String getFixedLineTelephone() {
        return fixedLineTelephone;
    }

    public void setFixedLineTelephone(String fixedLineTelephone) {
        this.fixedLineTelephone = fixedLineTelephone == null ? null : fixedLineTelephone.trim();
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber == null ? null : cellPhoneNumber.trim();
    }

    public String getMajorBusiness1() {
        return majorBusiness1;
    }

    public void setMajorBusiness1(String majorBusiness1) {
        this.majorBusiness1 = majorBusiness1 == null ? null : majorBusiness1.trim();
    }

    public String getMajorBusiness2() {
        return majorBusiness2;
    }

    public void setMajorBusiness2(String majorBusiness2) {
        this.majorBusiness2 = majorBusiness2 == null ? null : majorBusiness2.trim();
    }

    public String getMajorBusiness3() {
        return majorBusiness3;
    }

    public void setMajorBusiness3(String majorBusiness3) {
        this.majorBusiness3 = majorBusiness3 == null ? null : majorBusiness3.trim();
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode == null ? null : industryCode.trim();
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName == null ? null : industryName.trim();
    }

    public String getVerifiedMajorBusiness1() {
        return verifiedMajorBusiness1;
    }

    public void setVerifiedMajorBusiness1(String verifiedMajorBusiness1) {
        this.verifiedMajorBusiness1 = verifiedMajorBusiness1 == null ? null : verifiedMajorBusiness1.trim();
    }

    public String getVerifiedMajorBusiness2() {
        return verifiedMajorBusiness2;
    }

    public void setVerifiedMajorBusiness2(String verifiedMajorBusiness2) {
        this.verifiedMajorBusiness2 = verifiedMajorBusiness2 == null ? null : verifiedMajorBusiness2.trim();
    }

    public String getVerifiedMajorBusiness3() {
        return verifiedMajorBusiness3;
    }

    public void setVerifiedMajorBusiness3(String verifiedMajorBusiness3) {
        this.verifiedMajorBusiness3 = verifiedMajorBusiness3 == null ? null : verifiedMajorBusiness3.trim();
    }

    public String getVerifiedIndustryCode() {
        return verifiedIndustryCode;
    }

    public void setVerifiedIndustryCode(String verifiedIndustryCode) {
        this.verifiedIndustryCode = verifiedIndustryCode == null ? null : verifiedIndustryCode.trim();
    }

    public String getVerifiedIndustryName() {
        return verifiedIndustryName;
    }

    public void setVerifiedIndustryName(String verifiedIndustryName) {
        this.verifiedIndustryName = verifiedIndustryName == null ? null : verifiedIndustryName.trim();
    }

    public String getMechanismType() {
        return mechanismType;
    }

    public void setMechanismType(String mechanismType) {
        this.mechanismType = mechanismType == null ? null : mechanismType.trim();
    }

    public Date getEstablishmentTime() {
        return establishmentTime;
    }

    public void setEstablishmentTime(Date establishmentTime) {
        this.establishmentTime = establishmentTime;
    }

    public String getBusinessState() {
        return businessState;
    }

    public void setBusinessState(String businessState) {
        this.businessState = businessState == null ? null : businessState.trim();
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType == null ? null : unitType.trim();
    }

    public Integer getVerifiedUnitType() {
        return verifiedUnitType;
    }

    public void setVerifiedUnitType(Integer verifiedUnitType) {
        this.verifiedUnitType = verifiedUnitType;
    }

    public String getAccountingStandardCategory() {
        return accountingStandardCategory;
    }

    public void setAccountingStandardCategory(String accountingStandardCategory) {
        this.accountingStandardCategory = accountingStandardCategory == null ? null : accountingStandardCategory.trim();
    }

    public Integer getProductEnergy() {
        return productEnergy;
    }

    public void setProductEnergy(Integer productEnergy) {
        this.productEnergy = productEnergy;
    }

    public Integer getSaleEnergy() {
        return saleEnergy;
    }

    public void setSaleEnergy(Integer saleEnergy) {
        this.saleEnergy = saleEnergy;
    }

    public Integer getIsHavaBrances() {
        return isHavaBrances;
    }

    public void setIsHavaBrances(Integer isHavaBrances) {
        this.isHavaBrances = isHavaBrances;
    }

    public Integer getCountBrances() {
        return countBrances;
    }

    public void setCountBrances(Integer countBrances) {
        this.countBrances = countBrances;
    }

    public Integer getLegalUnitType() {
        return legalUnitType;
    }

    public void setLegalUnitType(Integer legalUnitType) {
        this.legalUnitType = legalUnitType;
    }

    public String getLegalSocialCreditCode() {
        return legalSocialCreditCode;
    }

    public void setLegalSocialCreditCode(String legalSocialCreditCode) {
        this.legalSocialCreditCode = legalSocialCreditCode == null ? null : legalSocialCreditCode.trim();
    }

    public String getLegalOrganizationCode() {
        return legalOrganizationCode;
    }

    public void setLegalOrganizationCode(String legalOrganizationCode) {
        this.legalOrganizationCode = legalOrganizationCode == null ? null : legalOrganizationCode.trim();
    }

    public String getLegalUnitDetailedName() {
        return legalUnitDetailedName;
    }

    public void setLegalUnitDetailedName(String legalUnitDetailedName) {
        this.legalUnitDetailedName = legalUnitDetailedName == null ? null : legalUnitDetailedName.trim();
    }

    public String getLegalAddress() {
        return legalAddress;
    }

    public void setLegalAddress(String legalAddress) {
        this.legalAddress = legalAddress == null ? null : legalAddress.trim();
    }

    public String getLegalZoning() {
        return legalZoning;
    }

    public void setLegalZoning(String legalZoning) {
        this.legalZoning = legalZoning == null ? null : legalZoning.trim();
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode == null ? null : uniqueCode.trim();
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource == null ? null : dataSource.trim();
    }

    public String getPreparer() {
        return preparer;
    }

    public void setPreparer(String preparer) {
        this.preparer = preparer == null ? null : preparer.trim();
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber == null ? null : contactNumber.trim();
    }

    public Date getFormTime() {
        return formTime;
    }

    public void setFormTime(Date formTime) {
        this.formTime = formTime;
    }

    public String getStatisticLeader() {
        return statisticLeader;
    }

    public void setStatisticLeader(String statisticLeader) {
        this.statisticLeader = statisticLeader == null ? null : statisticLeader.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPreparerFixedLineTelephone() {
        return preparerFixedLineTelephone;
    }

    public void setPreparerFixedLineTelephone(String preparerFixedLineTelephone) {
        this.preparerFixedLineTelephone = preparerFixedLineTelephone == null ? null : preparerFixedLineTelephone.trim();
    }

    public String getUnitLeader() {
        return unitLeader;
    }

    public void setUnitLeader(String unitLeader) {
        this.unitLeader = unitLeader == null ? null : unitLeader.trim();
    }

    public String getReferenceAddress() {
        return referenceAddress;
    }

    public void setReferenceAddress(String referenceAddress) {
        this.referenceAddress = referenceAddress == null ? null : referenceAddress.trim();
    }

    public String getReferenceAddress02() {
        return referenceAddress02;
    }

    public void setReferenceAddress02(String referenceAddress02) {
        this.referenceAddress02 = referenceAddress02 == null ? null : referenceAddress02.trim();
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName == null ? null : buildingName.trim();
    }

    public String getTaxesRange() {
        return taxesRange;
    }

    public void setTaxesRange(String taxesRange) {
        this.taxesRange = taxesRange == null ? null : taxesRange.trim();
    }

    public String getRegisteredCapital() {
        return registeredCapital;
    }

    public void setRegisteredCapital(String registeredCapital) {
        this.registeredCapital = registeredCapital == null ? null : registeredCapital.trim();
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community == null ? null : community.trim();
    }

    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType == null ? null : registerType.trim();
    }

    public String getAppendix1() {
        return appendix1;
    }

    public void setAppendix1(String appendix1) {
        this.appendix1 = appendix1 == null ? null : appendix1.trim();
    }

    public String getAppendix2() {
        return appendix2;
    }

    public void setAppendix2(String appendix2) {
        this.appendix2 = appendix2 == null ? null : appendix2.trim();
    }

    public String getAppendix3() {
        return appendix3;
    }

    public void setAppendix3(String appendix3) {
        this.appendix3 = appendix3 == null ? null : appendix3.trim();
    }

    public Integer getUiteStatus() {
        return uiteStatus;
    }

    public void setUiteStatus(Integer uiteStatus) {
        this.uiteStatus = uiteStatus;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
}