package net.yunxinyong.sign.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class CecTwo {
    private Integer id;

    private Integer invId;

    private String organizationCode;

    private String socialCreditCode;

    private String unitDetailedName;

    private Integer finalEmployeesNumber;

    private Integer femaleNumber;

    private Integer postgraduateNumber;

    private Integer undergraduateNumber;

    private Integer collegeNumber;

    private Integer skilledWorkersNumber;

    private Integer skilledLevelOne;

    private Integer skilledLevelTwo;

    private Integer skilledLevelThree;

    private Integer skilledLevelFour;

    private Integer skilledLevelFive;

    private Integer workOutsideProvince;

    private Integer domicileOutsideProvince;

    private Integer workAndDomicileOutside;

    private Integer status;

    private Integer state;

    private String fillFormBy;

    private String phone;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvId() {
        return invId;
    }

    public void setInvId(Integer invId) {
        this.invId = invId;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName == null ? null : unitDetailedName.trim();
    }

    public Integer getFinalEmployeesNumber() {
        return finalEmployeesNumber;
    }

    public void setFinalEmployeesNumber(Integer finalEmployeesNumber) {
        this.finalEmployeesNumber = finalEmployeesNumber;
    }

    public Integer getFemaleNumber() {
        return femaleNumber;
    }

    public void setFemaleNumber(Integer femaleNumber) {
        this.femaleNumber = femaleNumber;
    }

    public Integer getPostgraduateNumber() {
        return postgraduateNumber;
    }

    public void setPostgraduateNumber(Integer postgraduateNumber) {
        this.postgraduateNumber = postgraduateNumber;
    }

    public Integer getUndergraduateNumber() {
        return undergraduateNumber;
    }

    public void setUndergraduateNumber(Integer undergraduateNumber) {
        this.undergraduateNumber = undergraduateNumber;
    }

    public Integer getCollegeNumber() {
        return collegeNumber;
    }

    public void setCollegeNumber(Integer collegeNumber) {
        this.collegeNumber = collegeNumber;
    }

    public Integer getSkilledWorkersNumber() {
        return skilledWorkersNumber;
    }

    public void setSkilledWorkersNumber(Integer skilledWorkersNumber) {
        this.skilledWorkersNumber = skilledWorkersNumber;
    }

    public Integer getSkilledLevelOne() {
        return skilledLevelOne;
    }

    public void setSkilledLevelOne(Integer skilledLevelOne) {
        this.skilledLevelOne = skilledLevelOne;
    }

    public Integer getSkilledLevelTwo() {
        return skilledLevelTwo;
    }

    public void setSkilledLevelTwo(Integer skilledLevelTwo) {
        this.skilledLevelTwo = skilledLevelTwo;
    }

    public Integer getSkilledLevelThree() {
        return skilledLevelThree;
    }

    public void setSkilledLevelThree(Integer skilledLevelThree) {
        this.skilledLevelThree = skilledLevelThree;
    }

    public Integer getSkilledLevelFour() {
        return skilledLevelFour;
    }

    public void setSkilledLevelFour(Integer skilledLevelFour) {
        this.skilledLevelFour = skilledLevelFour;
    }

    public Integer getSkilledLevelFive() {
        return skilledLevelFive;
    }

    public void setSkilledLevelFive(Integer skilledLevelFive) {
        this.skilledLevelFive = skilledLevelFive;
    }

    public Integer getWorkOutsideProvince() {
        return workOutsideProvince;
    }

    public void setWorkOutsideProvince(Integer workOutsideProvince) {
        this.workOutsideProvince = workOutsideProvince;
    }

    public Integer getDomicileOutsideProvince() {
        return domicileOutsideProvince;
    }

    public void setDomicileOutsideProvince(Integer domicileOutsideProvince) {
        this.domicileOutsideProvince = domicileOutsideProvince;
    }

    public Integer getWorkAndDomicileOutside() {
        return workAndDomicileOutside;
    }

    public void setWorkAndDomicileOutside(Integer workAndDomicileOutside) {
        this.workAndDomicileOutside = workAndDomicileOutside;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getFillFormBy() {
        return fillFormBy;
    }

    public void setFillFormBy(String fillFormBy) {
        this.fillFormBy = fillFormBy == null ? null : fillFormBy.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}