package net.yunxinyong.sign.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CecBaseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CecBaseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNull() {
            addCriterion("inv_id is null");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNotNull() {
            addCriterion("inv_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvIdEqualTo(Integer value) {
            addCriterion("inv_id =", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotEqualTo(Integer value) {
            addCriterion("inv_id <>", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThan(Integer value) {
            addCriterion("inv_id >", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("inv_id >=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThan(Integer value) {
            addCriterion("inv_id <", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThanOrEqualTo(Integer value) {
            addCriterion("inv_id <=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdIn(List<Integer> values) {
            addCriterion("inv_id in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotIn(List<Integer> values) {
            addCriterion("inv_id not in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdBetween(Integer value1, Integer value2) {
            addCriterion("inv_id between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotBetween(Integer value1, Integer value2) {
            addCriterion("inv_id not between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIsNull() {
            addCriterion("unit_type is null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIsNotNull() {
            addCriterion("unit_type is not null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeEqualTo(Integer value) {
            addCriterion("unit_type =", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotEqualTo(Integer value) {
            addCriterion("unit_type <>", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeGreaterThan(Integer value) {
            addCriterion("unit_type >", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("unit_type >=", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLessThan(Integer value) {
            addCriterion("unit_type <", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeLessThanOrEqualTo(Integer value) {
            addCriterion("unit_type <=", value, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeIn(List<Integer> values) {
            addCriterion("unit_type in", values, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotIn(List<Integer> values) {
            addCriterion("unit_type not in", values, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeBetween(Integer value1, Integer value2) {
            addCriterion("unit_type between", value1, value2, "unitType");
            return (Criteria) this;
        }

        public Criteria andUnitTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("unit_type not between", value1, value2, "unitType");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitIsNull() {
            addCriterion("is_corporate_unit is null");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitIsNotNull() {
            addCriterion("is_corporate_unit is not null");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitEqualTo(Integer value) {
            addCriterion("is_corporate_unit =", value, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitNotEqualTo(Integer value) {
            addCriterion("is_corporate_unit <>", value, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitGreaterThan(Integer value) {
            addCriterion("is_corporate_unit >", value, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_corporate_unit >=", value, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitLessThan(Integer value) {
            addCriterion("is_corporate_unit <", value, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitLessThanOrEqualTo(Integer value) {
            addCriterion("is_corporate_unit <=", value, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitIn(List<Integer> values) {
            addCriterion("is_corporate_unit in", values, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitNotIn(List<Integer> values) {
            addCriterion("is_corporate_unit not in", values, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitBetween(Integer value1, Integer value2) {
            addCriterion("is_corporate_unit between", value1, value2, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andIsCorporateUnitNotBetween(Integer value1, Integer value2) {
            addCriterion("is_corporate_unit not between", value1, value2, "isCorporateUnit");
            return (Criteria) this;
        }

        public Criteria andCellCodeIsNull() {
            addCriterion("cell_code is null");
            return (Criteria) this;
        }

        public Criteria andCellCodeIsNotNull() {
            addCriterion("cell_code is not null");
            return (Criteria) this;
        }

        public Criteria andCellCodeEqualTo(String value) {
            addCriterion("cell_code =", value, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeNotEqualTo(String value) {
            addCriterion("cell_code <>", value, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeGreaterThan(String value) {
            addCriterion("cell_code >", value, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeGreaterThanOrEqualTo(String value) {
            addCriterion("cell_code >=", value, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeLessThan(String value) {
            addCriterion("cell_code <", value, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeLessThanOrEqualTo(String value) {
            addCriterion("cell_code <=", value, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeLike(String value) {
            addCriterion("cell_code like", value, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeNotLike(String value) {
            addCriterion("cell_code not like", value, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeIn(List<String> values) {
            addCriterion("cell_code in", values, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeNotIn(List<String> values) {
            addCriterion("cell_code not in", values, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeBetween(String value1, String value2) {
            addCriterion("cell_code between", value1, value2, "cellCode");
            return (Criteria) this;
        }

        public Criteria andCellCodeNotBetween(String value1, String value2) {
            addCriterion("cell_code not between", value1, value2, "cellCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeIsNull() {
            addCriterion("building_code is null");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeIsNotNull() {
            addCriterion("building_code is not null");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeEqualTo(String value) {
            addCriterion("building_code =", value, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeNotEqualTo(String value) {
            addCriterion("building_code <>", value, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeGreaterThan(String value) {
            addCriterion("building_code >", value, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeGreaterThanOrEqualTo(String value) {
            addCriterion("building_code >=", value, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeLessThan(String value) {
            addCriterion("building_code <", value, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeLessThanOrEqualTo(String value) {
            addCriterion("building_code <=", value, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeLike(String value) {
            addCriterion("building_code like", value, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeNotLike(String value) {
            addCriterion("building_code not like", value, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeIn(List<String> values) {
            addCriterion("building_code in", values, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeNotIn(List<String> values) {
            addCriterion("building_code not in", values, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeBetween(String value1, String value2) {
            addCriterion("building_code between", value1, value2, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBuildingCodeNotBetween(String value1, String value2) {
            addCriterion("building_code not between", value1, value2, "buildingCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeIsNull() {
            addCriterion("book_sequence_code is null");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeIsNotNull() {
            addCriterion("book_sequence_code is not null");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeEqualTo(String value) {
            addCriterion("book_sequence_code =", value, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeNotEqualTo(String value) {
            addCriterion("book_sequence_code <>", value, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeGreaterThan(String value) {
            addCriterion("book_sequence_code >", value, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeGreaterThanOrEqualTo(String value) {
            addCriterion("book_sequence_code >=", value, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeLessThan(String value) {
            addCriterion("book_sequence_code <", value, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeLessThanOrEqualTo(String value) {
            addCriterion("book_sequence_code <=", value, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeLike(String value) {
            addCriterion("book_sequence_code like", value, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeNotLike(String value) {
            addCriterion("book_sequence_code not like", value, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeIn(List<String> values) {
            addCriterion("book_sequence_code in", values, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeNotIn(List<String> values) {
            addCriterion("book_sequence_code not in", values, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeBetween(String value1, String value2) {
            addCriterion("book_sequence_code between", value1, value2, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andBookSequenceCodeNotBetween(String value1, String value2) {
            addCriterion("book_sequence_code not between", value1, value2, "bookSequenceCode");
            return (Criteria) this;
        }

        public Criteria andMajorTypeIsNull() {
            addCriterion("major_type is null");
            return (Criteria) this;
        }

        public Criteria andMajorTypeIsNotNull() {
            addCriterion("major_type is not null");
            return (Criteria) this;
        }

        public Criteria andMajorTypeEqualTo(String value) {
            addCriterion("major_type =", value, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeNotEqualTo(String value) {
            addCriterion("major_type <>", value, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeGreaterThan(String value) {
            addCriterion("major_type >", value, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeGreaterThanOrEqualTo(String value) {
            addCriterion("major_type >=", value, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeLessThan(String value) {
            addCriterion("major_type <", value, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeLessThanOrEqualTo(String value) {
            addCriterion("major_type <=", value, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeLike(String value) {
            addCriterion("major_type like", value, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeNotLike(String value) {
            addCriterion("major_type not like", value, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeIn(List<String> values) {
            addCriterion("major_type in", values, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeNotIn(List<String> values) {
            addCriterion("major_type not in", values, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeBetween(String value1, String value2) {
            addCriterion("major_type between", value1, value2, "majorType");
            return (Criteria) this;
        }

        public Criteria andMajorTypeNotBetween(String value1, String value2) {
            addCriterion("major_type not between", value1, value2, "majorType");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNull() {
            addCriterion("organization_code is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNotNull() {
            addCriterion("organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeEqualTo(String value) {
            addCriterion("organization_code =", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotEqualTo(String value) {
            addCriterion("organization_code <>", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThan(String value) {
            addCriterion("organization_code >", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("organization_code >=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThan(String value) {
            addCriterion("organization_code <", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("organization_code <=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLike(String value) {
            addCriterion("organization_code like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotLike(String value) {
            addCriterion("organization_code not like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIn(List<String> values) {
            addCriterion("organization_code in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotIn(List<String> values) {
            addCriterion("organization_code not in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeBetween(String value1, String value2) {
            addCriterion("organization_code between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("organization_code not between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNull() {
            addCriterion("social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNotNull() {
            addCriterion("social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeEqualTo(String value) {
            addCriterion("social_credit_code =", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotEqualTo(String value) {
            addCriterion("social_credit_code <>", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThan(String value) {
            addCriterion("social_credit_code >", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("social_credit_code >=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThan(String value) {
            addCriterion("social_credit_code <", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("social_credit_code <=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLike(String value) {
            addCriterion("social_credit_code like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotLike(String value) {
            addCriterion("social_credit_code not like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIn(List<String> values) {
            addCriterion("social_credit_code in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotIn(List<String> values) {
            addCriterion("social_credit_code not in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("social_credit_code between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("social_credit_code not between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNull() {
            addCriterion("unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNotNull() {
            addCriterion("unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameEqualTo(String value) {
            addCriterion("unit_detailed_name =", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotEqualTo(String value) {
            addCriterion("unit_detailed_name <>", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThan(String value) {
            addCriterion("unit_detailed_name >", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name >=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThan(String value) {
            addCriterion("unit_detailed_name <", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name <=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLike(String value) {
            addCriterion("unit_detailed_name like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotLike(String value) {
            addCriterion("unit_detailed_name not like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIn(List<String> values) {
            addCriterion("unit_detailed_name in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotIn(List<String> values) {
            addCriterion("unit_detailed_name not in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("unit_detailed_name between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("unit_detailed_name not between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeIsNull() {
            addCriterion("legal_representative is null");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeIsNotNull() {
            addCriterion("legal_representative is not null");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeEqualTo(String value) {
            addCriterion("legal_representative =", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeNotEqualTo(String value) {
            addCriterion("legal_representative <>", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeGreaterThan(String value) {
            addCriterion("legal_representative >", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeGreaterThanOrEqualTo(String value) {
            addCriterion("legal_representative >=", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeLessThan(String value) {
            addCriterion("legal_representative <", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeLessThanOrEqualTo(String value) {
            addCriterion("legal_representative <=", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeLike(String value) {
            addCriterion("legal_representative like", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeNotLike(String value) {
            addCriterion("legal_representative not like", value, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeIn(List<String> values) {
            addCriterion("legal_representative in", values, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeNotIn(List<String> values) {
            addCriterion("legal_representative not in", values, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeBetween(String value1, String value2) {
            addCriterion("legal_representative between", value1, value2, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andLegalRepresentativeNotBetween(String value1, String value2) {
            addCriterion("legal_representative not between", value1, value2, "legalRepresentative");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeIsNull() {
            addCriterion("establishment_time is null");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeIsNotNull() {
            addCriterion("establishment_time is not null");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeEqualTo(String value) {
            addCriterion("establishment_time =", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeNotEqualTo(String value) {
            addCriterion("establishment_time <>", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeGreaterThan(String value) {
            addCriterion("establishment_time >", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeGreaterThanOrEqualTo(String value) {
            addCriterion("establishment_time >=", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeLessThan(String value) {
            addCriterion("establishment_time <", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeLessThanOrEqualTo(String value) {
            addCriterion("establishment_time <=", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeLike(String value) {
            addCriterion("establishment_time like", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeNotLike(String value) {
            addCriterion("establishment_time not like", value, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeIn(List<String> values) {
            addCriterion("establishment_time in", values, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeNotIn(List<String> values) {
            addCriterion("establishment_time not in", values, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeBetween(String value1, String value2) {
            addCriterion("establishment_time between", value1, value2, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andEstablishmentTimeNotBetween(String value1, String value2) {
            addCriterion("establishment_time not between", value1, value2, "establishmentTime");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIsNull() {
            addCriterion("area_code is null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIsNotNull() {
            addCriterion("area_code is not null");
            return (Criteria) this;
        }

        public Criteria andAreaCodeEqualTo(String value) {
            addCriterion("area_code =", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotEqualTo(String value) {
            addCriterion("area_code <>", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThan(String value) {
            addCriterion("area_code >", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeGreaterThanOrEqualTo(String value) {
            addCriterion("area_code >=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThan(String value) {
            addCriterion("area_code <", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLessThanOrEqualTo(String value) {
            addCriterion("area_code <=", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeLike(String value) {
            addCriterion("area_code like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotLike(String value) {
            addCriterion("area_code not like", value, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeIn(List<String> values) {
            addCriterion("area_code in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotIn(List<String> values) {
            addCriterion("area_code not in", values, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeBetween(String value1, String value2) {
            addCriterion("area_code between", value1, value2, "areaCode");
            return (Criteria) this;
        }

        public Criteria andAreaCodeNotBetween(String value1, String value2) {
            addCriterion("area_code not between", value1, value2, "areaCode");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneIsNull() {
            addCriterion("fixed_line_telephone is null");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneIsNotNull() {
            addCriterion("fixed_line_telephone is not null");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneEqualTo(String value) {
            addCriterion("fixed_line_telephone =", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneNotEqualTo(String value) {
            addCriterion("fixed_line_telephone <>", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneGreaterThan(String value) {
            addCriterion("fixed_line_telephone >", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("fixed_line_telephone >=", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneLessThan(String value) {
            addCriterion("fixed_line_telephone <", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneLessThanOrEqualTo(String value) {
            addCriterion("fixed_line_telephone <=", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneLike(String value) {
            addCriterion("fixed_line_telephone like", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneNotLike(String value) {
            addCriterion("fixed_line_telephone not like", value, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneIn(List<String> values) {
            addCriterion("fixed_line_telephone in", values, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneNotIn(List<String> values) {
            addCriterion("fixed_line_telephone not in", values, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneBetween(String value1, String value2) {
            addCriterion("fixed_line_telephone between", value1, value2, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andFixedLineTelephoneNotBetween(String value1, String value2) {
            addCriterion("fixed_line_telephone not between", value1, value2, "fixedLineTelephone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberIsNull() {
            addCriterion("cell_phone_number is null");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberIsNotNull() {
            addCriterion("cell_phone_number is not null");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberEqualTo(String value) {
            addCriterion("cell_phone_number =", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberNotEqualTo(String value) {
            addCriterion("cell_phone_number <>", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberGreaterThan(String value) {
            addCriterion("cell_phone_number >", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberGreaterThanOrEqualTo(String value) {
            addCriterion("cell_phone_number >=", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberLessThan(String value) {
            addCriterion("cell_phone_number <", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberLessThanOrEqualTo(String value) {
            addCriterion("cell_phone_number <=", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberLike(String value) {
            addCriterion("cell_phone_number like", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberNotLike(String value) {
            addCriterion("cell_phone_number not like", value, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberIn(List<String> values) {
            addCriterion("cell_phone_number in", values, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberNotIn(List<String> values) {
            addCriterion("cell_phone_number not in", values, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberBetween(String value1, String value2) {
            addCriterion("cell_phone_number between", value1, value2, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNumberNotBetween(String value1, String value2) {
            addCriterion("cell_phone_number not between", value1, value2, "cellPhoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberIsNull() {
            addCriterion("fax_number is null");
            return (Criteria) this;
        }

        public Criteria andFaxNumberIsNotNull() {
            addCriterion("fax_number is not null");
            return (Criteria) this;
        }

        public Criteria andFaxNumberEqualTo(String value) {
            addCriterion("fax_number =", value, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberNotEqualTo(String value) {
            addCriterion("fax_number <>", value, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberGreaterThan(String value) {
            addCriterion("fax_number >", value, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberGreaterThanOrEqualTo(String value) {
            addCriterion("fax_number >=", value, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberLessThan(String value) {
            addCriterion("fax_number <", value, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberLessThanOrEqualTo(String value) {
            addCriterion("fax_number <=", value, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberLike(String value) {
            addCriterion("fax_number like", value, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberNotLike(String value) {
            addCriterion("fax_number not like", value, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberIn(List<String> values) {
            addCriterion("fax_number in", values, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberNotIn(List<String> values) {
            addCriterion("fax_number not in", values, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberBetween(String value1, String value2) {
            addCriterion("fax_number between", value1, value2, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andFaxNumberNotBetween(String value1, String value2) {
            addCriterion("fax_number not between", value1, value2, "faxNumber");
            return (Criteria) this;
        }

        public Criteria andPostalCodeIsNull() {
            addCriterion("postal_code is null");
            return (Criteria) this;
        }

        public Criteria andPostalCodeIsNotNull() {
            addCriterion("postal_code is not null");
            return (Criteria) this;
        }

        public Criteria andPostalCodeEqualTo(String value) {
            addCriterion("postal_code =", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeNotEqualTo(String value) {
            addCriterion("postal_code <>", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeGreaterThan(String value) {
            addCriterion("postal_code >", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeGreaterThanOrEqualTo(String value) {
            addCriterion("postal_code >=", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeLessThan(String value) {
            addCriterion("postal_code <", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeLessThanOrEqualTo(String value) {
            addCriterion("postal_code <=", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeLike(String value) {
            addCriterion("postal_code like", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeNotLike(String value) {
            addCriterion("postal_code not like", value, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeIn(List<String> values) {
            addCriterion("postal_code in", values, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeNotIn(List<String> values) {
            addCriterion("postal_code not in", values, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeBetween(String value1, String value2) {
            addCriterion("postal_code between", value1, value2, "postalCode");
            return (Criteria) this;
        }

        public Criteria andPostalCodeNotBetween(String value1, String value2) {
            addCriterion("postal_code not between", value1, value2, "postalCode");
            return (Criteria) this;
        }

        public Criteria andEMailIsNull() {
            addCriterion("e_mail is null");
            return (Criteria) this;
        }

        public Criteria andEMailIsNotNull() {
            addCriterion("e_mail is not null");
            return (Criteria) this;
        }

        public Criteria andEMailEqualTo(String value) {
            addCriterion("e_mail =", value, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailNotEqualTo(String value) {
            addCriterion("e_mail <>", value, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailGreaterThan(String value) {
            addCriterion("e_mail >", value, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailGreaterThanOrEqualTo(String value) {
            addCriterion("e_mail >=", value, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailLessThan(String value) {
            addCriterion("e_mail <", value, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailLessThanOrEqualTo(String value) {
            addCriterion("e_mail <=", value, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailLike(String value) {
            addCriterion("e_mail like", value, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailNotLike(String value) {
            addCriterion("e_mail not like", value, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailIn(List<String> values) {
            addCriterion("e_mail in", values, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailNotIn(List<String> values) {
            addCriterion("e_mail not in", values, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailBetween(String value1, String value2) {
            addCriterion("e_mail between", value1, value2, "eMail");
            return (Criteria) this;
        }

        public Criteria andEMailNotBetween(String value1, String value2) {
            addCriterion("e_mail not between", value1, value2, "eMail");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsNull() {
            addCriterion("website is null");
            return (Criteria) this;
        }

        public Criteria andWebsiteIsNotNull() {
            addCriterion("website is not null");
            return (Criteria) this;
        }

        public Criteria andWebsiteEqualTo(String value) {
            addCriterion("website =", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotEqualTo(String value) {
            addCriterion("website <>", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteGreaterThan(String value) {
            addCriterion("website >", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteGreaterThanOrEqualTo(String value) {
            addCriterion("website >=", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLessThan(String value) {
            addCriterion("website <", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLessThanOrEqualTo(String value) {
            addCriterion("website <=", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteLike(String value) {
            addCriterion("website like", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotLike(String value) {
            addCriterion("website not like", value, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteIn(List<String> values) {
            addCriterion("website in", values, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotIn(List<String> values) {
            addCriterion("website not in", values, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteBetween(String value1, String value2) {
            addCriterion("website between", value1, value2, "website");
            return (Criteria) this;
        }

        public Criteria andWebsiteNotBetween(String value1, String value2) {
            addCriterion("website not between", value1, value2, "website");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("city like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("city not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCountyIsNull() {
            addCriterion("county is null");
            return (Criteria) this;
        }

        public Criteria andCountyIsNotNull() {
            addCriterion("county is not null");
            return (Criteria) this;
        }

        public Criteria andCountyEqualTo(String value) {
            addCriterion("county =", value, "county");
            return (Criteria) this;
        }

        public Criteria andCountyNotEqualTo(String value) {
            addCriterion("county <>", value, "county");
            return (Criteria) this;
        }

        public Criteria andCountyGreaterThan(String value) {
            addCriterion("county >", value, "county");
            return (Criteria) this;
        }

        public Criteria andCountyGreaterThanOrEqualTo(String value) {
            addCriterion("county >=", value, "county");
            return (Criteria) this;
        }

        public Criteria andCountyLessThan(String value) {
            addCriterion("county <", value, "county");
            return (Criteria) this;
        }

        public Criteria andCountyLessThanOrEqualTo(String value) {
            addCriterion("county <=", value, "county");
            return (Criteria) this;
        }

        public Criteria andCountyLike(String value) {
            addCriterion("county like", value, "county");
            return (Criteria) this;
        }

        public Criteria andCountyNotLike(String value) {
            addCriterion("county not like", value, "county");
            return (Criteria) this;
        }

        public Criteria andCountyIn(List<String> values) {
            addCriterion("county in", values, "county");
            return (Criteria) this;
        }

        public Criteria andCountyNotIn(List<String> values) {
            addCriterion("county not in", values, "county");
            return (Criteria) this;
        }

        public Criteria andCountyBetween(String value1, String value2) {
            addCriterion("county between", value1, value2, "county");
            return (Criteria) this;
        }

        public Criteria andCountyNotBetween(String value1, String value2) {
            addCriterion("county not between", value1, value2, "county");
            return (Criteria) this;
        }

        public Criteria andTownsIsNull() {
            addCriterion("towns is null");
            return (Criteria) this;
        }

        public Criteria andTownsIsNotNull() {
            addCriterion("towns is not null");
            return (Criteria) this;
        }

        public Criteria andTownsEqualTo(String value) {
            addCriterion("towns =", value, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsNotEqualTo(String value) {
            addCriterion("towns <>", value, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsGreaterThan(String value) {
            addCriterion("towns >", value, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsGreaterThanOrEqualTo(String value) {
            addCriterion("towns >=", value, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsLessThan(String value) {
            addCriterion("towns <", value, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsLessThanOrEqualTo(String value) {
            addCriterion("towns <=", value, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsLike(String value) {
            addCriterion("towns like", value, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsNotLike(String value) {
            addCriterion("towns not like", value, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsIn(List<String> values) {
            addCriterion("towns in", values, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsNotIn(List<String> values) {
            addCriterion("towns not in", values, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsBetween(String value1, String value2) {
            addCriterion("towns between", value1, value2, "towns");
            return (Criteria) this;
        }

        public Criteria andTownsNotBetween(String value1, String value2) {
            addCriterion("towns not between", value1, value2, "towns");
            return (Criteria) this;
        }

        public Criteria andVillageIsNull() {
            addCriterion("village is null");
            return (Criteria) this;
        }

        public Criteria andVillageIsNotNull() {
            addCriterion("village is not null");
            return (Criteria) this;
        }

        public Criteria andVillageEqualTo(String value) {
            addCriterion("village =", value, "village");
            return (Criteria) this;
        }

        public Criteria andVillageNotEqualTo(String value) {
            addCriterion("village <>", value, "village");
            return (Criteria) this;
        }

        public Criteria andVillageGreaterThan(String value) {
            addCriterion("village >", value, "village");
            return (Criteria) this;
        }

        public Criteria andVillageGreaterThanOrEqualTo(String value) {
            addCriterion("village >=", value, "village");
            return (Criteria) this;
        }

        public Criteria andVillageLessThan(String value) {
            addCriterion("village <", value, "village");
            return (Criteria) this;
        }

        public Criteria andVillageLessThanOrEqualTo(String value) {
            addCriterion("village <=", value, "village");
            return (Criteria) this;
        }

        public Criteria andVillageLike(String value) {
            addCriterion("village like", value, "village");
            return (Criteria) this;
        }

        public Criteria andVillageNotLike(String value) {
            addCriterion("village not like", value, "village");
            return (Criteria) this;
        }

        public Criteria andVillageIn(List<String> values) {
            addCriterion("village in", values, "village");
            return (Criteria) this;
        }

        public Criteria andVillageNotIn(List<String> values) {
            addCriterion("village not in", values, "village");
            return (Criteria) this;
        }

        public Criteria andVillageBetween(String value1, String value2) {
            addCriterion("village between", value1, value2, "village");
            return (Criteria) this;
        }

        public Criteria andVillageNotBetween(String value1, String value2) {
            addCriterion("village not between", value1, value2, "village");
            return (Criteria) this;
        }

        public Criteria andSubdistrictIsNull() {
            addCriterion("subdistrict is null");
            return (Criteria) this;
        }

        public Criteria andSubdistrictIsNotNull() {
            addCriterion("subdistrict is not null");
            return (Criteria) this;
        }

        public Criteria andSubdistrictEqualTo(String value) {
            addCriterion("subdistrict =", value, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictNotEqualTo(String value) {
            addCriterion("subdistrict <>", value, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictGreaterThan(String value) {
            addCriterion("subdistrict >", value, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictGreaterThanOrEqualTo(String value) {
            addCriterion("subdistrict >=", value, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictLessThan(String value) {
            addCriterion("subdistrict <", value, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictLessThanOrEqualTo(String value) {
            addCriterion("subdistrict <=", value, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictLike(String value) {
            addCriterion("subdistrict like", value, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictNotLike(String value) {
            addCriterion("subdistrict not like", value, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictIn(List<String> values) {
            addCriterion("subdistrict in", values, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictNotIn(List<String> values) {
            addCriterion("subdistrict not in", values, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictBetween(String value1, String value2) {
            addCriterion("subdistrict between", value1, value2, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andSubdistrictNotBetween(String value1, String value2) {
            addCriterion("subdistrict not between", value1, value2, "subdistrict");
            return (Criteria) this;
        }

        public Criteria andCummunityIsNull() {
            addCriterion("cummunity is null");
            return (Criteria) this;
        }

        public Criteria andCummunityIsNotNull() {
            addCriterion("cummunity is not null");
            return (Criteria) this;
        }

        public Criteria andCummunityEqualTo(String value) {
            addCriterion("cummunity =", value, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityNotEqualTo(String value) {
            addCriterion("cummunity <>", value, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityGreaterThan(String value) {
            addCriterion("cummunity >", value, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityGreaterThanOrEqualTo(String value) {
            addCriterion("cummunity >=", value, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityLessThan(String value) {
            addCriterion("cummunity <", value, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityLessThanOrEqualTo(String value) {
            addCriterion("cummunity <=", value, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityLike(String value) {
            addCriterion("cummunity like", value, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityNotLike(String value) {
            addCriterion("cummunity not like", value, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityIn(List<String> values) {
            addCriterion("cummunity in", values, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityNotIn(List<String> values) {
            addCriterion("cummunity not in", values, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityBetween(String value1, String value2) {
            addCriterion("cummunity between", value1, value2, "cummunity");
            return (Criteria) this;
        }

        public Criteria andCummunityNotBetween(String value1, String value2) {
            addCriterion("cummunity not between", value1, value2, "cummunity");
            return (Criteria) this;
        }

        public Criteria andParkNameIsNull() {
            addCriterion("park_name is null");
            return (Criteria) this;
        }

        public Criteria andParkNameIsNotNull() {
            addCriterion("park_name is not null");
            return (Criteria) this;
        }

        public Criteria andParkNameEqualTo(String value) {
            addCriterion("park_name =", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameNotEqualTo(String value) {
            addCriterion("park_name <>", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameGreaterThan(String value) {
            addCriterion("park_name >", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameGreaterThanOrEqualTo(String value) {
            addCriterion("park_name >=", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameLessThan(String value) {
            addCriterion("park_name <", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameLessThanOrEqualTo(String value) {
            addCriterion("park_name <=", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameLike(String value) {
            addCriterion("park_name like", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameNotLike(String value) {
            addCriterion("park_name not like", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameIn(List<String> values) {
            addCriterion("park_name in", values, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameNotIn(List<String> values) {
            addCriterion("park_name not in", values, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameBetween(String value1, String value2) {
            addCriterion("park_name between", value1, value2, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameNotBetween(String value1, String value2) {
            addCriterion("park_name not between", value1, value2, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkCodeIsNull() {
            addCriterion("park_code is null");
            return (Criteria) this;
        }

        public Criteria andParkCodeIsNotNull() {
            addCriterion("park_code is not null");
            return (Criteria) this;
        }

        public Criteria andParkCodeEqualTo(String value) {
            addCriterion("park_code =", value, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeNotEqualTo(String value) {
            addCriterion("park_code <>", value, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeGreaterThan(String value) {
            addCriterion("park_code >", value, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeGreaterThanOrEqualTo(String value) {
            addCriterion("park_code >=", value, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeLessThan(String value) {
            addCriterion("park_code <", value, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeLessThanOrEqualTo(String value) {
            addCriterion("park_code <=", value, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeLike(String value) {
            addCriterion("park_code like", value, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeNotLike(String value) {
            addCriterion("park_code not like", value, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeIn(List<String> values) {
            addCriterion("park_code in", values, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeNotIn(List<String> values) {
            addCriterion("park_code not in", values, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeBetween(String value1, String value2) {
            addCriterion("park_code between", value1, value2, "parkCode");
            return (Criteria) this;
        }

        public Criteria andParkCodeNotBetween(String value1, String value2) {
            addCriterion("park_code not between", value1, value2, "parkCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeIsNull() {
            addCriterion("unit_zoning_code is null");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeIsNotNull() {
            addCriterion("unit_zoning_code is not null");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeEqualTo(String value) {
            addCriterion("unit_zoning_code =", value, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeNotEqualTo(String value) {
            addCriterion("unit_zoning_code <>", value, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeGreaterThan(String value) {
            addCriterion("unit_zoning_code >", value, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeGreaterThanOrEqualTo(String value) {
            addCriterion("unit_zoning_code >=", value, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeLessThan(String value) {
            addCriterion("unit_zoning_code <", value, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeLessThanOrEqualTo(String value) {
            addCriterion("unit_zoning_code <=", value, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeLike(String value) {
            addCriterion("unit_zoning_code like", value, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeNotLike(String value) {
            addCriterion("unit_zoning_code not like", value, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeIn(List<String> values) {
            addCriterion("unit_zoning_code in", values, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeNotIn(List<String> values) {
            addCriterion("unit_zoning_code not in", values, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeBetween(String value1, String value2) {
            addCriterion("unit_zoning_code between", value1, value2, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeNotBetween(String value1, String value2) {
            addCriterion("unit_zoning_code not between", value1, value2, "unitZoningCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeIsNull() {
            addCriterion("unit_country_code is null");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeIsNotNull() {
            addCriterion("unit_country_code is not null");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeEqualTo(String value) {
            addCriterion("unit_country_code =", value, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeNotEqualTo(String value) {
            addCriterion("unit_country_code <>", value, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeGreaterThan(String value) {
            addCriterion("unit_country_code >", value, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeGreaterThanOrEqualTo(String value) {
            addCriterion("unit_country_code >=", value, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeLessThan(String value) {
            addCriterion("unit_country_code <", value, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeLessThanOrEqualTo(String value) {
            addCriterion("unit_country_code <=", value, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeLike(String value) {
            addCriterion("unit_country_code like", value, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeNotLike(String value) {
            addCriterion("unit_country_code not like", value, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeIn(List<String> values) {
            addCriterion("unit_country_code in", values, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeNotIn(List<String> values) {
            addCriterion("unit_country_code not in", values, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeBetween(String value1, String value2) {
            addCriterion("unit_country_code between", value1, value2, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andUnitCountryCodeNotBetween(String value1, String value2) {
            addCriterion("unit_country_code not between", value1, value2, "unitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceIsNull() {
            addCriterion("register_province is null");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceIsNotNull() {
            addCriterion("register_province is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceEqualTo(String value) {
            addCriterion("register_province =", value, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceNotEqualTo(String value) {
            addCriterion("register_province <>", value, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceGreaterThan(String value) {
            addCriterion("register_province >", value, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("register_province >=", value, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceLessThan(String value) {
            addCriterion("register_province <", value, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceLessThanOrEqualTo(String value) {
            addCriterion("register_province <=", value, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceLike(String value) {
            addCriterion("register_province like", value, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceNotLike(String value) {
            addCriterion("register_province not like", value, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceIn(List<String> values) {
            addCriterion("register_province in", values, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceNotIn(List<String> values) {
            addCriterion("register_province not in", values, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceBetween(String value1, String value2) {
            addCriterion("register_province between", value1, value2, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterProvinceNotBetween(String value1, String value2) {
            addCriterion("register_province not between", value1, value2, "registerProvince");
            return (Criteria) this;
        }

        public Criteria andRegisterCityIsNull() {
            addCriterion("register_city is null");
            return (Criteria) this;
        }

        public Criteria andRegisterCityIsNotNull() {
            addCriterion("register_city is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterCityEqualTo(String value) {
            addCriterion("register_city =", value, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityNotEqualTo(String value) {
            addCriterion("register_city <>", value, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityGreaterThan(String value) {
            addCriterion("register_city >", value, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityGreaterThanOrEqualTo(String value) {
            addCriterion("register_city >=", value, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityLessThan(String value) {
            addCriterion("register_city <", value, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityLessThanOrEqualTo(String value) {
            addCriterion("register_city <=", value, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityLike(String value) {
            addCriterion("register_city like", value, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityNotLike(String value) {
            addCriterion("register_city not like", value, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityIn(List<String> values) {
            addCriterion("register_city in", values, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityNotIn(List<String> values) {
            addCriterion("register_city not in", values, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityBetween(String value1, String value2) {
            addCriterion("register_city between", value1, value2, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCityNotBetween(String value1, String value2) {
            addCriterion("register_city not between", value1, value2, "registerCity");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyIsNull() {
            addCriterion("register_county is null");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyIsNotNull() {
            addCriterion("register_county is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyEqualTo(String value) {
            addCriterion("register_county =", value, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyNotEqualTo(String value) {
            addCriterion("register_county <>", value, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyGreaterThan(String value) {
            addCriterion("register_county >", value, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyGreaterThanOrEqualTo(String value) {
            addCriterion("register_county >=", value, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyLessThan(String value) {
            addCriterion("register_county <", value, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyLessThanOrEqualTo(String value) {
            addCriterion("register_county <=", value, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyLike(String value) {
            addCriterion("register_county like", value, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyNotLike(String value) {
            addCriterion("register_county not like", value, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyIn(List<String> values) {
            addCriterion("register_county in", values, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyNotIn(List<String> values) {
            addCriterion("register_county not in", values, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyBetween(String value1, String value2) {
            addCriterion("register_county between", value1, value2, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterCountyNotBetween(String value1, String value2) {
            addCriterion("register_county not between", value1, value2, "registerCounty");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsIsNull() {
            addCriterion("register_towns is null");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsIsNotNull() {
            addCriterion("register_towns is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsEqualTo(String value) {
            addCriterion("register_towns =", value, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsNotEqualTo(String value) {
            addCriterion("register_towns <>", value, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsGreaterThan(String value) {
            addCriterion("register_towns >", value, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsGreaterThanOrEqualTo(String value) {
            addCriterion("register_towns >=", value, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsLessThan(String value) {
            addCriterion("register_towns <", value, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsLessThanOrEqualTo(String value) {
            addCriterion("register_towns <=", value, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsLike(String value) {
            addCriterion("register_towns like", value, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsNotLike(String value) {
            addCriterion("register_towns not like", value, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsIn(List<String> values) {
            addCriterion("register_towns in", values, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsNotIn(List<String> values) {
            addCriterion("register_towns not in", values, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsBetween(String value1, String value2) {
            addCriterion("register_towns between", value1, value2, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterTownsNotBetween(String value1, String value2) {
            addCriterion("register_towns not between", value1, value2, "registerTowns");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageIsNull() {
            addCriterion("register_village is null");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageIsNotNull() {
            addCriterion("register_village is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageEqualTo(String value) {
            addCriterion("register_village =", value, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageNotEqualTo(String value) {
            addCriterion("register_village <>", value, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageGreaterThan(String value) {
            addCriterion("register_village >", value, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageGreaterThanOrEqualTo(String value) {
            addCriterion("register_village >=", value, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageLessThan(String value) {
            addCriterion("register_village <", value, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageLessThanOrEqualTo(String value) {
            addCriterion("register_village <=", value, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageLike(String value) {
            addCriterion("register_village like", value, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageNotLike(String value) {
            addCriterion("register_village not like", value, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageIn(List<String> values) {
            addCriterion("register_village in", values, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageNotIn(List<String> values) {
            addCriterion("register_village not in", values, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageBetween(String value1, String value2) {
            addCriterion("register_village between", value1, value2, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterVillageNotBetween(String value1, String value2) {
            addCriterion("register_village not between", value1, value2, "registerVillage");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictIsNull() {
            addCriterion("register_subdistrict is null");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictIsNotNull() {
            addCriterion("register_subdistrict is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictEqualTo(String value) {
            addCriterion("register_subdistrict =", value, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictNotEqualTo(String value) {
            addCriterion("register_subdistrict <>", value, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictGreaterThan(String value) {
            addCriterion("register_subdistrict >", value, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictGreaterThanOrEqualTo(String value) {
            addCriterion("register_subdistrict >=", value, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictLessThan(String value) {
            addCriterion("register_subdistrict <", value, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictLessThanOrEqualTo(String value) {
            addCriterion("register_subdistrict <=", value, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictLike(String value) {
            addCriterion("register_subdistrict like", value, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictNotLike(String value) {
            addCriterion("register_subdistrict not like", value, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictIn(List<String> values) {
            addCriterion("register_subdistrict in", values, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictNotIn(List<String> values) {
            addCriterion("register_subdistrict not in", values, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictBetween(String value1, String value2) {
            addCriterion("register_subdistrict between", value1, value2, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterSubdistrictNotBetween(String value1, String value2) {
            addCriterion("register_subdistrict not between", value1, value2, "registerSubdistrict");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityIsNull() {
            addCriterion("register_cummunity is null");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityIsNotNull() {
            addCriterion("register_cummunity is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityEqualTo(String value) {
            addCriterion("register_cummunity =", value, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityNotEqualTo(String value) {
            addCriterion("register_cummunity <>", value, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityGreaterThan(String value) {
            addCriterion("register_cummunity >", value, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityGreaterThanOrEqualTo(String value) {
            addCriterion("register_cummunity >=", value, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityLessThan(String value) {
            addCriterion("register_cummunity <", value, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityLessThanOrEqualTo(String value) {
            addCriterion("register_cummunity <=", value, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityLike(String value) {
            addCriterion("register_cummunity like", value, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityNotLike(String value) {
            addCriterion("register_cummunity not like", value, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityIn(List<String> values) {
            addCriterion("register_cummunity in", values, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityNotIn(List<String> values) {
            addCriterion("register_cummunity not in", values, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityBetween(String value1, String value2) {
            addCriterion("register_cummunity between", value1, value2, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterCummunityNotBetween(String value1, String value2) {
            addCriterion("register_cummunity not between", value1, value2, "registerCummunity");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameIsNull() {
            addCriterion("register_park_name is null");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameIsNotNull() {
            addCriterion("register_park_name is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameEqualTo(String value) {
            addCriterion("register_park_name =", value, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameNotEqualTo(String value) {
            addCriterion("register_park_name <>", value, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameGreaterThan(String value) {
            addCriterion("register_park_name >", value, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameGreaterThanOrEqualTo(String value) {
            addCriterion("register_park_name >=", value, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameLessThan(String value) {
            addCriterion("register_park_name <", value, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameLessThanOrEqualTo(String value) {
            addCriterion("register_park_name <=", value, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameLike(String value) {
            addCriterion("register_park_name like", value, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameNotLike(String value) {
            addCriterion("register_park_name not like", value, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameIn(List<String> values) {
            addCriterion("register_park_name in", values, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameNotIn(List<String> values) {
            addCriterion("register_park_name not in", values, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameBetween(String value1, String value2) {
            addCriterion("register_park_name between", value1, value2, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkNameNotBetween(String value1, String value2) {
            addCriterion("register_park_name not between", value1, value2, "registerParkName");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeIsNull() {
            addCriterion("register_park_code is null");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeIsNotNull() {
            addCriterion("register_park_code is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeEqualTo(String value) {
            addCriterion("register_park_code =", value, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeNotEqualTo(String value) {
            addCriterion("register_park_code <>", value, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeGreaterThan(String value) {
            addCriterion("register_park_code >", value, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeGreaterThanOrEqualTo(String value) {
            addCriterion("register_park_code >=", value, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeLessThan(String value) {
            addCriterion("register_park_code <", value, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeLessThanOrEqualTo(String value) {
            addCriterion("register_park_code <=", value, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeLike(String value) {
            addCriterion("register_park_code like", value, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeNotLike(String value) {
            addCriterion("register_park_code not like", value, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeIn(List<String> values) {
            addCriterion("register_park_code in", values, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeNotIn(List<String> values) {
            addCriterion("register_park_code not in", values, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeBetween(String value1, String value2) {
            addCriterion("register_park_code between", value1, value2, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterParkCodeNotBetween(String value1, String value2) {
            addCriterion("register_park_code not between", value1, value2, "registerParkCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeIsNull() {
            addCriterion("register_unit_zoning_code is null");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeIsNotNull() {
            addCriterion("register_unit_zoning_code is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeEqualTo(String value) {
            addCriterion("register_unit_zoning_code =", value, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeNotEqualTo(String value) {
            addCriterion("register_unit_zoning_code <>", value, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeGreaterThan(String value) {
            addCriterion("register_unit_zoning_code >", value, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeGreaterThanOrEqualTo(String value) {
            addCriterion("register_unit_zoning_code >=", value, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeLessThan(String value) {
            addCriterion("register_unit_zoning_code <", value, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeLessThanOrEqualTo(String value) {
            addCriterion("register_unit_zoning_code <=", value, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeLike(String value) {
            addCriterion("register_unit_zoning_code like", value, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeNotLike(String value) {
            addCriterion("register_unit_zoning_code not like", value, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeIn(List<String> values) {
            addCriterion("register_unit_zoning_code in", values, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeNotIn(List<String> values) {
            addCriterion("register_unit_zoning_code not in", values, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeBetween(String value1, String value2) {
            addCriterion("register_unit_zoning_code between", value1, value2, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitZoningCodeNotBetween(String value1, String value2) {
            addCriterion("register_unit_zoning_code not between", value1, value2, "registerUnitZoningCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeIsNull() {
            addCriterion("register_unit_country_code is null");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeIsNotNull() {
            addCriterion("register_unit_country_code is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeEqualTo(String value) {
            addCriterion("register_unit_country_code =", value, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeNotEqualTo(String value) {
            addCriterion("register_unit_country_code <>", value, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeGreaterThan(String value) {
            addCriterion("register_unit_country_code >", value, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeGreaterThanOrEqualTo(String value) {
            addCriterion("register_unit_country_code >=", value, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeLessThan(String value) {
            addCriterion("register_unit_country_code <", value, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeLessThanOrEqualTo(String value) {
            addCriterion("register_unit_country_code <=", value, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeLike(String value) {
            addCriterion("register_unit_country_code like", value, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeNotLike(String value) {
            addCriterion("register_unit_country_code not like", value, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeIn(List<String> values) {
            addCriterion("register_unit_country_code in", values, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeNotIn(List<String> values) {
            addCriterion("register_unit_country_code not in", values, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeBetween(String value1, String value2) {
            addCriterion("register_unit_country_code between", value1, value2, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andRegisterUnitCountryCodeNotBetween(String value1, String value2) {
            addCriterion("register_unit_country_code not between", value1, value2, "registerUnitCountryCode");
            return (Criteria) this;
        }

        public Criteria andBusinessStateIsNull() {
            addCriterion("business_state is null");
            return (Criteria) this;
        }

        public Criteria andBusinessStateIsNotNull() {
            addCriterion("business_state is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessStateEqualTo(Integer value) {
            addCriterion("business_state =", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotEqualTo(Integer value) {
            addCriterion("business_state <>", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateGreaterThan(Integer value) {
            addCriterion("business_state >", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("business_state >=", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateLessThan(Integer value) {
            addCriterion("business_state <", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateLessThanOrEqualTo(Integer value) {
            addCriterion("business_state <=", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateIn(List<Integer> values) {
            addCriterion("business_state in", values, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotIn(List<Integer> values) {
            addCriterion("business_state not in", values, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateBetween(Integer value1, Integer value2) {
            addCriterion("business_state between", value1, value2, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotBetween(Integer value1, Integer value2) {
            addCriterion("business_state not between", value1, value2, "businessState");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1IsNull() {
            addCriterion("major_business1 is null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1IsNotNull() {
            addCriterion("major_business1 is not null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1EqualTo(String value) {
            addCriterion("major_business1 =", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1NotEqualTo(String value) {
            addCriterion("major_business1 <>", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1GreaterThan(String value) {
            addCriterion("major_business1 >", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1GreaterThanOrEqualTo(String value) {
            addCriterion("major_business1 >=", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1LessThan(String value) {
            addCriterion("major_business1 <", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1LessThanOrEqualTo(String value) {
            addCriterion("major_business1 <=", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1Like(String value) {
            addCriterion("major_business1 like", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1NotLike(String value) {
            addCriterion("major_business1 not like", value, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1In(List<String> values) {
            addCriterion("major_business1 in", values, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1NotIn(List<String> values) {
            addCriterion("major_business1 not in", values, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1Between(String value1, String value2) {
            addCriterion("major_business1 between", value1, value2, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness1NotBetween(String value1, String value2) {
            addCriterion("major_business1 not between", value1, value2, "majorBusiness1");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2IsNull() {
            addCriterion("major_business2 is null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2IsNotNull() {
            addCriterion("major_business2 is not null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2EqualTo(String value) {
            addCriterion("major_business2 =", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2NotEqualTo(String value) {
            addCriterion("major_business2 <>", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2GreaterThan(String value) {
            addCriterion("major_business2 >", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2GreaterThanOrEqualTo(String value) {
            addCriterion("major_business2 >=", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2LessThan(String value) {
            addCriterion("major_business2 <", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2LessThanOrEqualTo(String value) {
            addCriterion("major_business2 <=", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2Like(String value) {
            addCriterion("major_business2 like", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2NotLike(String value) {
            addCriterion("major_business2 not like", value, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2In(List<String> values) {
            addCriterion("major_business2 in", values, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2NotIn(List<String> values) {
            addCriterion("major_business2 not in", values, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2Between(String value1, String value2) {
            addCriterion("major_business2 between", value1, value2, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness2NotBetween(String value1, String value2) {
            addCriterion("major_business2 not between", value1, value2, "majorBusiness2");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3IsNull() {
            addCriterion("major_business3 is null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3IsNotNull() {
            addCriterion("major_business3 is not null");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3EqualTo(String value) {
            addCriterion("major_business3 =", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3NotEqualTo(String value) {
            addCriterion("major_business3 <>", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3GreaterThan(String value) {
            addCriterion("major_business3 >", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3GreaterThanOrEqualTo(String value) {
            addCriterion("major_business3 >=", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3LessThan(String value) {
            addCriterion("major_business3 <", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3LessThanOrEqualTo(String value) {
            addCriterion("major_business3 <=", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3Like(String value) {
            addCriterion("major_business3 like", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3NotLike(String value) {
            addCriterion("major_business3 not like", value, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3In(List<String> values) {
            addCriterion("major_business3 in", values, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3NotIn(List<String> values) {
            addCriterion("major_business3 not in", values, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3Between(String value1, String value2) {
            addCriterion("major_business3 between", value1, value2, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andMajorBusiness3NotBetween(String value1, String value2) {
            addCriterion("major_business3 not between", value1, value2, "majorBusiness3");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeIsNull() {
            addCriterion("industry_code is null");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeIsNotNull() {
            addCriterion("industry_code is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeEqualTo(String value) {
            addCriterion("industry_code =", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotEqualTo(String value) {
            addCriterion("industry_code <>", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeGreaterThan(String value) {
            addCriterion("industry_code >", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeGreaterThanOrEqualTo(String value) {
            addCriterion("industry_code >=", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeLessThan(String value) {
            addCriterion("industry_code <", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeLessThanOrEqualTo(String value) {
            addCriterion("industry_code <=", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeLike(String value) {
            addCriterion("industry_code like", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotLike(String value) {
            addCriterion("industry_code not like", value, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeIn(List<String> values) {
            addCriterion("industry_code in", values, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotIn(List<String> values) {
            addCriterion("industry_code not in", values, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeBetween(String value1, String value2) {
            addCriterion("industry_code between", value1, value2, "industryCode");
            return (Criteria) this;
        }

        public Criteria andIndustryCodeNotBetween(String value1, String value2) {
            addCriterion("industry_code not between", value1, value2, "industryCode");
            return (Criteria) this;
        }

        public Criteria andReportCategoryIsNull() {
            addCriterion("report_category is null");
            return (Criteria) this;
        }

        public Criteria andReportCategoryIsNotNull() {
            addCriterion("report_category is not null");
            return (Criteria) this;
        }

        public Criteria andReportCategoryEqualTo(String value) {
            addCriterion("report_category =", value, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryNotEqualTo(String value) {
            addCriterion("report_category <>", value, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryGreaterThan(String value) {
            addCriterion("report_category >", value, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryGreaterThanOrEqualTo(String value) {
            addCriterion("report_category >=", value, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryLessThan(String value) {
            addCriterion("report_category <", value, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryLessThanOrEqualTo(String value) {
            addCriterion("report_category <=", value, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryLike(String value) {
            addCriterion("report_category like", value, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryNotLike(String value) {
            addCriterion("report_category not like", value, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryIn(List<String> values) {
            addCriterion("report_category in", values, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryNotIn(List<String> values) {
            addCriterion("report_category not in", values, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryBetween(String value1, String value2) {
            addCriterion("report_category between", value1, value2, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andReportCategoryNotBetween(String value1, String value2) {
            addCriterion("report_category not between", value1, value2, "reportCategory");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeIsNull() {
            addCriterion("mechanism_type is null");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeIsNotNull() {
            addCriterion("mechanism_type is not null");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeEqualTo(Integer value) {
            addCriterion("mechanism_type =", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeNotEqualTo(Integer value) {
            addCriterion("mechanism_type <>", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeGreaterThan(Integer value) {
            addCriterion("mechanism_type >", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("mechanism_type >=", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeLessThan(Integer value) {
            addCriterion("mechanism_type <", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeLessThanOrEqualTo(Integer value) {
            addCriterion("mechanism_type <=", value, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeIn(List<Integer> values) {
            addCriterion("mechanism_type in", values, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeNotIn(List<Integer> values) {
            addCriterion("mechanism_type not in", values, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeBetween(Integer value1, Integer value2) {
            addCriterion("mechanism_type between", value1, value2, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andMechanismTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("mechanism_type not between", value1, value2, "mechanismType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeIsNull() {
            addCriterion("registration_type is null");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeIsNotNull() {
            addCriterion("registration_type is not null");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeEqualTo(String value) {
            addCriterion("registration_type =", value, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeNotEqualTo(String value) {
            addCriterion("registration_type <>", value, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeGreaterThan(String value) {
            addCriterion("registration_type >", value, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeGreaterThanOrEqualTo(String value) {
            addCriterion("registration_type >=", value, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeLessThan(String value) {
            addCriterion("registration_type <", value, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeLessThanOrEqualTo(String value) {
            addCriterion("registration_type <=", value, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeLike(String value) {
            addCriterion("registration_type like", value, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeNotLike(String value) {
            addCriterion("registration_type not like", value, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeIn(List<String> values) {
            addCriterion("registration_type in", values, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeNotIn(List<String> values) {
            addCriterion("registration_type not in", values, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeBetween(String value1, String value2) {
            addCriterion("registration_type between", value1, value2, "registrationType");
            return (Criteria) this;
        }

        public Criteria andRegistrationTypeNotBetween(String value1, String value2) {
            addCriterion("registration_type not between", value1, value2, "registrationType");
            return (Criteria) this;
        }

        public Criteria andIsBIsNull() {
            addCriterion("is_b is null");
            return (Criteria) this;
        }

        public Criteria andIsBIsNotNull() {
            addCriterion("is_b is not null");
            return (Criteria) this;
        }

        public Criteria andIsBEqualTo(Integer value) {
            addCriterion("is_b =", value, "isB");
            return (Criteria) this;
        }

        public Criteria andIsBNotEqualTo(Integer value) {
            addCriterion("is_b <>", value, "isB");
            return (Criteria) this;
        }

        public Criteria andIsBGreaterThan(Integer value) {
            addCriterion("is_b >", value, "isB");
            return (Criteria) this;
        }

        public Criteria andIsBGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_b >=", value, "isB");
            return (Criteria) this;
        }

        public Criteria andIsBLessThan(Integer value) {
            addCriterion("is_b <", value, "isB");
            return (Criteria) this;
        }

        public Criteria andIsBLessThanOrEqualTo(Integer value) {
            addCriterion("is_b <=", value, "isB");
            return (Criteria) this;
        }

        public Criteria andIsBIn(List<Integer> values) {
            addCriterion("is_b in", values, "isB");
            return (Criteria) this;
        }

        public Criteria andIsBNotIn(List<Integer> values) {
            addCriterion("is_b not in", values, "isB");
            return (Criteria) this;
        }

        public Criteria andIsBBetween(Integer value1, Integer value2) {
            addCriterion("is_b between", value1, value2, "isB");
            return (Criteria) this;
        }

        public Criteria andIsBNotBetween(Integer value1, Integer value2) {
            addCriterion("is_b not between", value1, value2, "isB");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationIsNull() {
            addCriterion("form_of_operation is null");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationIsNotNull() {
            addCriterion("form_of_operation is not null");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationEqualTo(Integer value) {
            addCriterion("form_of_operation =", value, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationNotEqualTo(Integer value) {
            addCriterion("form_of_operation <>", value, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationGreaterThan(Integer value) {
            addCriterion("form_of_operation >", value, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationGreaterThanOrEqualTo(Integer value) {
            addCriterion("form_of_operation >=", value, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationLessThan(Integer value) {
            addCriterion("form_of_operation <", value, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationLessThanOrEqualTo(Integer value) {
            addCriterion("form_of_operation <=", value, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationIn(List<Integer> values) {
            addCriterion("form_of_operation in", values, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationNotIn(List<Integer> values) {
            addCriterion("form_of_operation not in", values, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationBetween(Integer value1, Integer value2) {
            addCriterion("form_of_operation between", value1, value2, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andFormOfOperationNotBetween(Integer value1, Integer value2) {
            addCriterion("form_of_operation not between", value1, value2, "formOfOperation");
            return (Criteria) this;
        }

        public Criteria andChainBrandIsNull() {
            addCriterion("chain_brand is null");
            return (Criteria) this;
        }

        public Criteria andChainBrandIsNotNull() {
            addCriterion("chain_brand is not null");
            return (Criteria) this;
        }

        public Criteria andChainBrandEqualTo(String value) {
            addCriterion("chain_brand =", value, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandNotEqualTo(String value) {
            addCriterion("chain_brand <>", value, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandGreaterThan(String value) {
            addCriterion("chain_brand >", value, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandGreaterThanOrEqualTo(String value) {
            addCriterion("chain_brand >=", value, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandLessThan(String value) {
            addCriterion("chain_brand <", value, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandLessThanOrEqualTo(String value) {
            addCriterion("chain_brand <=", value, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandLike(String value) {
            addCriterion("chain_brand like", value, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandNotLike(String value) {
            addCriterion("chain_brand not like", value, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandIn(List<String> values) {
            addCriterion("chain_brand in", values, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandNotIn(List<String> values) {
            addCriterion("chain_brand not in", values, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandBetween(String value1, String value2) {
            addCriterion("chain_brand between", value1, value2, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andChainBrandNotBetween(String value1, String value2) {
            addCriterion("chain_brand not between", value1, value2, "chainBrand");
            return (Criteria) this;
        }

        public Criteria andRetailFormatIsNull() {
            addCriterion("retail_format is null");
            return (Criteria) this;
        }

        public Criteria andRetailFormatIsNotNull() {
            addCriterion("retail_format is not null");
            return (Criteria) this;
        }

        public Criteria andRetailFormatEqualTo(String value) {
            addCriterion("retail_format =", value, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatNotEqualTo(String value) {
            addCriterion("retail_format <>", value, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatGreaterThan(String value) {
            addCriterion("retail_format >", value, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatGreaterThanOrEqualTo(String value) {
            addCriterion("retail_format >=", value, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatLessThan(String value) {
            addCriterion("retail_format <", value, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatLessThanOrEqualTo(String value) {
            addCriterion("retail_format <=", value, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatLike(String value) {
            addCriterion("retail_format like", value, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatNotLike(String value) {
            addCriterion("retail_format not like", value, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatIn(List<String> values) {
            addCriterion("retail_format in", values, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatNotIn(List<String> values) {
            addCriterion("retail_format not in", values, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatBetween(String value1, String value2) {
            addCriterion("retail_format between", value1, value2, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andRetailFormatNotBetween(String value1, String value2) {
            addCriterion("retail_format not between", value1, value2, "retailFormat");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaIsNull() {
            addCriterion("war_operating_area is null");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaIsNotNull() {
            addCriterion("war_operating_area is not null");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaEqualTo(Float value) {
            addCriterion("war_operating_area =", value, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaNotEqualTo(Float value) {
            addCriterion("war_operating_area <>", value, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaGreaterThan(Float value) {
            addCriterion("war_operating_area >", value, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaGreaterThanOrEqualTo(Float value) {
            addCriterion("war_operating_area >=", value, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaLessThan(Float value) {
            addCriterion("war_operating_area <", value, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaLessThanOrEqualTo(Float value) {
            addCriterion("war_operating_area <=", value, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaIn(List<Float> values) {
            addCriterion("war_operating_area in", values, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaNotIn(List<Float> values) {
            addCriterion("war_operating_area not in", values, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaBetween(Float value1, Float value2) {
            addCriterion("war_operating_area between", value1, value2, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andWarOperatingAreaNotBetween(Float value1, Float value2) {
            addCriterion("war_operating_area not between", value1, value2, "warOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarIsNull() {
            addCriterion("accommodation_star is null");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarIsNotNull() {
            addCriterion("accommodation_star is not null");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarEqualTo(Integer value) {
            addCriterion("accommodation_star =", value, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarNotEqualTo(Integer value) {
            addCriterion("accommodation_star <>", value, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarGreaterThan(Integer value) {
            addCriterion("accommodation_star >", value, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarGreaterThanOrEqualTo(Integer value) {
            addCriterion("accommodation_star >=", value, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarLessThan(Integer value) {
            addCriterion("accommodation_star <", value, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarLessThanOrEqualTo(Integer value) {
            addCriterion("accommodation_star <=", value, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarIn(List<Integer> values) {
            addCriterion("accommodation_star in", values, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarNotIn(List<Integer> values) {
            addCriterion("accommodation_star not in", values, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarBetween(Integer value1, Integer value2) {
            addCriterion("accommodation_star between", value1, value2, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAccommodationStarNotBetween(Integer value1, Integer value2) {
            addCriterion("accommodation_star not between", value1, value2, "accommodationStar");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaIsNull() {
            addCriterion("aac_operating_area is null");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaIsNotNull() {
            addCriterion("aac_operating_area is not null");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaEqualTo(Float value) {
            addCriterion("aac_operating_area =", value, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaNotEqualTo(Float value) {
            addCriterion("aac_operating_area <>", value, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaGreaterThan(Float value) {
            addCriterion("aac_operating_area >", value, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaGreaterThanOrEqualTo(Float value) {
            addCriterion("aac_operating_area >=", value, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaLessThan(Float value) {
            addCriterion("aac_operating_area <", value, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaLessThanOrEqualTo(Float value) {
            addCriterion("aac_operating_area <=", value, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaIn(List<Float> values) {
            addCriterion("aac_operating_area in", values, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaNotIn(List<Float> values) {
            addCriterion("aac_operating_area not in", values, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaBetween(Float value1, Float value2) {
            addCriterion("aac_operating_area between", value1, value2, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andAacOperatingAreaNotBetween(Float value1, Float value2) {
            addCriterion("aac_operating_area not between", value1, value2, "aacOperatingArea");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationIsNull() {
            addCriterion("gat_Investment_situation is null");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationIsNotNull() {
            addCriterion("gat_Investment_situation is not null");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationEqualTo(String value) {
            addCriterion("gat_Investment_situation =", value, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationNotEqualTo(String value) {
            addCriterion("gat_Investment_situation <>", value, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationGreaterThan(String value) {
            addCriterion("gat_Investment_situation >", value, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationGreaterThanOrEqualTo(String value) {
            addCriterion("gat_Investment_situation >=", value, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationLessThan(String value) {
            addCriterion("gat_Investment_situation <", value, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationLessThanOrEqualTo(String value) {
            addCriterion("gat_Investment_situation <=", value, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationLike(String value) {
            addCriterion("gat_Investment_situation like", value, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationNotLike(String value) {
            addCriterion("gat_Investment_situation not like", value, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationIn(List<String> values) {
            addCriterion("gat_Investment_situation in", values, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationNotIn(List<String> values) {
            addCriterion("gat_Investment_situation not in", values, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationBetween(String value1, String value2) {
            addCriterion("gat_Investment_situation between", value1, value2, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andGatInvestmentSituationNotBetween(String value1, String value2) {
            addCriterion("gat_Investment_situation not between", value1, value2, "gatInvestmentSituation");
            return (Criteria) this;
        }

        public Criteria andAffiliationIsNull() {
            addCriterion("affiliation is null");
            return (Criteria) this;
        }

        public Criteria andAffiliationIsNotNull() {
            addCriterion("affiliation is not null");
            return (Criteria) this;
        }

        public Criteria andAffiliationEqualTo(Integer value) {
            addCriterion("affiliation =", value, "affiliation");
            return (Criteria) this;
        }

        public Criteria andAffiliationNotEqualTo(Integer value) {
            addCriterion("affiliation <>", value, "affiliation");
            return (Criteria) this;
        }

        public Criteria andAffiliationGreaterThan(Integer value) {
            addCriterion("affiliation >", value, "affiliation");
            return (Criteria) this;
        }

        public Criteria andAffiliationGreaterThanOrEqualTo(Integer value) {
            addCriterion("affiliation >=", value, "affiliation");
            return (Criteria) this;
        }

        public Criteria andAffiliationLessThan(Integer value) {
            addCriterion("affiliation <", value, "affiliation");
            return (Criteria) this;
        }

        public Criteria andAffiliationLessThanOrEqualTo(Integer value) {
            addCriterion("affiliation <=", value, "affiliation");
            return (Criteria) this;
        }

        public Criteria andAffiliationIn(List<Integer> values) {
            addCriterion("affiliation in", values, "affiliation");
            return (Criteria) this;
        }

        public Criteria andAffiliationNotIn(List<Integer> values) {
            addCriterion("affiliation not in", values, "affiliation");
            return (Criteria) this;
        }

        public Criteria andAffiliationBetween(Integer value1, Integer value2) {
            addCriterion("affiliation between", value1, value2, "affiliation");
            return (Criteria) this;
        }

        public Criteria andAffiliationNotBetween(Integer value1, Integer value2) {
            addCriterion("affiliation not between", value1, value2, "affiliation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationIsNull() {
            addCriterion("holding_situation is null");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationIsNotNull() {
            addCriterion("holding_situation is not null");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationEqualTo(Integer value) {
            addCriterion("holding_situation =", value, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationNotEqualTo(Integer value) {
            addCriterion("holding_situation <>", value, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationGreaterThan(Integer value) {
            addCriterion("holding_situation >", value, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationGreaterThanOrEqualTo(Integer value) {
            addCriterion("holding_situation >=", value, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationLessThan(Integer value) {
            addCriterion("holding_situation <", value, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationLessThanOrEqualTo(Integer value) {
            addCriterion("holding_situation <=", value, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationIn(List<Integer> values) {
            addCriterion("holding_situation in", values, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationNotIn(List<Integer> values) {
            addCriterion("holding_situation not in", values, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationBetween(Integer value1, Integer value2) {
            addCriterion("holding_situation between", value1, value2, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andHoldingSituationNotBetween(Integer value1, Integer value2) {
            addCriterion("holding_situation not between", value1, value2, "holdingSituation");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryIsNull() {
            addCriterion("accounting_standard_category is null");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryIsNotNull() {
            addCriterion("accounting_standard_category is not null");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryEqualTo(Integer value) {
            addCriterion("accounting_standard_category =", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryNotEqualTo(Integer value) {
            addCriterion("accounting_standard_category <>", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryGreaterThan(Integer value) {
            addCriterion("accounting_standard_category >", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryGreaterThanOrEqualTo(Integer value) {
            addCriterion("accounting_standard_category >=", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryLessThan(Integer value) {
            addCriterion("accounting_standard_category <", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryLessThanOrEqualTo(Integer value) {
            addCriterion("accounting_standard_category <=", value, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryIn(List<Integer> values) {
            addCriterion("accounting_standard_category in", values, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryNotIn(List<Integer> values) {
            addCriterion("accounting_standard_category not in", values, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryBetween(Integer value1, Integer value2) {
            addCriterion("accounting_standard_category between", value1, value2, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardCategoryNotBetween(Integer value1, Integer value2) {
            addCriterion("accounting_standard_category not between", value1, value2, "accountingStandardCategory");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardIsNull() {
            addCriterion("accounting_standard is null");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardIsNotNull() {
            addCriterion("accounting_standard is not null");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardEqualTo(Integer value) {
            addCriterion("accounting_standard =", value, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardNotEqualTo(Integer value) {
            addCriterion("accounting_standard <>", value, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardGreaterThan(Integer value) {
            addCriterion("accounting_standard >", value, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardGreaterThanOrEqualTo(Integer value) {
            addCriterion("accounting_standard >=", value, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardLessThan(Integer value) {
            addCriterion("accounting_standard <", value, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardLessThanOrEqualTo(Integer value) {
            addCriterion("accounting_standard <=", value, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardIn(List<Integer> values) {
            addCriterion("accounting_standard in", values, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardNotIn(List<Integer> values) {
            addCriterion("accounting_standard not in", values, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardBetween(Integer value1, Integer value2) {
            addCriterion("accounting_standard between", value1, value2, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andAccountingStandardNotBetween(Integer value1, Integer value2) {
            addCriterion("accounting_standard not between", value1, value2, "accountingStandard");
            return (Criteria) this;
        }

        public Criteria andUnitScaleIsNull() {
            addCriterion("unit_scale is null");
            return (Criteria) this;
        }

        public Criteria andUnitScaleIsNotNull() {
            addCriterion("unit_scale is not null");
            return (Criteria) this;
        }

        public Criteria andUnitScaleEqualTo(Integer value) {
            addCriterion("unit_scale =", value, "unitScale");
            return (Criteria) this;
        }

        public Criteria andUnitScaleNotEqualTo(Integer value) {
            addCriterion("unit_scale <>", value, "unitScale");
            return (Criteria) this;
        }

        public Criteria andUnitScaleGreaterThan(Integer value) {
            addCriterion("unit_scale >", value, "unitScale");
            return (Criteria) this;
        }

        public Criteria andUnitScaleGreaterThanOrEqualTo(Integer value) {
            addCriterion("unit_scale >=", value, "unitScale");
            return (Criteria) this;
        }

        public Criteria andUnitScaleLessThan(Integer value) {
            addCriterion("unit_scale <", value, "unitScale");
            return (Criteria) this;
        }

        public Criteria andUnitScaleLessThanOrEqualTo(Integer value) {
            addCriterion("unit_scale <=", value, "unitScale");
            return (Criteria) this;
        }

        public Criteria andUnitScaleIn(List<Integer> values) {
            addCriterion("unit_scale in", values, "unitScale");
            return (Criteria) this;
        }

        public Criteria andUnitScaleNotIn(List<Integer> values) {
            addCriterion("unit_scale not in", values, "unitScale");
            return (Criteria) this;
        }

        public Criteria andUnitScaleBetween(Integer value1, Integer value2) {
            addCriterion("unit_scale between", value1, value2, "unitScale");
            return (Criteria) this;
        }

        public Criteria andUnitScaleNotBetween(Integer value1, Integer value2) {
            addCriterion("unit_scale not between", value1, value2, "unitScale");
            return (Criteria) this;
        }

        public Criteria andEmployeeIsNull() {
            addCriterion("employee is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeIsNotNull() {
            addCriterion("employee is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeEqualTo(Integer value) {
            addCriterion("employee =", value, "employee");
            return (Criteria) this;
        }

        public Criteria andEmployeeNotEqualTo(Integer value) {
            addCriterion("employee <>", value, "employee");
            return (Criteria) this;
        }

        public Criteria andEmployeeGreaterThan(Integer value) {
            addCriterion("employee >", value, "employee");
            return (Criteria) this;
        }

        public Criteria andEmployeeGreaterThanOrEqualTo(Integer value) {
            addCriterion("employee >=", value, "employee");
            return (Criteria) this;
        }

        public Criteria andEmployeeLessThan(Integer value) {
            addCriterion("employee <", value, "employee");
            return (Criteria) this;
        }

        public Criteria andEmployeeLessThanOrEqualTo(Integer value) {
            addCriterion("employee <=", value, "employee");
            return (Criteria) this;
        }

        public Criteria andEmployeeIn(List<Integer> values) {
            addCriterion("employee in", values, "employee");
            return (Criteria) this;
        }

        public Criteria andEmployeeNotIn(List<Integer> values) {
            addCriterion("employee not in", values, "employee");
            return (Criteria) this;
        }

        public Criteria andEmployeeBetween(Integer value1, Integer value2) {
            addCriterion("employee between", value1, value2, "employee");
            return (Criteria) this;
        }

        public Criteria andEmployeeNotBetween(Integer value1, Integer value2) {
            addCriterion("employee not between", value1, value2, "employee");
            return (Criteria) this;
        }

        public Criteria andFemaleIsNull() {
            addCriterion("female is null");
            return (Criteria) this;
        }

        public Criteria andFemaleIsNotNull() {
            addCriterion("female is not null");
            return (Criteria) this;
        }

        public Criteria andFemaleEqualTo(Integer value) {
            addCriterion("female =", value, "female");
            return (Criteria) this;
        }

        public Criteria andFemaleNotEqualTo(Integer value) {
            addCriterion("female <>", value, "female");
            return (Criteria) this;
        }

        public Criteria andFemaleGreaterThan(Integer value) {
            addCriterion("female >", value, "female");
            return (Criteria) this;
        }

        public Criteria andFemaleGreaterThanOrEqualTo(Integer value) {
            addCriterion("female >=", value, "female");
            return (Criteria) this;
        }

        public Criteria andFemaleLessThan(Integer value) {
            addCriterion("female <", value, "female");
            return (Criteria) this;
        }

        public Criteria andFemaleLessThanOrEqualTo(Integer value) {
            addCriterion("female <=", value, "female");
            return (Criteria) this;
        }

        public Criteria andFemaleIn(List<Integer> values) {
            addCriterion("female in", values, "female");
            return (Criteria) this;
        }

        public Criteria andFemaleNotIn(List<Integer> values) {
            addCriterion("female not in", values, "female");
            return (Criteria) this;
        }

        public Criteria andFemaleBetween(Integer value1, Integer value2) {
            addCriterion("female between", value1, value2, "female");
            return (Criteria) this;
        }

        public Criteria andFemaleNotBetween(Integer value1, Integer value2) {
            addCriterion("female not between", value1, value2, "female");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeIsNull() {
            addCriterion("business_income is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeIsNotNull() {
            addCriterion("business_income is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeEqualTo(BigDecimal value) {
            addCriterion("business_income =", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeNotEqualTo(BigDecimal value) {
            addCriterion("business_income <>", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeGreaterThan(BigDecimal value) {
            addCriterion("business_income >", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("business_income >=", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLessThan(BigDecimal value) {
            addCriterion("business_income <", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("business_income <=", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeIn(List<BigDecimal> values) {
            addCriterion("business_income in", values, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeNotIn(List<BigDecimal> values) {
            addCriterion("business_income not in", values, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("business_income between", value1, value2, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("business_income not between", value1, value2, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeIsNull() {
            addCriterion("main_business_income is null");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeIsNotNull() {
            addCriterion("main_business_income is not null");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeEqualTo(BigDecimal value) {
            addCriterion("main_business_income =", value, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeNotEqualTo(BigDecimal value) {
            addCriterion("main_business_income <>", value, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeGreaterThan(BigDecimal value) {
            addCriterion("main_business_income >", value, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("main_business_income >=", value, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeLessThan(BigDecimal value) {
            addCriterion("main_business_income <", value, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("main_business_income <=", value, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeIn(List<BigDecimal> values) {
            addCriterion("main_business_income in", values, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeNotIn(List<BigDecimal> values) {
            addCriterion("main_business_income not in", values, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("main_business_income between", value1, value2, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andMainBusinessIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("main_business_income not between", value1, value2, "mainBusinessIncome");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNull() {
            addCriterion("total_assets is null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNotNull() {
            addCriterion("total_assets is not null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsEqualTo(BigDecimal value) {
            addCriterion("total_assets =", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotEqualTo(BigDecimal value) {
            addCriterion("total_assets <>", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThan(BigDecimal value) {
            addCriterion("total_assets >", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_assets >=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThan(BigDecimal value) {
            addCriterion("total_assets <", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_assets <=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIn(List<BigDecimal> values) {
            addCriterion("total_assets in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotIn(List<BigDecimal> values) {
            addCriterion("total_assets not in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_assets between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_assets not between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalIsNull() {
            addCriterion("tax_and_additional is null");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalIsNotNull() {
            addCriterion("tax_and_additional is not null");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalEqualTo(BigDecimal value) {
            addCriterion("tax_and_additional =", value, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalNotEqualTo(BigDecimal value) {
            addCriterion("tax_and_additional <>", value, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalGreaterThan(BigDecimal value) {
            addCriterion("tax_and_additional >", value, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("tax_and_additional >=", value, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalLessThan(BigDecimal value) {
            addCriterion("tax_and_additional <", value, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("tax_and_additional <=", value, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalIn(List<BigDecimal> values) {
            addCriterion("tax_and_additional in", values, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalNotIn(List<BigDecimal> values) {
            addCriterion("tax_and_additional not in", values, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("tax_and_additional between", value1, value2, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andTaxAndAdditionalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("tax_and_additional not between", value1, value2, "taxAndAdditional");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureIsNull() {
            addCriterion("non_unit_expenditure is null");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureIsNotNull() {
            addCriterion("non_unit_expenditure is not null");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureEqualTo(BigDecimal value) {
            addCriterion("non_unit_expenditure =", value, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureNotEqualTo(BigDecimal value) {
            addCriterion("non_unit_expenditure <>", value, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureGreaterThan(BigDecimal value) {
            addCriterion("non_unit_expenditure >", value, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("non_unit_expenditure >=", value, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureLessThan(BigDecimal value) {
            addCriterion("non_unit_expenditure <", value, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureLessThanOrEqualTo(BigDecimal value) {
            addCriterion("non_unit_expenditure <=", value, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureIn(List<BigDecimal> values) {
            addCriterion("non_unit_expenditure in", values, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureNotIn(List<BigDecimal> values) {
            addCriterion("non_unit_expenditure not in", values, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("non_unit_expenditure between", value1, value2, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitExpenditureNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("non_unit_expenditure not between", value1, value2, "nonUnitExpenditure");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsIsNull() {
            addCriterion("non_unit_total_assets is null");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsIsNotNull() {
            addCriterion("non_unit_total_assets is not null");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsEqualTo(BigDecimal value) {
            addCriterion("non_unit_total_assets =", value, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsNotEqualTo(BigDecimal value) {
            addCriterion("non_unit_total_assets <>", value, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsGreaterThan(BigDecimal value) {
            addCriterion("non_unit_total_assets >", value, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("non_unit_total_assets >=", value, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsLessThan(BigDecimal value) {
            addCriterion("non_unit_total_assets <", value, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("non_unit_total_assets <=", value, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsIn(List<BigDecimal> values) {
            addCriterion("non_unit_total_assets in", values, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsNotIn(List<BigDecimal> values) {
            addCriterion("non_unit_total_assets not in", values, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("non_unit_total_assets between", value1, value2, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andNonUnitTotalAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("non_unit_total_assets not between", value1, value2, "nonUnitTotalAssets");
            return (Criteria) this;
        }

        public Criteria andCateringFormatIsNull() {
            addCriterion("catering_format is null");
            return (Criteria) this;
        }

        public Criteria andCateringFormatIsNotNull() {
            addCriterion("catering_format is not null");
            return (Criteria) this;
        }

        public Criteria andCateringFormatEqualTo(String value) {
            addCriterion("catering_format =", value, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatNotEqualTo(String value) {
            addCriterion("catering_format <>", value, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatGreaterThan(String value) {
            addCriterion("catering_format >", value, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatGreaterThanOrEqualTo(String value) {
            addCriterion("catering_format >=", value, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatLessThan(String value) {
            addCriterion("catering_format <", value, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatLessThanOrEqualTo(String value) {
            addCriterion("catering_format <=", value, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatLike(String value) {
            addCriterion("catering_format like", value, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatNotLike(String value) {
            addCriterion("catering_format not like", value, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatIn(List<String> values) {
            addCriterion("catering_format in", values, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatNotIn(List<String> values) {
            addCriterion("catering_format not in", values, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatBetween(String value1, String value2) {
            addCriterion("catering_format between", value1, value2, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andCateringFormatNotBetween(String value1, String value2) {
            addCriterion("catering_format not between", value1, value2, "cateringFormat");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeIsNull() {
            addCriterion("construction_qualification_code is null");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeIsNotNull() {
            addCriterion("construction_qualification_code is not null");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeEqualTo(String value) {
            addCriterion("construction_qualification_code =", value, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeNotEqualTo(String value) {
            addCriterion("construction_qualification_code <>", value, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeGreaterThan(String value) {
            addCriterion("construction_qualification_code >", value, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("construction_qualification_code >=", value, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeLessThan(String value) {
            addCriterion("construction_qualification_code <", value, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeLessThanOrEqualTo(String value) {
            addCriterion("construction_qualification_code <=", value, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeLike(String value) {
            addCriterion("construction_qualification_code like", value, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeNotLike(String value) {
            addCriterion("construction_qualification_code not like", value, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeIn(List<String> values) {
            addCriterion("construction_qualification_code in", values, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeNotIn(List<String> values) {
            addCriterion("construction_qualification_code not in", values, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeBetween(String value1, String value2) {
            addCriterion("construction_qualification_code between", value1, value2, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andConstructionQualificationCodeNotBetween(String value1, String value2) {
            addCriterion("construction_qualification_code not between", value1, value2, "constructionQualificationCode");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelIsNull() {
            addCriterion("real_estate_level is null");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelIsNotNull() {
            addCriterion("real_estate_level is not null");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelEqualTo(Integer value) {
            addCriterion("real_estate_level =", value, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelNotEqualTo(Integer value) {
            addCriterion("real_estate_level <>", value, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelGreaterThan(Integer value) {
            addCriterion("real_estate_level >", value, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelGreaterThanOrEqualTo(Integer value) {
            addCriterion("real_estate_level >=", value, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelLessThan(Integer value) {
            addCriterion("real_estate_level <", value, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelLessThanOrEqualTo(Integer value) {
            addCriterion("real_estate_level <=", value, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelIn(List<Integer> values) {
            addCriterion("real_estate_level in", values, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelNotIn(List<Integer> values) {
            addCriterion("real_estate_level not in", values, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelBetween(Integer value1, Integer value2) {
            addCriterion("real_estate_level between", value1, value2, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andRealEstateLevelNotBetween(Integer value1, Integer value2) {
            addCriterion("real_estate_level not between", value1, value2, "realEstateLevel");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorIsNull() {
            addCriterion("is_hava_superior is null");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorIsNotNull() {
            addCriterion("is_hava_superior is not null");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorEqualTo(Integer value) {
            addCriterion("is_hava_superior =", value, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorNotEqualTo(Integer value) {
            addCriterion("is_hava_superior <>", value, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorGreaterThan(Integer value) {
            addCriterion("is_hava_superior >", value, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_hava_superior >=", value, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorLessThan(Integer value) {
            addCriterion("is_hava_superior <", value, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorLessThanOrEqualTo(Integer value) {
            addCriterion("is_hava_superior <=", value, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorIn(List<Integer> values) {
            addCriterion("is_hava_superior in", values, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorNotIn(List<Integer> values) {
            addCriterion("is_hava_superior not in", values, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorBetween(Integer value1, Integer value2) {
            addCriterion("is_hava_superior between", value1, value2, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andIsHavaSuperiorNotBetween(Integer value1, Integer value2) {
            addCriterion("is_hava_superior not between", value1, value2, "isHavaSuperior");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeIsNull() {
            addCriterion("superior_organization_code is null");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeIsNotNull() {
            addCriterion("superior_organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeEqualTo(String value) {
            addCriterion("superior_organization_code =", value, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeNotEqualTo(String value) {
            addCriterion("superior_organization_code <>", value, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeGreaterThan(String value) {
            addCriterion("superior_organization_code >", value, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("superior_organization_code >=", value, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeLessThan(String value) {
            addCriterion("superior_organization_code <", value, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("superior_organization_code <=", value, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeLike(String value) {
            addCriterion("superior_organization_code like", value, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeNotLike(String value) {
            addCriterion("superior_organization_code not like", value, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeIn(List<String> values) {
            addCriterion("superior_organization_code in", values, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeNotIn(List<String> values) {
            addCriterion("superior_organization_code not in", values, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeBetween(String value1, String value2) {
            addCriterion("superior_organization_code between", value1, value2, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("superior_organization_code not between", value1, value2, "superiorOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeIsNull() {
            addCriterion("superior_social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeIsNotNull() {
            addCriterion("superior_social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeEqualTo(String value) {
            addCriterion("superior_social_credit_code =", value, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeNotEqualTo(String value) {
            addCriterion("superior_social_credit_code <>", value, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeGreaterThan(String value) {
            addCriterion("superior_social_credit_code >", value, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("superior_social_credit_code >=", value, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeLessThan(String value) {
            addCriterion("superior_social_credit_code <", value, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("superior_social_credit_code <=", value, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeLike(String value) {
            addCriterion("superior_social_credit_code like", value, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeNotLike(String value) {
            addCriterion("superior_social_credit_code not like", value, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeIn(List<String> values) {
            addCriterion("superior_social_credit_code in", values, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeNotIn(List<String> values) {
            addCriterion("superior_social_credit_code not in", values, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("superior_social_credit_code between", value1, value2, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("superior_social_credit_code not between", value1, value2, "superiorSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameIsNull() {
            addCriterion("superior_unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameIsNotNull() {
            addCriterion("superior_unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameEqualTo(String value) {
            addCriterion("superior_unit_detailed_name =", value, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameNotEqualTo(String value) {
            addCriterion("superior_unit_detailed_name <>", value, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameGreaterThan(String value) {
            addCriterion("superior_unit_detailed_name >", value, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("superior_unit_detailed_name >=", value, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameLessThan(String value) {
            addCriterion("superior_unit_detailed_name <", value, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("superior_unit_detailed_name <=", value, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameLike(String value) {
            addCriterion("superior_unit_detailed_name like", value, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameNotLike(String value) {
            addCriterion("superior_unit_detailed_name not like", value, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameIn(List<String> values) {
            addCriterion("superior_unit_detailed_name in", values, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameNotIn(List<String> values) {
            addCriterion("superior_unit_detailed_name not in", values, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("superior_unit_detailed_name between", value1, value2, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSuperiorUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("superior_unit_detailed_name not between", value1, value2, "superiorUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialIsNull() {
            addCriterion("is_have_industrial is null");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialIsNotNull() {
            addCriterion("is_have_industrial is not null");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialEqualTo(Integer value) {
            addCriterion("is_have_industrial =", value, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialNotEqualTo(Integer value) {
            addCriterion("is_have_industrial <>", value, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialGreaterThan(Integer value) {
            addCriterion("is_have_industrial >", value, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_have_industrial >=", value, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialLessThan(Integer value) {
            addCriterion("is_have_industrial <", value, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialLessThanOrEqualTo(Integer value) {
            addCriterion("is_have_industrial <=", value, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialIn(List<Integer> values) {
            addCriterion("is_have_industrial in", values, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialNotIn(List<Integer> values) {
            addCriterion("is_have_industrial not in", values, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialBetween(Integer value1, Integer value2) {
            addCriterion("is_have_industrial between", value1, value2, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andIsHaveIndustrialNotBetween(Integer value1, Integer value2) {
            addCriterion("is_have_industrial not between", value1, value2, "isHaveIndustrial");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDIsNull() {
            addCriterion("unit_type_d is null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDIsNotNull() {
            addCriterion("unit_type_d is not null");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDEqualTo(Integer value) {
            addCriterion("unit_type_d =", value, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDNotEqualTo(Integer value) {
            addCriterion("unit_type_d <>", value, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDGreaterThan(Integer value) {
            addCriterion("unit_type_d >", value, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDGreaterThanOrEqualTo(Integer value) {
            addCriterion("unit_type_d >=", value, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDLessThan(Integer value) {
            addCriterion("unit_type_d <", value, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDLessThanOrEqualTo(Integer value) {
            addCriterion("unit_type_d <=", value, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDIn(List<Integer> values) {
            addCriterion("unit_type_d in", values, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDNotIn(List<Integer> values) {
            addCriterion("unit_type_d not in", values, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDBetween(Integer value1, Integer value2) {
            addCriterion("unit_type_d between", value1, value2, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andUnitTypeDNotBetween(Integer value1, Integer value2) {
            addCriterion("unit_type_d not between", value1, value2, "unitTypeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDIsNull() {
            addCriterion("social_credit_code_d is null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDIsNotNull() {
            addCriterion("social_credit_code_d is not null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDEqualTo(String value) {
            addCriterion("social_credit_code_d =", value, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDNotEqualTo(String value) {
            addCriterion("social_credit_code_d <>", value, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDGreaterThan(String value) {
            addCriterion("social_credit_code_d >", value, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDGreaterThanOrEqualTo(String value) {
            addCriterion("social_credit_code_d >=", value, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDLessThan(String value) {
            addCriterion("social_credit_code_d <", value, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDLessThanOrEqualTo(String value) {
            addCriterion("social_credit_code_d <=", value, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDLike(String value) {
            addCriterion("social_credit_code_d like", value, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDNotLike(String value) {
            addCriterion("social_credit_code_d not like", value, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDIn(List<String> values) {
            addCriterion("social_credit_code_d in", values, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDNotIn(List<String> values) {
            addCriterion("social_credit_code_d not in", values, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDBetween(String value1, String value2) {
            addCriterion("social_credit_code_d between", value1, value2, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeDNotBetween(String value1, String value2) {
            addCriterion("social_credit_code_d not between", value1, value2, "socialCreditCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDIsNull() {
            addCriterion("organization_code_d is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDIsNotNull() {
            addCriterion("organization_code_d is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDEqualTo(String value) {
            addCriterion("organization_code_d =", value, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDNotEqualTo(String value) {
            addCriterion("organization_code_d <>", value, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDGreaterThan(String value) {
            addCriterion("organization_code_d >", value, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDGreaterThanOrEqualTo(String value) {
            addCriterion("organization_code_d >=", value, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDLessThan(String value) {
            addCriterion("organization_code_d <", value, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDLessThanOrEqualTo(String value) {
            addCriterion("organization_code_d <=", value, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDLike(String value) {
            addCriterion("organization_code_d like", value, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDNotLike(String value) {
            addCriterion("organization_code_d not like", value, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDIn(List<String> values) {
            addCriterion("organization_code_d in", values, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDNotIn(List<String> values) {
            addCriterion("organization_code_d not in", values, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDBetween(String value1, String value2) {
            addCriterion("organization_code_d between", value1, value2, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeDNotBetween(String value1, String value2) {
            addCriterion("organization_code_d not between", value1, value2, "organizationCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDIsNull() {
            addCriterion("unit_detailed_name_d is null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDIsNotNull() {
            addCriterion("unit_detailed_name_d is not null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDEqualTo(String value) {
            addCriterion("unit_detailed_name_d =", value, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDNotEqualTo(String value) {
            addCriterion("unit_detailed_name_d <>", value, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDGreaterThan(String value) {
            addCriterion("unit_detailed_name_d >", value, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDGreaterThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name_d >=", value, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDLessThan(String value) {
            addCriterion("unit_detailed_name_d <", value, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDLessThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name_d <=", value, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDLike(String value) {
            addCriterion("unit_detailed_name_d like", value, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDNotLike(String value) {
            addCriterion("unit_detailed_name_d not like", value, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDIn(List<String> values) {
            addCriterion("unit_detailed_name_d in", values, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDNotIn(List<String> values) {
            addCriterion("unit_detailed_name_d not in", values, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDBetween(String value1, String value2) {
            addCriterion("unit_detailed_name_d between", value1, value2, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameDNotBetween(String value1, String value2) {
            addCriterion("unit_detailed_name_d not between", value1, value2, "unitDetailedNameD");
            return (Criteria) this;
        }

        public Criteria andAddressDIsNull() {
            addCriterion("address_d is null");
            return (Criteria) this;
        }

        public Criteria andAddressDIsNotNull() {
            addCriterion("address_d is not null");
            return (Criteria) this;
        }

        public Criteria andAddressDEqualTo(String value) {
            addCriterion("address_d =", value, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDNotEqualTo(String value) {
            addCriterion("address_d <>", value, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDGreaterThan(String value) {
            addCriterion("address_d >", value, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDGreaterThanOrEqualTo(String value) {
            addCriterion("address_d >=", value, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDLessThan(String value) {
            addCriterion("address_d <", value, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDLessThanOrEqualTo(String value) {
            addCriterion("address_d <=", value, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDLike(String value) {
            addCriterion("address_d like", value, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDNotLike(String value) {
            addCriterion("address_d not like", value, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDIn(List<String> values) {
            addCriterion("address_d in", values, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDNotIn(List<String> values) {
            addCriterion("address_d not in", values, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDBetween(String value1, String value2) {
            addCriterion("address_d between", value1, value2, "addressD");
            return (Criteria) this;
        }

        public Criteria andAddressDNotBetween(String value1, String value2) {
            addCriterion("address_d not between", value1, value2, "addressD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDIsNull() {
            addCriterion("unit_zoning_code_d is null");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDIsNotNull() {
            addCriterion("unit_zoning_code_d is not null");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDEqualTo(String value) {
            addCriterion("unit_zoning_code_d =", value, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDNotEqualTo(String value) {
            addCriterion("unit_zoning_code_d <>", value, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDGreaterThan(String value) {
            addCriterion("unit_zoning_code_d >", value, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDGreaterThanOrEqualTo(String value) {
            addCriterion("unit_zoning_code_d >=", value, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDLessThan(String value) {
            addCriterion("unit_zoning_code_d <", value, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDLessThanOrEqualTo(String value) {
            addCriterion("unit_zoning_code_d <=", value, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDLike(String value) {
            addCriterion("unit_zoning_code_d like", value, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDNotLike(String value) {
            addCriterion("unit_zoning_code_d not like", value, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDIn(List<String> values) {
            addCriterion("unit_zoning_code_d in", values, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDNotIn(List<String> values) {
            addCriterion("unit_zoning_code_d not in", values, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDBetween(String value1, String value2) {
            addCriterion("unit_zoning_code_d between", value1, value2, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andUnitZoningCodeDNotBetween(String value1, String value2) {
            addCriterion("unit_zoning_code_d not between", value1, value2, "unitZoningCodeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDIsNull() {
            addCriterion("employee_d is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeDIsNotNull() {
            addCriterion("employee_d is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeDEqualTo(Integer value) {
            addCriterion("employee_d =", value, "employeeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDNotEqualTo(Integer value) {
            addCriterion("employee_d <>", value, "employeeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDGreaterThan(Integer value) {
            addCriterion("employee_d >", value, "employeeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDGreaterThanOrEqualTo(Integer value) {
            addCriterion("employee_d >=", value, "employeeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDLessThan(Integer value) {
            addCriterion("employee_d <", value, "employeeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDLessThanOrEqualTo(Integer value) {
            addCriterion("employee_d <=", value, "employeeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDIn(List<Integer> values) {
            addCriterion("employee_d in", values, "employeeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDNotIn(List<Integer> values) {
            addCriterion("employee_d not in", values, "employeeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDBetween(Integer value1, Integer value2) {
            addCriterion("employee_d between", value1, value2, "employeeD");
            return (Criteria) this;
        }

        public Criteria andEmployeeDNotBetween(Integer value1, Integer value2) {
            addCriterion("employee_d not between", value1, value2, "employeeD");
            return (Criteria) this;
        }

        public Criteria andFemaleDIsNull() {
            addCriterion("female_d is null");
            return (Criteria) this;
        }

        public Criteria andFemaleDIsNotNull() {
            addCriterion("female_d is not null");
            return (Criteria) this;
        }

        public Criteria andFemaleDEqualTo(Integer value) {
            addCriterion("female_d =", value, "femaleD");
            return (Criteria) this;
        }

        public Criteria andFemaleDNotEqualTo(Integer value) {
            addCriterion("female_d <>", value, "femaleD");
            return (Criteria) this;
        }

        public Criteria andFemaleDGreaterThan(Integer value) {
            addCriterion("female_d >", value, "femaleD");
            return (Criteria) this;
        }

        public Criteria andFemaleDGreaterThanOrEqualTo(Integer value) {
            addCriterion("female_d >=", value, "femaleD");
            return (Criteria) this;
        }

        public Criteria andFemaleDLessThan(Integer value) {
            addCriterion("female_d <", value, "femaleD");
            return (Criteria) this;
        }

        public Criteria andFemaleDLessThanOrEqualTo(Integer value) {
            addCriterion("female_d <=", value, "femaleD");
            return (Criteria) this;
        }

        public Criteria andFemaleDIn(List<Integer> values) {
            addCriterion("female_d in", values, "femaleD");
            return (Criteria) this;
        }

        public Criteria andFemaleDNotIn(List<Integer> values) {
            addCriterion("female_d not in", values, "femaleD");
            return (Criteria) this;
        }

        public Criteria andFemaleDBetween(Integer value1, Integer value2) {
            addCriterion("female_d between", value1, value2, "femaleD");
            return (Criteria) this;
        }

        public Criteria andFemaleDNotBetween(Integer value1, Integer value2) {
            addCriterion("female_d not between", value1, value2, "femaleD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDIsNull() {
            addCriterion("operating_income_d is null");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDIsNotNull() {
            addCriterion("operating_income_d is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDEqualTo(BigDecimal value) {
            addCriterion("operating_income_d =", value, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDNotEqualTo(BigDecimal value) {
            addCriterion("operating_income_d <>", value, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDGreaterThan(BigDecimal value) {
            addCriterion("operating_income_d >", value, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("operating_income_d >=", value, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDLessThan(BigDecimal value) {
            addCriterion("operating_income_d <", value, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDLessThanOrEqualTo(BigDecimal value) {
            addCriterion("operating_income_d <=", value, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDIn(List<BigDecimal> values) {
            addCriterion("operating_income_d in", values, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDNotIn(List<BigDecimal> values) {
            addCriterion("operating_income_d not in", values, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("operating_income_d between", value1, value2, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andOperatingIncomeDNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("operating_income_d not between", value1, value2, "operatingIncomeD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDIsNull() {
            addCriterion("non_operating_expenditure_d is null");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDIsNotNull() {
            addCriterion("non_operating_expenditure_d is not null");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDEqualTo(BigDecimal value) {
            addCriterion("non_operating_expenditure_d =", value, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDNotEqualTo(BigDecimal value) {
            addCriterion("non_operating_expenditure_d <>", value, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDGreaterThan(BigDecimal value) {
            addCriterion("non_operating_expenditure_d >", value, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("non_operating_expenditure_d >=", value, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDLessThan(BigDecimal value) {
            addCriterion("non_operating_expenditure_d <", value, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDLessThanOrEqualTo(BigDecimal value) {
            addCriterion("non_operating_expenditure_d <=", value, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDIn(List<BigDecimal> values) {
            addCriterion("non_operating_expenditure_d in", values, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDNotIn(List<BigDecimal> values) {
            addCriterion("non_operating_expenditure_d not in", values, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("non_operating_expenditure_d between", value1, value2, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andNonOperatingExpenditureDNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("non_operating_expenditure_d not between", value1, value2, "nonOperatingExpenditureD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDIsNull() {
            addCriterion("construction_area_d is null");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDIsNotNull() {
            addCriterion("construction_area_d is not null");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDEqualTo(Float value) {
            addCriterion("construction_area_d =", value, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDNotEqualTo(Float value) {
            addCriterion("construction_area_d <>", value, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDGreaterThan(Float value) {
            addCriterion("construction_area_d >", value, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDGreaterThanOrEqualTo(Float value) {
            addCriterion("construction_area_d >=", value, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDLessThan(Float value) {
            addCriterion("construction_area_d <", value, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDLessThanOrEqualTo(Float value) {
            addCriterion("construction_area_d <=", value, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDIn(List<Float> values) {
            addCriterion("construction_area_d in", values, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDNotIn(List<Float> values) {
            addCriterion("construction_area_d not in", values, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDBetween(Float value1, Float value2) {
            addCriterion("construction_area_d between", value1, value2, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andConstructionAreaDNotBetween(Float value1, Float value2) {
            addCriterion("construction_area_d not between", value1, value2, "constructionAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDIsNull() {
            addCriterion("sales_area_d is null");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDIsNotNull() {
            addCriterion("sales_area_d is not null");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDEqualTo(Float value) {
            addCriterion("sales_area_d =", value, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDNotEqualTo(Float value) {
            addCriterion("sales_area_d <>", value, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDGreaterThan(Float value) {
            addCriterion("sales_area_d >", value, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDGreaterThanOrEqualTo(Float value) {
            addCriterion("sales_area_d >=", value, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDLessThan(Float value) {
            addCriterion("sales_area_d <", value, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDLessThanOrEqualTo(Float value) {
            addCriterion("sales_area_d <=", value, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDIn(List<Float> values) {
            addCriterion("sales_area_d in", values, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDNotIn(List<Float> values) {
            addCriterion("sales_area_d not in", values, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDBetween(Float value1, Float value2) {
            addCriterion("sales_area_d between", value1, value2, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andSalesAreaDNotBetween(Float value1, Float value2) {
            addCriterion("sales_area_d not between", value1, value2, "salesAreaD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDIsNull() {
            addCriterion("area_for_sale_d is null");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDIsNotNull() {
            addCriterion("area_for_sale_d is not null");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDEqualTo(Float value) {
            addCriterion("area_for_sale_d =", value, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDNotEqualTo(Float value) {
            addCriterion("area_for_sale_d <>", value, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDGreaterThan(Float value) {
            addCriterion("area_for_sale_d >", value, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDGreaterThanOrEqualTo(Float value) {
            addCriterion("area_for_sale_d >=", value, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDLessThan(Float value) {
            addCriterion("area_for_sale_d <", value, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDLessThanOrEqualTo(Float value) {
            addCriterion("area_for_sale_d <=", value, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDIn(List<Float> values) {
            addCriterion("area_for_sale_d in", values, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDNotIn(List<Float> values) {
            addCriterion("area_for_sale_d not in", values, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDBetween(Float value1, Float value2) {
            addCriterion("area_for_sale_d between", value1, value2, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andAreaForSaleDNotBetween(Float value1, Float value2) {
            addCriterion("area_for_sale_d not between", value1, value2, "areaForSaleD");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIsNull() {
            addCriterion("unit_head is null");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIsNotNull() {
            addCriterion("unit_head is not null");
            return (Criteria) this;
        }

        public Criteria andUnitHeadEqualTo(String value) {
            addCriterion("unit_head =", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotEqualTo(String value) {
            addCriterion("unit_head <>", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadGreaterThan(String value) {
            addCriterion("unit_head >", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadGreaterThanOrEqualTo(String value) {
            addCriterion("unit_head >=", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLessThan(String value) {
            addCriterion("unit_head <", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLessThanOrEqualTo(String value) {
            addCriterion("unit_head <=", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLike(String value) {
            addCriterion("unit_head like", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotLike(String value) {
            addCriterion("unit_head not like", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIn(List<String> values) {
            addCriterion("unit_head in", values, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotIn(List<String> values) {
            addCriterion("unit_head not in", values, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadBetween(String value1, String value2) {
            addCriterion("unit_head between", value1, value2, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotBetween(String value1, String value2) {
            addCriterion("unit_head not between", value1, value2, "unitHead");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIsNull() {
            addCriterion("statistical_control_officer is null");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIsNotNull() {
            addCriterion("statistical_control_officer is not null");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerEqualTo(String value) {
            addCriterion("statistical_control_officer =", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotEqualTo(String value) {
            addCriterion("statistical_control_officer <>", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerGreaterThan(String value) {
            addCriterion("statistical_control_officer >", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerGreaterThanOrEqualTo(String value) {
            addCriterion("statistical_control_officer >=", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLessThan(String value) {
            addCriterion("statistical_control_officer <", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLessThanOrEqualTo(String value) {
            addCriterion("statistical_control_officer <=", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLike(String value) {
            addCriterion("statistical_control_officer like", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotLike(String value) {
            addCriterion("statistical_control_officer not like", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIn(List<String> values) {
            addCriterion("statistical_control_officer in", values, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotIn(List<String> values) {
            addCriterion("statistical_control_officer not in", values, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerBetween(String value1, String value2) {
            addCriterion("statistical_control_officer between", value1, value2, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotBetween(String value1, String value2) {
            addCriterion("statistical_control_officer not between", value1, value2, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNull() {
            addCriterion("fill_form_by is null");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNotNull() {
            addCriterion("fill_form_by is not null");
            return (Criteria) this;
        }

        public Criteria andFillFormByEqualTo(String value) {
            addCriterion("fill_form_by =", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotEqualTo(String value) {
            addCriterion("fill_form_by <>", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThan(String value) {
            addCriterion("fill_form_by >", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThanOrEqualTo(String value) {
            addCriterion("fill_form_by >=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThan(String value) {
            addCriterion("fill_form_by <", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThanOrEqualTo(String value) {
            addCriterion("fill_form_by <=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLike(String value) {
            addCriterion("fill_form_by like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotLike(String value) {
            addCriterion("fill_form_by not like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByIn(List<String> values) {
            addCriterion("fill_form_by in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotIn(List<String> values) {
            addCriterion("fill_form_by not in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByBetween(String value1, String value2) {
            addCriterion("fill_form_by between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotBetween(String value1, String value2) {
            addCriterion("fill_form_by not between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberIsNull() {
            addCriterion("extension_number is null");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberIsNotNull() {
            addCriterion("extension_number is not null");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberEqualTo(String value) {
            addCriterion("extension_number =", value, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberNotEqualTo(String value) {
            addCriterion("extension_number <>", value, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberGreaterThan(String value) {
            addCriterion("extension_number >", value, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberGreaterThanOrEqualTo(String value) {
            addCriterion("extension_number >=", value, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberLessThan(String value) {
            addCriterion("extension_number <", value, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberLessThanOrEqualTo(String value) {
            addCriterion("extension_number <=", value, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberLike(String value) {
            addCriterion("extension_number like", value, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberNotLike(String value) {
            addCriterion("extension_number not like", value, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberIn(List<String> values) {
            addCriterion("extension_number in", values, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberNotIn(List<String> values) {
            addCriterion("extension_number not in", values, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberBetween(String value1, String value2) {
            addCriterion("extension_number between", value1, value2, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andExtensionNumberNotBetween(String value1, String value2) {
            addCriterion("extension_number not between", value1, value2, "extensionNumber");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andReportingDateIsNull() {
            addCriterion("reporting_date is null");
            return (Criteria) this;
        }

        public Criteria andReportingDateIsNotNull() {
            addCriterion("reporting_date is not null");
            return (Criteria) this;
        }

        public Criteria andReportingDateEqualTo(Date value) {
            addCriterion("reporting_date =", value, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andReportingDateNotEqualTo(Date value) {
            addCriterion("reporting_date <>", value, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andReportingDateGreaterThan(Date value) {
            addCriterion("reporting_date >", value, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andReportingDateGreaterThanOrEqualTo(Date value) {
            addCriterion("reporting_date >=", value, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andReportingDateLessThan(Date value) {
            addCriterion("reporting_date <", value, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andReportingDateLessThanOrEqualTo(Date value) {
            addCriterion("reporting_date <=", value, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andReportingDateIn(List<Date> values) {
            addCriterion("reporting_date in", values, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andReportingDateNotIn(List<Date> values) {
            addCriterion("reporting_date not in", values, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andReportingDateBetween(Date value1, Date value2) {
            addCriterion("reporting_date between", value1, value2, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andReportingDateNotBetween(Date value1, Date value2) {
            addCriterion("reporting_date not between", value1, value2, "reportingDate");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andMacaoInputIsNull() {
            addCriterion("macao_input is null");
            return (Criteria) this;
        }

        public Criteria andMacaoInputIsNotNull() {
            addCriterion("macao_input is not null");
            return (Criteria) this;
        }

        public Criteria andMacaoInputEqualTo(BigDecimal value) {
            addCriterion("macao_input =", value, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoInputNotEqualTo(BigDecimal value) {
            addCriterion("macao_input <>", value, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoInputGreaterThan(BigDecimal value) {
            addCriterion("macao_input >", value, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoInputGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("macao_input >=", value, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoInputLessThan(BigDecimal value) {
            addCriterion("macao_input <", value, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoInputLessThanOrEqualTo(BigDecimal value) {
            addCriterion("macao_input <=", value, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoInputIn(List<BigDecimal> values) {
            addCriterion("macao_input in", values, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoInputNotIn(List<BigDecimal> values) {
            addCriterion("macao_input not in", values, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoInputBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("macao_input between", value1, value2, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoInputNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("macao_input not between", value1, value2, "macaoInput");
            return (Criteria) this;
        }

        public Criteria andMacaoIsNull() {
            addCriterion("macao is null");
            return (Criteria) this;
        }

        public Criteria andMacaoIsNotNull() {
            addCriterion("macao is not null");
            return (Criteria) this;
        }

        public Criteria andMacaoEqualTo(Integer value) {
            addCriterion("macao =", value, "macao");
            return (Criteria) this;
        }

        public Criteria andMacaoNotEqualTo(Integer value) {
            addCriterion("macao <>", value, "macao");
            return (Criteria) this;
        }

        public Criteria andMacaoGreaterThan(Integer value) {
            addCriterion("macao >", value, "macao");
            return (Criteria) this;
        }

        public Criteria andMacaoGreaterThanOrEqualTo(Integer value) {
            addCriterion("macao >=", value, "macao");
            return (Criteria) this;
        }

        public Criteria andMacaoLessThan(Integer value) {
            addCriterion("macao <", value, "macao");
            return (Criteria) this;
        }

        public Criteria andMacaoLessThanOrEqualTo(Integer value) {
            addCriterion("macao <=", value, "macao");
            return (Criteria) this;
        }

        public Criteria andMacaoIn(List<Integer> values) {
            addCriterion("macao in", values, "macao");
            return (Criteria) this;
        }

        public Criteria andMacaoNotIn(List<Integer> values) {
            addCriterion("macao not in", values, "macao");
            return (Criteria) this;
        }

        public Criteria andMacaoBetween(Integer value1, Integer value2) {
            addCriterion("macao between", value1, value2, "macao");
            return (Criteria) this;
        }

        public Criteria andMacaoNotBetween(Integer value1, Integer value2) {
            addCriterion("macao not between", value1, value2, "macao");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}