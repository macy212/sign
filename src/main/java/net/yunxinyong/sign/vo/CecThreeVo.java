package net.yunxinyong.sign.vo;

import net.yunxinyong.sign.entity.CecThree;
import net.yunxinyong.sign.entity.CecThreePlatform;

import java.util.List;

public class CecThreeVo extends CecThree {

    private List<CecThreePlatform> brancesList;
    private String majorType;

    public String getMajorType() {
        return majorType;
    }

    public void setMajorType(String majorType) {
        this.majorType = majorType;
    }


    public List<CecThreePlatform> getBrancesList() {
        return brancesList;
    }

    public void setBrancesList(List<CecThreePlatform> brancesList) {
        this.brancesList = brancesList;
    }
}
