package net.yunxinyong.sign.vo;

import net.yunxinyong.sign.utils.ExcelAnno;

public class TestExcelVo2 {


    @ExcelAnno(head = "组织机构代码")
    private String organizationCode;
    @ExcelAnno(head = "统一社会信用代码")
    private String socialCreditCode;
    @ExcelAnno(head = "公司名称")
    private String unitDetailedName;
    @ExcelAnno(head = "法人")
    private String legalRepresentative;
    @ExcelAnno(head = "手机号")
    private String phone;

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode;
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative;
    }
}
