package net.yunxinyong.sign.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import net.yunxinyong.sign.entity.AppointmentTime;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class AppointmentTimeVo extends AppointmentTime {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
