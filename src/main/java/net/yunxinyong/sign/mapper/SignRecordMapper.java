package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.SignRecord;
import net.yunxinyong.sign.entity.SignRecordExample;
import org.apache.ibatis.annotations.Param;

public interface SignRecordMapper {
    int countByExample(SignRecordExample example);

    int deleteByExample(SignRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SignRecord record);

    int insertSelective(SignRecord record);

    List<SignRecord> selectByExample(SignRecordExample example);

    SignRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SignRecord record, @Param("example") SignRecordExample example);

    int updateByExample(@Param("record") SignRecord record, @Param("example") SignRecordExample example);

    int updateByPrimaryKeySelective(SignRecord record);

    int updateByPrimaryKey(SignRecord record);
}