package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.CecSix;
import net.yunxinyong.sign.entity.CecSixExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface CecSixMapper {
    int countByExample(CecSixExample example);

    int deleteByExample(CecSixExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecSix record);

    int insertSelective(CecSix record);

    List<CecSix> selectByExample(CecSixExample example);

    CecSix selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecSix record, @Param("example") CecSixExample example);

    int updateByExample(@Param("record") CecSix record, @Param("example") CecSixExample example);

    int updateByPrimaryKeySelective(CecSix record);

    int updateByPrimaryKey(CecSix record);
}