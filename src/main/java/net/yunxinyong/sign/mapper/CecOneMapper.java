package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.CecOne;
import net.yunxinyong.sign.entity.CecOneExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface CecOneMapper {
    int countByExample(CecOneExample example);

    int deleteByExample(CecOneExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecOne record);

    int insertSelective(CecOne record);

    List<CecOne> selectByExample(CecOneExample example);

    CecOne selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecOne record, @Param("example") CecOneExample example);

    int updateByExample(@Param("record") CecOne record, @Param("example") CecOneExample example);

    int updateByPrimaryKeySelective(CecOne record);

    int updateByPrimaryKey(CecOne record);
}