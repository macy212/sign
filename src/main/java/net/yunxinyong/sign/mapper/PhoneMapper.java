package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.Phone;
import net.yunxinyong.sign.entity.PhoneExample;
import org.apache.ibatis.annotations.Param;

public interface PhoneMapper {
    int countByExample(PhoneExample example);

    int deleteByExample(PhoneExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Phone record);

    int insertSelective(Phone record);

    List<Phone> selectByExample(PhoneExample example);

    Phone selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Phone record, @Param("example") PhoneExample example);

    int updateByExample(@Param("record") Phone record, @Param("example") PhoneExample example);

    int updateByPrimaryKeySelective(Phone record);

    int updateByPrimaryKey(Phone record);
}