package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.CecFour;
import net.yunxinyong.sign.entity.CecFourExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface CecFourMapper {
    int countByExample(CecFourExample example);

    int deleteByExample(CecFourExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecFour record);

    int insertSelective(CecFour record);

    List<CecFour> selectByExample(CecFourExample example);

    CecFour selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecFour record, @Param("example") CecFourExample example);

    int updateByExample(@Param("record") CecFour record, @Param("example") CecFourExample example);

    int updateByPrimaryKeySelective(CecFour record);

    int updateByPrimaryKey(CecFour record);
}