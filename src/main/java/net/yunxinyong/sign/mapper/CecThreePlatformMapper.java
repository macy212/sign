package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.CecThreePlatform;
import net.yunxinyong.sign.entity.CecThreePlatformExample;
import org.apache.ibatis.annotations.Param;

public interface CecThreePlatformMapper {
    int countByExample(CecThreePlatformExample example);

    int deleteByExample(CecThreePlatformExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecThreePlatform record);

    int insertSelective(CecThreePlatform record);

    List<CecThreePlatform> selectByExample(CecThreePlatformExample example);

    CecThreePlatform selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecThreePlatform record, @Param("example") CecThreePlatformExample example);

    int updateByExample(@Param("record") CecThreePlatform record, @Param("example") CecThreePlatformExample example);

    int updateByPrimaryKeySelective(CecThreePlatform record);

    int updateByPrimaryKey(CecThreePlatform record);
}