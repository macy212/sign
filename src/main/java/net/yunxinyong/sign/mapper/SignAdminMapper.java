package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.entity.SignAdminExample;
import org.apache.ibatis.annotations.Param;

public interface SignAdminMapper {
    int countByExample(SignAdminExample example);

    int deleteByExample(SignAdminExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SignAdmin record);

    int insertSelective(SignAdmin record);

    List<SignAdmin> selectByExample(SignAdminExample example);

    SignAdmin selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SignAdmin record, @Param("example") SignAdminExample example);

    int updateByExample(@Param("record") SignAdmin record, @Param("example") SignAdminExample example);

    int updateByPrimaryKeySelective(SignAdmin record);

    int updateByPrimaryKey(SignAdmin record);
}