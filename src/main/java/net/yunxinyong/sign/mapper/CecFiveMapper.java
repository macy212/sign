package net.yunxinyong.sign.mapper;

import java.util.List;

import net.yunxinyong.sign.entity.CecFive;
import net.yunxinyong.sign.entity.CecFiveExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CecFiveMapper {
    int countByExample(CecFiveExample example);

    int deleteByExample(CecFiveExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecFive record);

    int insertSelective(CecFive record);

    List<CecFive> selectByExample(CecFiveExample example);

    CecFive selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecFive record, @Param("example") CecFiveExample example);

    int updateByExample(@Param("record") CecFive record, @Param("example") CecFiveExample example);

    int updateByPrimaryKeySelective(CecFive record);

    int updateByPrimaryKey(CecFive record);
}