package net.yunxinyong.sign.service;


import net.yunxinyong.sign.entity.Image;

import java.io.IOException;
import java.util.List;

public interface QiniuService {
    /**
     * 添加企业登记表上传记录
     * @param imageList
     * @return
     */
    void insert(List<Image> imageList, Integer invId, String name);

    /**
     * 获取企业所有表格pdf
     * @param invId
     * @return
     */
    List<String> getUrl(Integer invId);

    List<String> getExamineUrl(Integer invId);

    String mkzip(List<Integer> idList,Integer type,Integer adminId) throws IOException;
}
