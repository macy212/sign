package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.utils.PageBean;

/**
 *@describe 项目经理权限模块
 *@author  ttbf
 *@date  2018/10/29
 */
public interface ProjectManagerService {

    /**
     * 添加普查员
     * @param signAdmin
     * @param id 操作人id
     * @return
     */
    SignAdmin insert(SignAdmin signAdmin,Integer id);

    /**
     * 修改普查员权限
     * @param signAdmin
     * @param id 操作人id
     * @return
     */
    SignAdmin update(SignAdmin signAdmin,Integer id);

    /**
     * 查询普查员列表
     * @param signAdmin
     * @param id 操作人id
     * @return
     */
    PageBean getList(SignAdmin signAdmin,Integer id, int page, int rows);


}
