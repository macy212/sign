package net.yunxinyong.sign.service;

/*
 * @author
 * @describe
 * @date
 * 每张表的填写进度
 */
public interface ProgressOfJudgmentService {
    /**
     * 计算611-Base表填写进度,总必填项为
     * 除了有选项的内容（比如选择了E 项，则F项必填。现在F项不算必填），剩下的内容都是必填
     * @param invId
     * @return
     */
     double getBaseNum(int invId);

    /**
     * 计算611-1表填写进度,总必填项为
     * @param invId
     * @return
     */
     double getOneNum(int invId);

    /**
     * 计算611-2表填写进度,总必填项为14
     * 二表的所有字段都是必填字段
     *
     */
     double getTwoNum(int invId);

    /**
     * 计算611-3表填写进度，总必填项为
     * @param invId
     * @return
     */
     double getThreeNum(int invId);

    /**
     * 计算611-4表填写进度,总必填项为
     * @param invId
     * @return
     */
     double getFourNum(int invId);

    /**
     * 计算611-5表填写进度,总必填项为
     * @param invId
     * @return
     */
     double getFiveNum(int invId);

    /**
     * 计算611-6表填写进度,总必填项为
     * @param invId
     * @return
     */

     double getSixNum(int invId);

    /**
     * 计算611-7表填写进度，总必填项为
     * @param invId
     * @return
     */
    double getSevenNum(int invId);

    /**
     *
     * @param invId
     * @return
     */
    double getTotalProgress(int invId);


}
