package net.yunxinyong.sign.service.impl;

import net.yunxinyong.sign.entity.*;
import net.yunxinyong.sign.mapper.*;
import net.yunxinyong.sign.service.ProgressOfJudgmentService;
import net.yunxinyong.sign.utils.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProgressOfJudgmentServiceImpl implements ProgressOfJudgmentService {
    @Autowired
    private CecBaseMapper cecBaseMapper;
    @Autowired
    private CecOneMapper cecOneMapper;
    @Autowired
    private CecTwoMapper cecTwoMapper;
    @Autowired
    private CecThreeMapper cecThreeMapper;
    @Autowired
    private CecFourMapper cecFourMapper;
    @Autowired
    private CecFiveMapper cecFiveMapper;
    @Autowired
    private CecSixMapper cecSixMapper;
    @Autowired
    private CecSevenMapper cecSevenMapper;
    @Autowired
    private CecThreePlatformMapper cecThreePlatformMapper;
    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;



    @Override
    public double getBaseNum(int invId) {
        double count = 0;
        CecBaseExample cecBaseExample = new CecBaseExample();
        cecBaseExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        List<CecBase> cecBaseList = cecBaseMapper.selectByExample(cecBaseExample);
        if(!ListUtils.isEmpty(cecBaseList)) {
            CecBase cecBase = cecBaseList.get(0);
            if (cecBase.getUnitType() != null) {
                count++;
            }
            if (cecBase.getMajorType() != null && !"".equals(cecBase.getMajorType())) {
                count++;
            }
            if ((cecBase.getSocialCreditCode() != null && !"".equals(cecBase.getSocialCreditCode())) || (cecBase.getOrganizationCode() != null && !"".equals(cecBase.getOrganizationCode()))) {
                count++;
            }
            if (cecBase.getLegalRepresentative() != null && !"".equals(cecBase.getLegalRepresentative())) {
                count++;
            }
            if (cecBase.getEstablishmentTime() != null && !"".equals(cecBase.getEstablishmentTime())) {
                count++;
            }
            //固定电话或者移动电话填一个就可以。
            if ((cecBase.getFixedLineTelephone() != null && !"".equals(cecBase.getFixedLineTelephone())) || (cecBase.getCellPhoneNumber() != null && !"".equals(cecBase.getCellPhoneNumber())) || (cecBase.getFixedLineTelephone() != null && !"-".equals(cecBase.getFixedLineTelephone()))) {
                count++;
            }
            if (cecBase.getPostalCode() != null && !"".equals(cecBase.getPostalCode())) {
                count++;
            }
            if (cecBase.getProvince() != null && !"".equals(cecBase.getProvince())) {
                count++;
            }
            if (cecBase.getCity() != null && !"".equals(cecBase.getCity())) {
                count++;
            }
            if (cecBase.getCounty() != null && !"".equals(cecBase.getCounty())) {
                count++;
            }
            //乡和街只填一项就可以。
            if ((cecBase.getTowns() != null && !"".equals(cecBase.getTowns())) || (cecBase.getVillage() != null && !"".equals(cecBase.getVillage()))) {
                count++;
            }
            //这里接到办事处和社区也是只填一项就可以。。。。
            if ((cecBase.getSubdistrict() != null && !"".equals(cecBase.getSubdistrict())) || ((cecBase.getCummunity() != null && !"".equals(cecBase.getCummunity()))) ){
                count++;
            }
            if (cecBase.getBusinessState() != null) {
                count++;
            }
            if ((cecBase.getMajorBusiness1() != null && !"".equals(cecBase.getMajorBusiness1())) || (cecBase.getMajorBusiness2() != null && !"".equals(cecBase.getMajorBusiness2())) || (cecBase.getMajorBusiness3() != null && !"".equals(cecBase.getMajorBusiness3()))) {
                count++;
            }
            if (cecBase.getMechanismType() != null) {
                count++;
            }
            if (cecBase.getRegistrationType() != null && !"".equals(cecBase.getRegistrationType())) {
                count++;
            }
            if(cecBase.getIsB() != null){
                count++;
            }
            //法人单位必填字段
            if(cecBase.getUnitType() != null && cecBase.getUnitType() == 1){
                if(cecBase.getAffiliation() !=null  ){
                    count++;
                }
                if(cecBase.getHoldingSituation() != null ){
                    count++;
                }
                if(cecBase.getAccountingStandardCategory() != null ){
                    count++;
                }
                if(cecBase.getIsHavaSuperior() != null ){
                    count++;
                }
                if(cecBase.getIsHaveIndustrial() != null ){
                    count++;
                }
                return count/22;
            }
            //产业活动单位必填字段
            if(cecBase.getUnitType() != null && cecBase.getUnitType() == 2){
                if (cecBase.getUnitTypeD() != null) {
                    count++;
                }
                if (cecBase.getAddressD() != null && !"".equals(cecBase.getAddressD())) {
                    count++;
                }
                if (cecBase.getOrganizationCodeD() != null && !"".equals(cecBase.getOrganizationCodeD())) {
                    count++;
                }
                if (cecBase.getFemaleD() != null) {
                    count++;
                }
                if (cecBase.getEmployeeD() != null) {
                    count++;
                }
                if ((cecBase.getOperatingIncomeD() != null && !"".equals(cecBase.getOperatingIncomeD())) || (cecBase.getNonOperatingExpenditureD() != null && !"".equals(cecBase.getNonOperatingExpenditureD()))) {
                    count++;
                }
                return count/23;
            }
            return count/17;

        }
        return count;
    }
    @Override
    public double getOneNum(int invId) {
        CecOneExample cecOneExample = new CecOneExample();
        cecOneExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        List<CecOne> cecOneList = cecOneMapper.selectByExample(cecOneExample);
        double count = 0;
        if(!ListUtils.isEmpty(cecOneList)){
            for(CecOne cecOne :cecOneList){
                //这里就不不安主表和分表来划分了， 这里循环每天记录的字段，最后加一（这个一代表分支机构的数量）
                if(cecOne.getAmount()!=null){
                    count++;
                }
                if(cecOne.getBrancesSocialCreditCode() != null && !"".equals(cecOne.getBrancesSocialCreditCode())){
                    count++;
                }
                if(cecOne.getBrancesOrganizationCode() != null && !"".equals(cecOne.getBrancesOrganizationCode())){
                    count++;
                }
                if(cecOne.getBrancesUnitDetailedName() != null && !"".equals(cecOne.getBrancesUnitDetailedName())){
                    count++;
                }
                if(cecOne.getBrancesAddress() != null && !"".equals(cecOne.getBrancesAddress())){
                    count++;
                }
                if(cecOne.getBrancesContactNumber() != null && !"".equals(cecOne.getBrancesContactNumber())){
                    count++;
                }
                if(cecOne.getBranceMainBussinese() != null && !"".equals(cecOne.getBranceMainBussinese())){
                    count++;
                }
                if(cecOne.getFinalEmployeesNumber() != null && !"".equals(cecOne.getFinalEmployeesNumber())){
                    count++;
                }
                //经营性单位收入和非经营性单位收入两个字段只填一个就可以
                if(cecOne.getBussineseIncome() != null && !"".equals(cecOne.getBussineseIncome()) || (cecOne.getNonBussineseIncome() != null && !"".equals(cecOne.getNonBussineseIncome()))){
                    count++;
                }
            }
            //计算出1表必填字段的数目
            double number = 1 + cecOneList.size()*8;
            return count/number;
        }
        return count;
    }

    @Override
    public double getTwoNum(int invId) {
        double count = 0;
        CecTwoExample cecTwoExample = new CecTwoExample();
        cecTwoExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        List<CecTwo> cecTwoList = cecTwoMapper.selectByExample(cecTwoExample);
        if(!ListUtils.isEmpty(cecTwoList)){
            CecTwo cecTwo = cecTwoList.get(0);
            if(cecTwo.getFinalEmployeesNumber() != null){
                count++;
            }
            if(cecTwo.getFemaleNumber() != null ){
                count++;
            }
            if(cecTwo.getPostgraduateNumber() != null ){
                count++;
            }
            if(cecTwo.getUndergraduateNumber() != null ){
                count++;
            }
            if(cecTwo.getCollegeNumber() != null ){
                count++;
            }
            if(cecTwo.getSkilledWorkersNumber() != null ){
                count++;
            }
            if(cecTwo.getSkilledLevelOne() != null ){
                count++;
            }
            if(cecTwo.getSkilledLevelTwo() != null ){
                count++;
            }
            if(cecTwo.getSkilledLevelThree() !=null){
                count++;
            }
            if(cecTwo.getSkilledLevelFour() != null ){
                count++;
            }
            if(cecTwo.getSkilledLevelFive() != null){
                count++;
            }
            if(cecTwo.getWorkOutsideProvince() != null ){
                count++;
            }
            if(cecTwo.getDomicileOutsideProvince() != null ){
                count++;
            }
            if(cecTwo.getWorkAndDomicileOutside() != null ){
                count++;
            }
            return count/14;
        }
        return count;
    }

    @Override
    public double getThreeNum(int invId) {
        double count = 0 ;
        CecThreeExample cecThreeExample = new CecThreeExample();
        cecThreeExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        List<CecThree> cecThreesList = cecThreeMapper.selectByExample(cecThreeExample);
        if(!ListUtils.isEmpty(cecThreesList)){
            //取出3表分支表的内容
            CecThreePlatformExample cecThreePlatformExample = new CecThreePlatformExample();
            cecThreePlatformExample.createCriteria().andThreeIdEqualTo(cecThreesList.get(0).getId()).andStatusEqualTo(0);
            List<CecThreePlatform> cecThreePlatformList = cecThreePlatformMapper.selectByExample(cecThreePlatformExample);
            //取出base表的114项，判断企业类型  /*这里若果不先填base表会报数组越界异常*/
            CecBaseExample cecBaseExample = new CecBaseExample();
            cecBaseExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
            List<CecBase> cecBases = cecBaseMapper.selectByExample(cecBaseExample);
            if(ListUtils.isEmpty(cecBases)){
                return 0;
            }
            CecBase  cecBase = cecBases.get(0);
            //取出base表的组织机构代码
            String baseOrganizationCode = cecBase.getOrganizationCode();
            CecThree cecThree = cecThreesList.get(0);
            //如果上下两个组织机构代码不相同，则直接返回一，代表所有的字段都是非必填的。。
            /*
             * 这里要注意，如若上面的组织机构代码或者下面的组织机构代码有一个不填，会报空指针。
             * */
            if(cecThree.getSupplementOrganizationCode() != null && !cecThree.getSupplementOrganizationCode().equals("") && !baseOrganizationCode.equals(cecThree.getSupplementOrganizationCode())){
                count = 1.0;
                return count;
            }
            if(cecThree.getThisYearDepreciation() != null ){
                count++;
            }
            if(cecThree.getFixedAssets() != null ){
                count++;
            }
            if(cecThree.getConstructionProject() != null ){
                count++;
            }
            if(cecThree.getTotalAssets() != null ){
                count++;
            }
            if(cecThree.getTotalDebt() != null ){
                count++;
            }
            if(cecThree.getBeginStock() != null ){
                count++;
            }
            if(cecThree.getEndTermStock() != null ){
                count++;
            }
            if(cecThree.getBusinessIncome() != null ){
                count++;
            }
            if(cecThree.getBusinessCost() != null ){
                count++;
            }
            if(cecThree.getTaxation() != null ){
                count++;
            }
            if(cecThree.getInvestmentIncome() != null ){
                count++;
            }
            if(cecThree.getOperatingProfit() != null  ){
                count++;
            }
            if(cecThree.getTotalRemuneration() != null ){
                count++;
            }
            if(cecThree.getTotalTax() != null ){
                count++;
            }
            //根据base表查出来的行业代码判断填写内容
            if(cecBase.getMajorType().equals("E")){
                if(cecThree.getCommodityPurchase() != null  ){
                    count++;
                }
                if(cecThree.getCommoditySales() != null ){
                    count++;
                }
                if(cecThree.geteCommoditySales() != null ){
                    count++;
                }
                if(cecThree.getRetailSales() != null ){
                    count++;
                }
                if(cecThree.geteRetailSales() != null ){
                    count++;
                }
                if(cecThree.getFinalCommodityStock() != null ){
                    count++;
                }
                if(cecThree.getServiceTurnover() != null ){
                    count++;
                }
                if(cecThree.getIsHaveEPlatform()== null || cecThree.getIsHaveEPlatform() == 2 ){
                    return count/21;
                }
                if(cecThree.getIsHaveEPlatform() == 1){
                    count++;
                    if(cecThree.getePlatformNumber() != null ){
                        count++;
                    }
                    if((cecThree.geteEconomicActivity()!=null && !"".equals(cecThree.geteEconomicActivity())) || (cecThree.getOtherEconomic()!=null && !"".equals(cecThree.getOtherEconomic()))){
                        count++;
                    }
                    if((cecThree.geteBusinessActivities() != null && !"".equals(cecThree.geteBusinessActivities())) || (cecThree.getOtherBusiness() != null && !"".equals(cecThree.getOtherBusiness())) ){
                        count++;
                    }
                    if(!ListUtils.isEmpty(cecThreePlatformList)){
                        for(CecThreePlatform cecThreePlatform : cecThreePlatformList){
                            if(cecThreePlatform.getName() != null && !"".equals(cecThreePlatform.getName())){
                                count++;
                            }
                            if(cecThreePlatform.getWebsite() != null && !"".equals(cecThreePlatform.getWebsite())){
                                count++;
                            }
                            if(cecThreePlatform.getTurnover() != null ){
                                count++;
                            }
                        }
                        //先计算出一共有多少必填字段。
                        double number = 25 + cecThreePlatformList.size()*3;
                        return count/number;
                    }
                    return count/25;
                }
            }
            if(cecBase.getMajorType().equals("S")){
                if(cecThree.getTurnover() != null ){
                    count++;
                }
                if(cecThree.geteTurnover() != null ){
                    count++;
                }
                if(cecThree.getRoomIncome() != null){
                    count++;
                }
                if(cecThree.getMealIncome() != null ){
                    count++;
                }
                if(cecThree.geteMealIncome() != null){
                    count++;
                }
                if(cecThree.getSecondCommoditySales() != null ){
                    count++;
                }
                if(cecThree.getSecondECommoditySales() != null ){
                    count++;
                }
                if(cecThree.getIsHaveEPlatform()== null || cecThree.getIsHaveEPlatform() == 2 ){
                    return count/21;
                }
                if(cecThree.getIsHaveEPlatform() == 1){
                    count++;
                    if(cecThree.getePlatformNumber() != null ){
                        count++;
                    }
                    if((cecThree.geteEconomicActivity()!=null && !"".equals(cecThree.geteEconomicActivity())) || (cecThree.getOtherEconomic()!=null && !"".equals(cecThree.getOtherEconomic()))){
                        count++;
                    }
                    if((cecThree.geteBusinessActivities() != null && !"".equals(cecThree.geteBusinessActivities())) || (cecThree.getOtherBusiness() != null && !"".equals(cecThree.getOtherBusiness()))){
                        count++;
                    }
                    if(!ListUtils.isEmpty(cecThreePlatformList)){
                        for(CecThreePlatform cecThreePlatform : cecThreePlatformList){
                            if(cecThreePlatform.getName() != null && !"".equals(cecThreePlatform.getName())){
                                count++;
                            }
                            if(cecThreePlatform.getWebsite() != null && !"".equals(cecThreePlatform.getWebsite())){
                                count++;
                            }
                            if(cecThreePlatform.getTurnover() != null ){
                                count++;
                            }
                        }
                        //先计算出一共有多少必填字段。
                        double number = 25 + cecThreePlatformList.size()*3;
                        return count/number;
                    }
                    return count/25;
                }
            }
            if(cecThree.getIsHaveEPlatform()== null || cecThree.getIsHaveEPlatform() == 2){
                return count/14;
            }
            if(cecThree.getIsHaveEPlatform() == 1){
                count++;
                if(cecThree.getePlatformNumber() != null ){
                    count++;
                }
                if((cecThree.geteEconomicActivity()!=null && !"".equals(cecThree.geteEconomicActivity())) ||( cecThree.getOtherEconomic()!=null && !"".equals(cecThree.getOtherEconomic()))){
                    count++;
                }
                if((cecThree.geteBusinessActivities() != null && !"".equals(cecThree.geteBusinessActivities())) || (cecThree.getOtherBusiness() != null && !"".equals(cecThree.getOtherBusiness()))){
                    count++;
                }
                if(!ListUtils.isEmpty(cecThreePlatformList)){
                    for(CecThreePlatform cecThreePlatform : cecThreePlatformList){
                        if(cecThreePlatform.getName() != null && !"".equals(cecThreePlatform.getName())){
                            count++;
                        }
                        if(cecThreePlatform.getWebsite() != null && !"".equals(cecThreePlatform.getWebsite()) ){
                            count++;
                        }
                        if(cecThreePlatform.getTurnover() != null ){
                            count++;
                        }
                    }
                    //先计算出一共有多少必填字段。
                    double number = 18 + cecThreePlatformList.size()*3;
                    return count/number;
                }
                return count/18;
            }
        }
        return count;
    }

    @Override
    public double getFourNum(int invId) {
        CecFourExample cecFourExample = new CecFourExample();
        cecFourExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        List<CecFour> cecFourList = cecFourMapper.selectByExample(cecFourExample);
        double count = 0;
        if(!ListUtils.isEmpty(cecFourList)){
            for(CecFour cecFour :cecFourList){
                if(cecFour.getFormType() == 1){
                    if(cecFour.getInvestmentAmount()!= null ){
                        count++;
                    }
                    if(cecFour.getMoreThan500() != null ){
                        count++;
                    }
                    if(cecFour.getMoreThan5000() != null ){
                        count++;
                    }
                }else if (cecFour.getFormType() == 2){
                    if(cecFour.getProjectCode() != null && !"".equals(cecFour.getProjectCode())){
                        count++;
                    }
                    if(cecFour.getProjectName() != null && !"".equals(cecFour.getProjectName())){
                        count++;
                    }
                    if(cecFour.getProvince() != null && !"".equals(cecFour.getProvince())){
                        count++;
                    }
                    if(cecFour.getCity() != null && !"".equals(cecFour.getCity())){
                        count++;
                    }
                    if(cecFour.getCounty() != null && !"".equals(cecFour.getCounty())){
                        count++;
                    }
                    if(cecFour.getTowns() != null && !"".equals(cecFour.getTowns())){
                        count++;
                    }
                    if(cecFour.getVillage() != null && !"".equals(cecFour.getVillage())){
                        count++;
                    }
                    if(cecFour.getUnitZoningCode() != null && !"".equals(cecFour.getUnitZoningCode())){
                        count++;
                    }
                    if(cecFour.getIndustryCode() != null && !"".equals(cecFour.getIndustryCode())){
                        count++;
                    }
                    if(cecFour.getStartTime() != null && !"".equals(cecFour.getStartTime())){
                        count++;
                    }
                    if(cecFour.getProductionTime() != null && !"".equals(cecFour.getProductionTime())){
                        count++;
                    }
                    if(cecFour.getPlanTotalInvestment() != null ){
                        count++;
                    }
                    if(cecFour.getCumulativeInvestment() != null ){
                        count++;
                    }
                    if(cecFour.getCompleteInvestment() != null){
                        count++;
                    }
                    if(cecFour.getInstallationEngineering()!= null ){
                        count++;
                    }
                }
            }
            //求出4表必填字段的数目
            double number = 3 + (cecFourList.size() - 1)*15;
            return count/number;
        }
        return count;
    }

    @Override
    public double getFiveNum(int invId) {
        double count = 0;
        CecFiveExample cecFiveExample = new CecFiveExample();
        cecFiveExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        List<CecFive> cecFiveList = cecFiveMapper.selectByExample(cecFiveExample);
        if(!ListUtils.isEmpty(cecFiveList)){
            //去base表查询组织机构代码
            CecBaseExample cecBaseExample = new CecBaseExample();
            cecBaseExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
            List<CecBase> cecBaseList = cecBaseMapper.selectByExample(cecBaseExample);
            if(ListUtils.isEmpty(cecBaseList)){
                return 0;
            }
            CecBase cecBase = cecBaseList.get(0);
            String baseOrganizationCode = cecBase.getOrganizationCode();

            CecFive cecFive = cecFiveList.get(0);
            //如果上下两个组织机构代码不相同，则直接返回一，代表所有的字段都是非必填的。。
            if( cecFive.getSupplementOrganizationCode() != null &&!cecFive.getSupplementOrganizationCode().equals("") && !baseOrganizationCode.equals(cecFive.getSupplementOrganizationCode())){
                count = 1.0;
                return count;
            }
            if(cecFive.getBeginStock() != null ){
                count++;
            }
            if(cecFive.getTotalCurrentAssets() != null ){
                count++;
            }
            if(cecFive.getMonetaryFunds() != null){
                count++;
            }
            if(cecFive.getCash() != null ){
                count++;
            }
            if(cecFive.getStock() !=null){
                count++;
            }
            if(cecFive.getSecurities() != null ){
                count++;
            }
            if(cecFive.getLongTermInvestment() != null ){
                count++;
            }
            if(cecFive.getFixedAssets() != null ){
                count++;
            }
            if(cecFive.getHousesStructures() != null ){
                count++;
            }
            if(cecFive.getMachineryEquipment() != null ){
                count++;
            }
            if(cecFive.getConveyance() !=null ){
                count++;
            }
            if(cecFive.getFixedAssetsUnderFinancingLease() !=null ){
                count++;
            }
            if(cecFive.getConstructionProject() != null ){
                count++;
            }
            if(cecFive.getIntangibleAssets() != null ){
                count++;
            }
            if(cecFive.getLandUseRight() != null ){
                count++;
            }
            if(cecFive.getPublicInfrastructure() != null ){
                count++;
            }
            if(cecFive.getPublicInfrastructureDepreciation() !=null ){
                count++;
            }
            if(cecFive.getTotalAssets() != null ){
                count++;
            }
            if(cecFive.getTotalDebt() != null ){
                count++;
            }
            if(cecFive.getTotalNetAssets() !=null ){
                count++;
            }
            if(cecFive.getTotalIncome() != null ){
                count++;
            }
            if(cecFive.getFinancialAppropriation() != null ){
                count++;
            }
            if(cecFive.getCauseIncome() != null ){
                count++;
            }
            if(cecFive.getOperatingIncome() != null ){
                count++;
            }
            if(cecFive.getTotalExpenditure() != null ){
                count++;
            }
            if(cecFive.getWelfareExpenditure() != null ){
                count++;
            }
            if(cecFive.getServiceExpenditure() != null ){
                count++;
            }
            if(cecFive.getLaborServiceFee() != null ){
                count++;
            }
            if(cecFive.getLabourUnionExpenditure() !=null){
                count++;
            }
            if(cecFive.getWelfareFunds() !=null ){
                count++;
            }
            if(cecFive.getTaxFee() != null ){
                count++;
            }
            if(cecFive.getHeatingFee() != null ){
                count++;
            }
            if(cecFive.getTravelAbroadFee() != null ){
                count++;
            }
            if(cecFive.getSubsidy() !=null ){
                count++;
            }
            if(cecFive.getRetired() != null ){
                count++;
            }
            if(cecFive.getMedicalAid() != null ){
                count++;
            }
            if(cecFive.getPension() != null ){
                count++;
            }
            if(cecFive.getLivingAllowance() != null ){
                count++;
            }
            if(cecFive.getReliefFee() != null ){
                count++;
            }
            if(cecFive.getSchoolGrant() != null ){
                count++;
            }
            if(cecFive.getOperatingExpenses() != null ){
                count++;
            }
            if(cecFive.getInterestIncome() != null ){
                count++;
            }
            if(cecFive.getInterestExpenditure() != null ){
                count++;
            }
            if(cecFive.getAverageEmployeesNumber() != null ){
                count++;
            }
            return count/44;
        }
        return count;
    }

    @Override
    public double getSixNum(int invId) {
        double count = 0;
        CecSixExample cecSixExample = new CecSixExample();
        cecSixExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        List<CecSix> cecSixList = cecSixMapper.selectByExample(cecSixExample);
        if(!ListUtils.isEmpty(cecSixList)){
            //去base表查询组织机构代码
            CecBaseExample cecBaseExample = new CecBaseExample();
            cecBaseExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
            List<CecBase> cecBasesList = cecBaseMapper.selectByExample(cecBaseExample);
            if(ListUtils.isEmpty(cecBasesList)){
                return 0;
            }
            String baseOrganizationCode = cecBasesList.get(0).getOrganizationCode();

            CecSix cecSix = cecSixList.get(0);
            //如果上下两个组织机构代码不相同，则直接返回一，代表所有的字段都是非必填的。。
            if( cecSix.getSupplementOrganizationCode() != null &&!cecSix.getSupplementOrganizationCode().equals("") && !baseOrganizationCode.equals(cecSix.getSupplementOrganizationCode())){
                count = 1.0;
                return count;
            }
            if(cecSix.getBeginStock() != null ){
                count++;
            }
            if(cecSix.getTotalCurrentAssets() != null ){
                count++;
            }
            if(cecSix.getMonetaryFunds() != null ){
                count++;
            }
            if(cecSix.getCash() != null ){
                count++;
            }
            if(cecSix.getStock() != null ){
                count++;
            }
            if(cecSix.getSecurities() != null ){
                count++;
            }
            if(cecSix.getLongTermInvestment() != null ){
                count++;
            }
            if(cecSix.getFixedAssets() != null ){
                count++;
            }
            if(cecSix.getHousesStructures() != null ){
                count++;
            }
            if(cecSix.getMachineryEquipment() != null ){
                count++;
            }
            if(cecSix.getConveyance() != null ){
                count++;
            }
            if(cecSix.getFixedAssetsUnderFinancingLease() != null ){
                count++;
            }
            if(cecSix.getConstructionProject() != null ){
                count++;
            }
            if(cecSix.getCulturalAssets() != null ){
                count++;
            }
            if(cecSix.getIntangibleAssets() != null ){
                count++;
            }
            if(cecSix.getLandUseRight() != null ){
                count++;
            }
            if(cecSix.getTotalAssets() != null ){
                count++;
            }
            if(cecSix.getTotalDebt() != null ){
                count++;
            }
            if(cecSix.getTotalNetAssets() != null ){
                count++;
            }
            if(cecSix.getTotalIncome() != null ){
                count++;
            }
            if(cecSix.getDonationIncome() != null ){
                count++;
            }
            if(cecSix.getFeeIncome() != null ){
                count++;
            }
            if(cecSix.getTotalExpenditure() != null ){
                count++;
            }
            if(cecSix.getBusinessActivityCost() != null ){
                count++;
            }
            if(cecSix.getActivityPersonnelCosts() != null ){
                count++;
            }
            if(cecSix.getActivityDailyExpenses() != null ){
                count++;
            }
            if(cecSix.getActivityFixedAssetsDepreciation() != null ){
                count++;
            }
            if(cecSix.getTax() != null ){
                count++;
            }
            if(cecSix.getManagementCost() != null ){
                count++;
            }
            if(cecSix.getManagementPersonnelCosts() !=null ){
                count++;
            }
            if(cecSix.getManagementDailyExpenses() != null ){
                count++;
            }
            if(cecSix.getManagementFixedAssetsDepreciation() != null ){
                count++;
            }
            if(cecSix.getManagementTaxation() != null ){
                count++;
            }
            if(cecSix.getNetAssetChange() != null ){
                count++;
            }
            return count/34;
        }
        return count;
    }

    @Override
    public double getSevenNum(int invId) {
        double count =0.0;
        CecSevenExample cecSevenExample = new CecSevenExample();
        cecSevenExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        List<CecSeven> cecSevenList = cecSevenMapper.selectByExample(cecSevenExample);
        if(!ListUtils.isEmpty(cecSevenList)){
            CecSeven cecSeven = cecSevenList.get(0);
            if(cecSeven.getTotalEnergy() != null ){
                count++;
            }
            if(cecSeven.getElectric() != null ){
                count++;
            }
            if(cecSeven.getCoal() != null ){
                count++;
            }
            if(cecSeven.getNaturalGas() != null ){
                count++;
            }
            if(cecSeven.getLiquifiedNaturalGas() != null ){
                count++;
            }
            if(cecSeven.getGasoline() != null ){
                count++;
            }
            if(cecSeven.getDieselOil() != null ){
                count++;
            }
            if(cecSeven.getElectricMoney() != null ){
                count++;
            }
            if(cecSeven.getCoalMoney() != null ){
                count++;
            }
            if(cecSeven.getNaturalGasMoney() != null){
                count++;
            }
            if(cecSeven.getLiquifiedNaturalGasMoney() != null){
                count++;
            }
            if(cecSeven.getGasolineMoney() != null ){
                count++;
            }
            if(cecSeven.getDieselOilMoney() != null  ){
                count++;
            }
            if(cecSeven.getPurchasingHeatMoney() != null ){
                count++;
            }
            return count/14;

        }
        return count;
    }


    @Override
    public double getTotalProgress(int invId) {
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andIdEqualTo((long)invId);
        List<EcnomicInventory> ecnomicInventoryList = ecnomicInventoryMapper.selectByExample(ecnomicInventoryExample);
        if(ListUtils.isEmpty(ecnomicInventoryList)){
            return 0;
        }
        //获取企业类型
        EcnomicInventory ecnomicInventory = ecnomicInventoryList.get(0);
        String type = ecnomicInventory.getType();
        if(type == null){
            return 0;
        }
        String[] strings = type.split(",");
        double baseNum = getBaseNum(invId);
        //长度为1，说明只有一张base表
        if(strings.length == 1){
            return baseNum/1;
        }
        for(String s : strings){
            switch (s){
                case "1" : baseNum += getOneNum(invId);
                    break;
                case "2" : baseNum += getTwoNum(invId);
                    break;
                case "3" : baseNum += getThreeNum(invId);
                    break;
                case "4" : baseNum += getFourNum(invId);
                    break;
                case "5" : baseNum += getFiveNum(invId);
                    break;
                case "6" : baseNum += getSixNum(invId);
                    break;
                case "7" : baseNum += getSevenNum(invId);
                    break;
                default:break;
            }
        }
        return baseNum/(strings.length+1);
    }
}
