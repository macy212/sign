package net.yunxinyong.sign.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.entity.SignAdminExample;
import net.yunxinyong.sign.entity.SignRecord;
import net.yunxinyong.sign.entity.SignRecordExample;
import net.yunxinyong.sign.mapper.*;
import net.yunxinyong.sign.service.RecordService;
import net.yunxinyong.sign.utils.ListUtils;
import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.vo.SignRecordReqVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Log4j2
public class RecordServiceImpl implements RecordService {

    @Autowired
    private SignRecordMapper signRecordMapper;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;
    @Autowired
    private CecUserMapper cecUserMapper;
    @Autowired
    private StatusMapper statusMapper;

    @Override
    public boolean insert(Integer adminId, Integer invId, Integer userId, Integer type) {

        SignRecord signRecord = new SignRecord();
        signRecord.setAdminId(adminId);
        signRecord.setTpye(type);
        signRecord.setCreateTime(new Date());
        signRecord.setInvId(invId);
        signRecord.setUserId(userId);
        signRecord.setArea(signAdminMapper.selectByPrimaryKey(adminId).getArea());

        String phone = signAdminMapper.selectByPrimaryKey(adminId).getPhone();
        String role = signAdminMapper.selectByPrimaryKey(adminId).getRole();
        String unitDetailedName = "";
        if (invId != null) {
            unitDetailedName = ecnomicInventoryMapper.selectByPrimaryKey((long) invId).getUnitDetailedName();
        }

        //操作类型默认0修改企业信息1修改企业状态2添加后台管理账号3修改后台管理账号权限4封禁企业账号5解封企业账号6添加企业状态7修改企业状态
        //8通过添加企业审核9未通过添加企业审核
        //12操作企业 14下载企业审核图片 15 下载企业登记图片
        if (type == 0) {
            signRecord.setContent(role + phone + "修改了" + unitDetailedName + "的登记数据");
        } else if (type == 1) {
            signRecord.setContent(role + phone + "修改了" + unitDetailedName + "的登记状态");
        } else if (type == 2) {
            SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(userId);
            signRecord.setContent(role + phone + "添加了" + signAdmin.getRole() + signAdmin.getPhone() + "的账号");
        } else if (type == 3) {
            SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(userId);
            signRecord.setContent(role + phone + "修改了" + signAdmin.getRole() + signAdmin.getPhone() + "的权限");
        } else if (type == 4) {
            signRecord.setContent(role + phone + "封禁了企业账号" + cecUserMapper.selectByPrimaryKey(userId).getPhone() + "的权限");
        } else if (type == 5) {
            signRecord.setContent(role + phone + "解封了企业账号" + cecUserMapper.selectByPrimaryKey(userId).getPhone() + "的权限");
        } else if (type == 6) {
            signRecord.setContent(role + phone + "添加了状态" + statusMapper.selectByPrimaryKey(userId).getStatusValue());
        } else if (type == 7) {
            signRecord.setContent(role + phone + "修改了状态" + statusMapper.selectByPrimaryKey(userId).getStatusValue());
        } else if (type == 8) {
            signRecord.setContent(role + phone + "审核通过了" + unitDetailedName + "的添加申请");
        } else if (type == 9) {
            signRecord.setContent(role + phone + "审核未通过" + unitDetailedName + "的添加申请");
        } else if (type == 10) {
            //signRecord.setInvId(null);
            //signRecord.setContent(role+phone+"给的普查员，"+signAdminMapper.selectByPrimaryKey(userId).getPhone()+"分配了"+invId+"家企业");
            signRecord.setContent(role + phone + "的管理员给普查员" + signAdminMapper.selectByPrimaryKey(userId).getPhone() + "分配了负责的企业");
        } else if (type == 11) {
            //signRecord.setInvId(null);
            //signRecord.setContent(role+phone+"减少了"+invId+"家普查员"+signAdminMapper.selectByPrimaryKey(userId).getPhone()+"负责的企业");
            signRecord.setContent(role + phone + "的管理员，减少了普查员" + signAdminMapper.selectByPrimaryKey(userId).getPhone() + "负责的企业");
        } else if (type == 12) {
            signRecord.setContent(role + phone + "操作了" + unitDetailedName);
        }else if (type == 13){ //添加企业
            signRecord.setContent(role + ":"+phone+"添加了企业"+unitDetailedName);
        }else if (type == 14){
            signRecord.setContent(role + " : " + phone + " 下载了被审核企业 " +unitDetailedName + " 上传的资料。");
        }else if (type == 15){
            signRecord.setContent(role + " : " + phone + " 下载了企业 " + unitDetailedName + " 登记填写的资料。");
        }
        return signRecordMapper.insertSelective(signRecord) > 0;

    }

    @Override
    public PageBean getList(SignRecordReqVo signRecordReqVo, Integer adminId, int page, int rows) {
        SignRecordExample signRecordExample = new SignRecordExample();
        SignRecordExample.Criteria criteria = signRecordExample.createCriteria();
        //普查员只能看到自己的操作记录，管理职能看到自己区域的操作记录
        if (signAdminMapper.selectByPrimaryKey(adminId).getRole().equals("普查员")) {
            criteria.andAdminIdEqualTo(adminId);
        } else if (signAdminMapper.selectByPrimaryKey(adminId).getRole().equals("管理员")) {
            criteria.andAreaEqualTo(signAdminMapper.selectByPrimaryKey(adminId).getArea());
        }

        if (signRecordReqVo.getArea() != null && !signRecordReqVo.getArea().equals("")) {
            criteria.andAreaEqualTo(signRecordReqVo.getArea());
        }
        //添加搜索条件
        if (signRecordReqVo.getStartTime() != null && !signRecordReqVo.getStartTime().toString().equals("")) {
            criteria.andCreateTimeGreaterThanOrEqualTo(signRecordReqVo.getStartTime());
        }
        if (signRecordReqVo.getEndTime() != null && !signRecordReqVo.getEndTime().toString().equals("")) {
            criteria.andCreateTimeLessThanOrEqualTo(signRecordReqVo.getEndTime());
        }
        if (signRecordReqVo.getAdminId() != null) {
            criteria.andAdminIdEqualTo(signRecordReqVo.getAdminId());
        }
        if (signRecordReqVo.getInvId() != null) {
            criteria.andInvIdEqualTo(signRecordReqVo.getInvId());
        }
        if (signRecordReqVo.getUserId() != null) {
            criteria.andUserIdEqualTo(signRecordReqVo.getUserId());
        }
        if (signRecordReqVo.getAdminId() == null && signRecordReqVo.getAdminAccount() != null && !signRecordReqVo.getAdminAccount().equals("")) {
            SignAdminExample signAdminExample = new SignAdminExample();
            signAdminExample.createCriteria().andStateEqualTo(0).andPhoneEqualTo(signRecordReqVo.getAdminAccount());
            List<SignAdmin> signAdmins = signAdminMapper.selectByExample(signAdminExample);
            if (!ListUtils.isEmpty(signAdmins)) {
                criteria.andAdminIdEqualTo(signAdmins.get(0).getId());
            }
        }
        if (signRecordReqVo.getUserId() == null && signRecordReqVo.getUserAccount() != null && !signRecordReqVo.getUserAccount().equals("")) {
            SignAdminExample signAdminExample1 = new SignAdminExample();
            signAdminExample1.createCriteria().andStateEqualTo(0).andPhoneEqualTo(signRecordReqVo.getUserAccount());
            List<SignAdmin> signAdmins = signAdminMapper.selectByExample(signAdminExample1);
            if (!ListUtils.isEmpty(signAdmins)) {
                criteria.andUserIdEqualTo(signAdmins.get(0).getId());
            }
        }


        if (page != 0 && rows != 0) {
            PageHelper.startPage(page, rows);
        }
        List<SignRecord> signRecordList = signRecordMapper.selectByExample(signRecordExample);
        PageBean<SignRecord> pageBean = new PageBean<>();
        PageInfo<SignRecord> pageInfo = new PageInfo<>(signRecordList);
        pageBean.setTotal(pageInfo.getTotal());
        pageBean.setTotalPages(pageInfo.getPages());
        pageBean.setPageNumber(pageInfo.getPageNum());
        pageBean.setPageSize(pageInfo.getSize());
        pageBean.setPageDatas(signRecordList);
        return pageBean;
    }
}
