package net.yunxinyong.sign.service;

import net.yunxinyong.sign.vo.StatusStatisticsVo;

public interface EnumeratorInfoService {
    StatusStatisticsVo getAllInfo(int adminId);
}
