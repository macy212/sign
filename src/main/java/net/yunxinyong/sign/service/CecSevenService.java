package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.CecSeven;
import net.yunxinyong.sign.vo.ResponseVo;

public interface CecSevenService {
    CecSeven select(int invid);

    void update(CecSeven cecSeven,Integer adminId);

    void insert(CecSeven cecSeven,Integer adminId);
}
