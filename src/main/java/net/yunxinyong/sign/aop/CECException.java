package net.yunxinyong.sign.aop;

/**
 * 全局异常（传入需要返回给前台的状态码和回显信息）
 */
public class CECException extends RuntimeException {

    private int status;

    public CECException(int status, String message) {
        super(message);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
