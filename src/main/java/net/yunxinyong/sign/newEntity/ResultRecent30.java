package net.yunxinyong.sign.newEntity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ResultRecent30 {
    private Integer year;
    private Integer month;
    private Integer day;
    private Integer amount;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dateTime;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "ResultRecent30{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", amount=" + amount +
                ", dateTime=" + dateTime +
                '}';
    }
}
